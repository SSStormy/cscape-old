using CScape.Game.Objects.Player;
using CScape.Network.Synchronization;

namespace CScape.Game.Interface
{
    public interface IInterfaceSyncContextStrategy
    {
        InterfaceSyncContext GetContext(Player player);
    }
}