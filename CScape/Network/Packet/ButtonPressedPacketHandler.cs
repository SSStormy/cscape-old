﻿using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class ButtonPressedPacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var buttonId = packetStream.ReadUnsignedWord();
            await context.Player.Interface.HandleButton(buttonId);
            return PacketResult.AllOk;
        }
    }
}
