﻿using CScape.Network;

namespace CScape.Game
{
    public sealed class UserConnectProof
    {
        public const int CrcAmount = 9;
        public const int KeySize = 4;
        public const int KeySalt = 50;

        public byte ConnectStatus { get; }
        public byte Size { get; }
        public bool IsLowMem { get; }
        public int[] CRCAuth { get; }
        public int[] Keys { get; }
        public int UserId { get; }
        public string Username { get; }
        public string Password { get; }
        public int Revision { get; }

        public ConnectionContext Context { get; }

        public UserConnectProof(ConnectionContext context, byte connectStatus, byte size, bool isLowMem, int[] crcAuth, int[] keys, int userId, string username, string password, int revision)
        {
            Context = context;
            ConnectStatus = connectStatus;
            Size = size;
            IsLowMem = isLowMem;
            CRCAuth = crcAuth;
            Keys = keys;
            UserId = userId;
            Username = username.ToLowerInvariant();
            Password = password;
            Revision = revision;
        }
    }
}
