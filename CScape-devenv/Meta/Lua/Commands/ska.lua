﻿REQUIRE = { "player", "args" }

PARAMS =
{
	skill = arg.new(valType.int, argType.required),
	exp = arg.new(valType.int, argType.required)
}
	
function command (player, args)
	player.skills[args.skill].gainExp (args.exp)
end