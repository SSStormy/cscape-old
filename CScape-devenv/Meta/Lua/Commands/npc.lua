﻿REQUIRE = { "player", "args", "server" }

PARAMS =
{
	id = arg.new(valType.int, argType.required)
}

function command (player, args, server)
	server.npcProvider.spawn(args.id, player.position.globalX, player.position.globalY, player.position.globalZ)
end
