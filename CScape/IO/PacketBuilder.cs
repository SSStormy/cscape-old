﻿using System;
using System.Diagnostics;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.Network;
using CScape.Network.Synchronization.Messages;
using CScape.Server;

namespace CScape.IO
{
    public sealed class PacketBuilder : RsByteWriter, ISynchronizationMessage
    {
        public const int BufferSize = 1024*2;

        public IsaacRandomGen Isaac { get; set; }
        public ScapeServer Server { get; }

        private bool _hasWrittenOpcode;
        private int _sizePos;
        public int SizeMeta { get; private set; }

        /// <summary>
        /// Creates a preset size packet shell.
        /// </summary>
        public PacketBuilder(Player player, OutPacketType type) : this(player, 0)
        {
            var size = player.Server.OutboundSchemas.Get((int) type);
            if (size < 0)
                throw new InvalidOperationException($"Tried to make preset size packet shell for packet type {type}");
            if (size > 0)
            {
                _buffer = new byte[size + 1];
                Size = size + 1;
                Opcode(type);
            }
            else
                _buffer[0] = (byte) type;
        }

        public PacketBuilder(Player player, int size = BufferSize) : this(player.Server, player.Connection.IsaacOut, size)
        {
        }

        public PacketBuilder(ScapeServer server, IsaacRandomGen isaac = null, int size = BufferSize) : base(size)
        {
            Server = server;
            Isaac = isaac;
            Server.Stats.CurrentTick.PacketBuildersAllocated++;
        }

        public PacketBuilder Opcode(OutPacketType opcode)
            => Opcode((byte) opcode);

        public PacketBuilder Opcode(byte opcode)
        {
            Debug.Assert(!_hasWrittenOpcode);
            _hasWrittenOpcode = true;

            if (Isaac != null)
                opcode = (byte) (opcode + Isaac.GetValue() & 0xff);

            WriteByte(opcode);

            return this;
        }

        public PacketBuilder OpcodeAndSizePlaceholder(byte opcode)
        {
            Opcode(opcode);
            SizePlaceholder(opcode);
            return this;
        }

        public PacketBuilder OpcodeAndSizePlaceholder(OutPacketType opcode)
        {
            Opcode(opcode);
            SizePlaceholder(opcode);
            return this;
        }

        public PacketBuilder OpcodeAndSizeShell(OutPacketType opcode)
        {
            Position++;
            SizePlaceholder((byte) opcode);
            return this;
        }

        public PacketBuilder SizePlaceholder(byte opcode)
        {
            SizeMeta = Server.OutboundSchemas.Get(opcode);
            Debug.Assert(SizeMeta != Globals.PacketSizeUnknown);

            _sizePos = Position;
            switch (SizeMeta)
            {
                case Globals.PacketSizeByte:
                    SizeMeta = Globals.PacketSizeByte;
                    WriteByte(0);
                    break;
                case Globals.PacketSizeShort:
                    SizeMeta = Globals.PacketSizeShort;
                    WriteWord(0);
                    break;
                default:
                    SizeMeta = Globals.PacketSizeUnknown;
                    _sizePos = Position;
                    break;

            }
            return this;
        }

        public PacketBuilder SizePlaceholder(OutPacketType opcode)
            => SizePlaceholder((byte) opcode);

        public PacketBuilder WriteSize()
        {
            Debug.Assert(SizeMeta != Globals.PacketSizeUnknown);

            var size = Position - _sizePos - Math.Abs(SizeMeta);
            if (size == 0)
                return this;

            var tempPos = Position;
            Position = _sizePos;

            switch (SizeMeta)
            {
                case Globals.PacketSizeByte:
                    WriteByte(size);
                    break;
                case Globals.PacketSizeShort:
                    WriteWord(size);
                    break;
            }
            Position = tempPos;
            return this;
        }

        public override RsByteWriter Reset(bool reuseForBitwise = false)
        {
            _hasWrittenOpcode = false;
            _sizePos = 0;
            SizeMeta = 0;

            return base.Reset(reuseForBitwise);
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            return this;
        }
    }
}