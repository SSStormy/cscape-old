﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CScapeCache;

namespace CollisionCalculator
{
    internal sealed class Program
    {
        public static void Main(string[] argv)
        {
            Console.WriteLine("cscape collision calculator");

            var args = argv.ToList();
            if (args.Count < 2)
            {
                Console.WriteLine("Invalid usage.");
                Console.WriteLine("Params:");
                Console.WriteLine("1: Cache data dir (main_file_cache.dat)");
                Console.WriteLine("2: Cache index base dir (main_file_cache.idx)");
                return;
            }

            var shouldDump = args.Count <= 3;
            if(shouldDump)
                Console.WriteLine("Will dump");

            var watch = Stopwatch.StartNew();
            var cache = new CacheReader(args[0], args[1]);
            Console.WriteLine("Calculating...");

            var collision = CollisionProvider.CalculateCollision(cache);

            if (shouldDump)
            {
                using (var writer = new StreamWriter("collision.txt") {AutoFlush = true})
                    foreach (var coll in collision)
                    {
                        writer.WriteLine(
                            $"{coll.Key.X} {coll.Key.Y} {coll.Key.Z} => {coll.Value.CollisionMask} | {coll.Value.IsBlocked} | {(int) coll.Value.CollisionMask}");
                    }
            }

            Console.WriteLine($"Calculated {collision.Count:G} collision indices.");
            Console.WriteLine("Serializing...");

            CollisionSerializer.Serialize("collision.bin", collision);

            watch.Stop();
            Console.WriteLine($"Finished in {watch.ElapsedMilliseconds}ms");
        }
    }
}
