﻿using Newtonsoft.Json;

namespace CScape.Game.Items
{
    public enum EquipType
    {
        None = -1,
        Helmet = 0,
        Cape = 1,
        Amulet = 2,
        Weapon = 3,
        Chest = 4,
        Shield = 5,
        Leg = 7,
        Glove = 9,
        Feet = 10,
        Ring = 12,
        Arrows = 13,
    }


    public sealed class ItemDef
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("examine")]
        public string Examine { get; set; }

        [JsonProperty("equipmentType")]
        public EquipType EquipmentType { get; set; }

        [JsonProperty("noted")]
        public bool Noted { get; set; }

        [JsonProperty("noteable")]
        public bool Noteable { get; set; }

        [JsonProperty("stackable")]
        public bool Stackable { get; set; }

        [JsonProperty("parentId")]
        public int ParentId { get; set; }

        [JsonProperty("notedId")]
        public int NotedId { get; set; }

        [JsonProperty("members")]
        public bool Members { get; set; }

        [JsonProperty("specialStorePrice")]
        public int SpecialStorePrice { get; set; }

        [JsonProperty("generalStorePrice")]
        public int GeneralStorePrice { get; set; }

        [JsonProperty("highAlchValue")]
        public int HighAlchValue { get; set; }

        [JsonProperty("lowAlchValue")]
        public int LowAlchValue { get; set; }

        [JsonProperty("weight")]
        public double Weight { get; set; }

        [JsonProperty("bonuses")]
        public int[] Bonuses { get; set; }
    }
}