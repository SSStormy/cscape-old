﻿using CScape.Game.Items;

namespace CScape.Game.Container
{
    public interface IListContainer  : IContainer
    {
        bool AddItem(ItemStack item);
        bool RemoveItem(ItemStack item);
    }
}