﻿namespace CScape.Server
{
    public sealed class ServerStats
    {
        public class StatCollection
        {
            public int PacketsSent { get; internal set; }
            public int BytesSent { get; internal set; }
            public int BytesReceived { get; internal set; }
            public int PacketsReceived { get; internal set; }
            public int TickProcessTime { get; internal set; }
            public int PacketBuildersAllocated { get; internal set; }

            internal StatCollection() { }
        }

        public StatCollection LastTick { get; private set; }=  new StatCollection();
        public StatCollection CurrentTick { get; private set; } = new StatCollection();

        public ScapeServer Server { get; set; }

        public ServerStats(ScapeServer server)
        {
            Server = server;
        }

        internal void ResetTickData()
        {
            LastTick = CurrentTick;
            CurrentTick = new StatCollection();
        }
    }
}