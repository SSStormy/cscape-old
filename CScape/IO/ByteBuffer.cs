﻿using System;

namespace CScape.IO
{
    /// <summary>
    /// Defines a class which hold a byte buffer, the start index of information and the size of the buffer.
    /// </summary>
    public sealed class ByteBuffer : IEquatable<ByteBuffer>
    {
        public byte[] Buffer { get; }
        public int Start { get; set; }
        public int Size { get; set; }

        public ByteBuffer(byte[] buffer, int start, int size)
        {
            Buffer = buffer;
            Start = start;
            Size = size;
        }

        public static ByteBuffer Empty => new ByteBuffer(null, 0 , 0);

        public bool Equals(ByteBuffer other)
        {
            return other.Buffer.Equals(Buffer) && other.Size == Size && other.Start == Start;
        }
    }
}
