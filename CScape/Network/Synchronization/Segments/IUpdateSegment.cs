﻿using CScape.IO;

namespace CScape.Network.Synchronization.Segments
{
    public interface IUpdateSegment
    {
        void WriteInto(PacketBuilder packet);
    }
}
