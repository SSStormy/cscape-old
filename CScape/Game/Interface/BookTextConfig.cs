﻿using System;
using CScape.Server;
using Imms;

namespace CScape.Game.Interface
{
    public sealed class BookTextConfig : ITextConfig
    {
        public ScapeServer Server { get; }
        public ImmMap<int, string> ChildMessages { get; private set; } = ImmMap<int, string>.Empty();

        public const int MaxLines = 29;

        public BookTextConfig(ScapeServer server)
        {
            Server = server;
        }

        public void SetLine(int lineIndex, string text)
        {
            VerifyLineIndex(lineIndex);
            ChildMessages = ChildMessages.Set(Server.Interfaces.Server.Interfaces.TextIds.BookFirstTextLine + lineIndex, text);
        }

        private void VerifyLineIndex(int index)
        {
            if (index < 0 || MaxLines < index)
                throw new InvalidOperationException();
        }

        public string GetLine(int lineIndex)
        {
            VerifyLineIndex(lineIndex);
            return OptionalToString(lineIndex);
        }

        private string OptionalToString(int key)
        {
            var ret = ChildMessages.TryGet(key);
            return ret.IsNone ? null : ret.Value;
        }

        public string PageOne
        {
            get { return OptionalToString(Server.Interfaces.TextIds.BookPageOne); }
            set { ChildMessages = ChildMessages.Set(Server.Interfaces.TextIds.BookPageOne, value); }
        }

        public string PageTwo
        {
            get { return OptionalToString(Server.Interfaces.TextIds.BookPageTwo); }
            set { ChildMessages = ChildMessages.Set(Server.Interfaces.TextIds.BookPageTwo, value); }
        }
        public string CloseWindow
        {
            get { return OptionalToString(Server.Interfaces.TextIds.BookCloseWindow); }
            set { ChildMessages = ChildMessages.Set(Server.Interfaces.TextIds.BookCloseWindow, value); }
        }
        public string Title
        {
            get { return OptionalToString(Server.Interfaces.TextIds.BookTitle); }
            set { ChildMessages = ChildMessages.Set(Server.Interfaces.TextIds.BookTitle, value); }
        }
    }
}
