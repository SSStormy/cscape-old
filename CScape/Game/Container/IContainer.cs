﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CScape.Game.Items;

namespace CScape.Game.Container
{
    public interface IContainer : IEnumerable<ItemStack>
    {
        int ContainerId { get; }
        int Capacity { get; }
        int Items { get; }
        IEnumerable<ContainerSlot> Slots { get; }

        event EventHandler SyncClear;
        event EventHandler<int> SyncDirtyAtIndex;
        
        ItemStack this[int i] { get; set; }

        ContainerChangeInfo CanAddItem(ItemStack item);
        ContainerChangeInfo CanRemoveItem(ItemStack item);

        void Clear();
    }

    public class ContainerSyncData
    {
        public IContainer Container { get; }

        public bool ClearFlag { get; set; }
        private bool[] _dirty;

        public bool this[int index]
        {
            get
            {
                Debug.Assert(index >= 0 && Container.Capacity > index);
                return _dirty[index];
            }
            set
            {
                Debug.Assert(index >= 0 && Container.Capacity > index);
                _dirty[index] = value;
            }
        }

        public ContainerSyncData(IContainer container)
        {
            Container = container;
            Reset();
        }

        public void Reset()
        {
            ClearFlag = false;
            _dirty = new bool[Container.Capacity];
        }     
    }
}
