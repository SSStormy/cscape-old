﻿namespace CScape.Game.Container
{
    public sealed class ContainerChangeInfo
    {
        public bool IsValidOperation { get; }
        public int Index { get; }
        public bool ShouldChangeAmount { get; }
        public uint AmountRemainder { get; }

        private ContainerChangeInfo(bool isValidOperation)
        {
            IsValidOperation = isValidOperation;
        }

        public ContainerChangeInfo(bool isValidOperation, int index)
        {
            IsValidOperation = isValidOperation;
            Index = index;
        }

        public ContainerChangeInfo(bool isValidOperation, int index, bool shouldChangeAmount, uint amountRemainder)
        {
            IsValidOperation = isValidOperation;
            Index = index;
            ShouldChangeAmount = shouldChangeAmount;
            AmountRemainder = amountRemainder;
        }

        public static ContainerChangeInfo Invalid { get; } = new ContainerChangeInfo(false);
        public static ContainerChangeInfo Valid { get; } = new ContainerChangeInfo(true);
    }
}
