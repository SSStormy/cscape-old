using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Container;
using CScape.Server.Tick;
using MoonSharp.Interpreter;

namespace CScape.Game.Interface
{
    public abstract class InterfaceController
    {
        private IContainer _container;

        public IShowableInterface Interface { get; }
        public PlayerInterfaces Parent { get; }

        public IContainer Container
        {
            get { return _container; }
            set
            {
                if (value != null)
                {
                    Parent.Player.Containers.AddOrUpdate(value.ContainerId, value, (i, c) => c);
                    _container = value;
                }
                else
                    DisposeContainer();
            }
        }

        private TickActions Tick => Parent.Server.Tick;

        public InterfaceController(PlayerInterfaces parent, IShowableInterface interf)
        {
            Parent = parent;
            Interface = interf;
        }

        public ConcurrentDictionary<int, Action> ButtonHandlers { get; } = new ConcurrentDictionary<int, Action>();
        public ConcurrentDictionary<int, Func<Task>> ButtonHandlersAsync { get; } =
            new ConcurrentDictionary<int, Func<Task>>();

        public ConcurrentDictionary<int, DynValue> ButtonHandlersLua { get; } =
            new ConcurrentDictionary<int, DynValue>();

        public async Task HandleButton(int buttonId)
        {
            if (ButtonHandlers.ContainsKey(buttonId))
                Tick.ScheduleUniqueNext(TickActionFactory.Create(ButtonHandlers[buttonId], GetHashCode() ^ buttonId));

            if (ButtonHandlersAsync.ContainsKey(buttonId))
                Tick.ScheduleUniqueNext(TickActionFactory.Create(ButtonHandlersAsync[buttonId], GetHashCode() ^ buttonId));

            if (ButtonHandlersLua.ContainsKey(buttonId))
            {
                var dyn = ButtonHandlersLua[buttonId];
                if (dyn.Type != DataType.Function)
                    throw new InvalidOperationException(
                        $"Tried to call a nonfunction value as button handler or id {buttonId} for {Interface.InterfaceId}");

                Tick.ScheduleUniqueNext(TickActionFactory.Create(ButtonHandlersLua[buttonId], GetHashCode() ^ buttonId));
            }
        }

        private void DisposeContainer()
        {
            if (_container != null)
            {
                IContainer container;
                Parent.Player.Containers.TryRemove(_container.ContainerId, out container);
                _container = null;
            }
        }

        public void SendConfig()
        {
            // 36
        }

        public void Offset()
        {
            // 70
        }

        public void ClearInventory()
        {
            // 72   
        }

        public void SetScrollbarPosition()
        {
            // 79
        }

        public void SetButtonState()
        {
            // 87
        }

        public void SetColor()
        {
            // 122
        }

        public void PlayAnimation()
        {
            // 200
        }

        public void SetComponentZoomRotation()
        {
            // 230
        }

        public void DisplayItem()
        {
            // 246
        }

        public void ShowPlayerHead()
        {
            // 185
        }

        public void ShowNpcHead()
        {
            // 75
        }

        public void MoveComponent()
        {
            // 70
        }

        public void ShowModelOnComponent()
        {
            // 8
        }

        public void RecolorComponent()
        {
            // 112
        }

        public abstract Task Show();
        
        public async Task Close()
        {
            DisposeContainer();
            await InternalClose();
        }

        protected abstract Task InternalClose();

        public override int GetHashCode()
            => GetType().Name.GetHashCode() ^ 13;
    }
}