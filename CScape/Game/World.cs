﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using CollisionCalculator;
using CScape.Server;
using CScapeCache.Caches.Collision;

namespace CScape.Game
{
    public sealed class World
    {
        public ScapeServer Server { get; }
        public LogManager Logs => Server.Logs;

        public const int MaxRegions = 1024;
        private readonly Region[,] _regions = new Region[MaxRegions, MaxRegions];

        private Dictionary<PositionKey, CollisionData> _collision;

        public World(ScapeServer server)
        {
            Server = server;
        }

        public void LoadCollision()
        {
            _collision?.Clear();
            _collision = CollisionSerializer.Deserialize(Server.Config.CollisionDataDir);
        }

        public CollisionData GetCollision(int x, int y, int z, out PositionKey posKey)
        {
            posKey = new PositionKey(x, y, z);
            return GetCollision(posKey);
        }

        public CollisionData GetCollision(int x, int y, int z)
        {
            PositionKey ignored;
            return GetCollision(x,y,z, out ignored);
        }

        public CollisionData GetCollision(PositionKey key)
            => !_collision.ContainsKey(key) ? CollisionData.None : _collision[key];

        public Region GetByTileCoordinate(int tileX, int tileY)
            => GetByMapCoordinate(tileX >> 3, tileY >> 3);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ThrowIfOutOfRegionRange(int region)
        {
            if (region < 0 || region >= MaxRegions) throw new ArgumentOutOfRangeException(nameof(region));
        }

        public Region GetByMapCoordinate(int regionX, int regionY)
        {
            ThrowIfOutOfRegionRange(regionX);
            ThrowIfOutOfRegionRange(regionY);

            lock (_regions)
            {
                if (regionX < 0 || regionY < 0 || regionX >= MaxRegions || regionY >= MaxRegions)
                {
                    Logs.Warning(this, $"Region index out of range {regionX} {regionY}");
                    return null;
                }

                if (_regions[regionX, regionY] == null)
                    Region.Allocate(regionX, regionY, this);

                return _regions[regionX, regionY];
            }
        }

        public void SetRegion(Region region)
        {
            if (region == null) throw new ArgumentNullException(nameof(region));
            ThrowIfOutOfRegionRange(region.X);
            ThrowIfOutOfRegionRange(region.Y);

            if (region is PrivateArea)
            {
                throw new NotImplementedException();

                // private areas can override regions.
                var old = _regions[region.X, region.Y];
                if (old != null)
                {
                    if(old.Players.List.Length > 0)
                        throw new InvalidOperationException($"Tried to override existing region with private area when region has players. new region X: {region.X} Y: {region.Y}");
                }
            }

            lock (_regions)
            {
                if (_regions[region.X, region.Y] != null)
                    throw new InvalidOperationException($"Tried to overwrite already set region at region coords X: {region.X} Y: {region.Y}");
                _regions[region.X, region.Y] = region;
            }
        }

        // should values that derive this should never decrease
        public const int PaStartX = 4000;
        // should values that derive this should never increase
        public const int PaStartY = 3500;

        /* 
         * 
         * (4000, 3500) global
         *    (0, 0) local 
         *       *-----------------------  (x)
         *       |
         *       |
         *       |   PRIVATE AREAS ONLY
         *       |
         *       |
         *    
         *      (y)
         *      
         */

        //public Region[,] AllocateNewPrivateArea()
        // {

        // }
    }
}