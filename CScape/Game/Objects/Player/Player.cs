﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Game.Container;
using CScape.Game.Interface;
using CScape.Game.Items;
using CScape.Helper;
using CScape.Network;
using CScape.Network.Synchronization;
using CScape.Network.Synchronization.Machine;
using CScape.Network.Synchronization.Messages;
using CScape.Server;
using Imms;
using Newtonsoft.Json;

namespace CScape.Game.Objects.Player
{
    [JsonObject(MemberSerialization.OptIn)]
    [DebuggerDisplay("Name: {Username}")]
    public class Player : IEquatable<Player>, IEntity
    {
        public enum TitleType : byte
        {
            Normal = 0,
            Moderator = 1,
            Admin = 2,
        }

        public const int MaxUsernameChars = 12;
        public const int MaxPasswordChars = 128;

        [JsonProperty]
        public string Username { get; }

        [JsonProperty]
        public TitleType Title { get; set; }

        [JsonProperty]
        public bool IsFlagged { get; set; }

        [JsonProperty]
        public PlayerBackpack Backpack
            => Containers[Server.Interfaces.ContainerIds.PlayerBackpack] as PlayerBackpack;

        [JsonProperty]
        public PlayerEquipment Equipment
            => Containers[Server.Interfaces.ContainerIds.PlayerEquipment] as PlayerEquipment;

        [JsonProperty]
        public PlayerAppearance Appearance { get; }

        [JsonProperty]
        public PlayerSkills Skills { get; }

        public PlayerInterfaces Interface { get; }
        public PlayerPosition Position { get; private set; }
        public PlayerSyncFlagContainer PlayerSync { get; }
        public PublicChatMessage LastSentPublicChatMessage { get; set; }

        PositionController IEntity.Position => Position;
        ISyncFlagFacade IEntity.Sync => PlayerSync;
        int IEntity.Id => PlayerId;

        public ConcurrentDictionary<int, IContainer> Containers { get; } =
            new ConcurrentDictionary<int, IContainer>();

        /// <summary>
        /// Used for serialization. Set when player is loaded or default initialized AND on logout.
        /// </summary>
        [JsonProperty]
        public int StaleGlobalPosX { get; set; }

        /// <summary>
        /// Used for serialization. Set when player is loaded or default initialized AND on logout.
        /// </summary>
        [JsonProperty]
        public int StaleGlobalPosY { get; set; }

        /// <summary>
        /// Used for serialization. Set when player is loaded or default initialized AND on logout.
        /// </summary>
        [JsonProperty]
        public int StaleGlobalPosZ { get; set; }

        public ScapeServer Server
        {
            get { return _server; }
            private set
            {
                if (_server != null)
                {
                    _server.Logs.Warning(this, $"Tried to set the server when it's not null. {Username}", conid: Connection.ConnectionId);
                    return;
                }

                _server = value;
            }
        }

        public ConnectionContext Connection { get; private set; }

        public int PlayerId { get; private set; }

        [JsonProperty]
        public bool IsMember { get; set; }

        public bool IsLowDetail { get; set; }
        public bool JustLoggedIn { get; set; }

        public bool IsPlaying { get; set; }
        private ScapeServer _server;

        [JsonProperty]
        public Guid Uuid { get; private set; }

        [JsonProperty]
        public DateTime CreatedTime { get; private set; }

        public bool MarkedAsKilledConnection { get; set; }

        private PlayerBackpack _tempBackpack;
        private PlayerEquipment _tempEquip;

        private int _viewDist;

        public List<Player> UpdatePlayers { get; } = new List<Player>(Globals.MaxUpdatePlayers);

        public sealed class SynchronizedImmutableContainer<T> : IEnumerable<T>
        {
            private ImmList<T> _list = ImmList<T>.Empty;
            private readonly object _lock = new object();

            public void AddFirst(T obj)
            {
                lock (_lock)
                    _list = _list.AddFirst(obj);
            }

            public void AddLast(T obj)
            {
                lock (_lock)
                    _list = _list.AddLast(obj);
            }

            public void AddFirstRange(IEnumerable<T> objs)
            {
                lock (_lock)
                    _list = _list.AddFirstRange(objs);
            }

            public void AddLastRange(IEnumerable<T> objs)
            {
                lock (_lock)
                    _list = _list.AddLastRange(objs);
            }

            public bool Remove(T reference)
            {
                lock (_lock)
                {
                    var index = _list.FindIndex(reference);
                    if (index.IsNone)
                        return false;

                    _list = _list.RemoveAt(index.Value);
                    return true;
                }
            }

            public ImmList<T> Flush()
            {
                lock (_lock)
                {
                    var temp = _list;
                    _list = ImmList<T>.Empty;
                    return temp;
                }
            }

            public IEnumerator<T> GetEnumerator()
            {
                lock (_lock)
                    return _list.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        public SynchronizedImmutableContainer<AbstractSynchronizationMachine> SyncMachines { get; }
            = new SynchronizedImmutableContainer<AbstractSynchronizationMachine>();

        public SynchronizedImmutableContainer<ISynchronizationMessage> SyncQueue { get; }
        = new SynchronizedImmutableContainer<ISynchronizationMessage>();

        public int ViewingDistance
        {
            get { return _viewDist; }
            set { _viewDist = Utils.Clamp(value, 0, Globals.MaxViewingDistance); }
        }

        public bool HasExcessivePlayers { get; set; }

        [JsonConstructor]
        private Player(string username, TitleType title, bool isFlagged, PlayerBackpack backpack,
            PlayerEquipment equipment, PlayerAppearance appearance, int staleGlobalPosX, int staleGlobalPosY,
            int staleGlobalPosZ, bool isMember, Guid uuid, PlayerSkills skills) : this()
        {
            Username = username;
            Title = title;
            IsFlagged = isFlagged;
            _tempBackpack = backpack;
            _tempEquip = equipment;
            Appearance = appearance;
            StaleGlobalPosX = staleGlobalPosX;
            StaleGlobalPosY = staleGlobalPosY;
            StaleGlobalPosZ = staleGlobalPosZ;
            IsMember = isMember;
            Uuid = uuid;

            if (skills == null)
                Skills = new PlayerSkills(this);
            else
            {
                Skills = skills;
                Skills.Player = this;
            }
        }

        // default constructor
        private Player(ScapeServer server, string username) : this()
        {
            Server = server;
            Username = username;
            Containers.TryAdd(Server.Interfaces.ContainerIds.PlayerBackpack, new PlayerBackpack(this));
            Containers.TryAdd(Server.Interfaces.ContainerIds.PlayerEquipment, new PlayerEquipment(this));
            Appearance = PlayerAppearance.Default(this);
            Skills = new PlayerSkills(this);
            Position = new PlayerPosition(this);
        }

        public PInterface NewInterfaces { get; }

        private Player()
        {
            NewInterfaces = new PInterface(this);
            Interface = new PlayerInterfaces(this);
            PlayerSync = new PlayerSyncFlagContainer(this);
        }

        public static Player Default(ConnectionContext context, string username)
        {
            var player = new Player(context.Server, username)
            {
                StaleGlobalPosX = context.Server.Config.SpawnX,
                StaleGlobalPosY = context.Server.Config.SpawnY,
                StaleGlobalPosZ = context.Server.Config.SpawnZ,
                IsMember = true,
                Uuid = Guid.NewGuid(),
                CreatedTime = DateTime.Now
            };

            player.Backpack.AddItem(7361); //shield
            player.Backpack.AddItem(4152); //weapon
            player.Backpack.AddItem(2628); //helm
            player.Backpack.AddItem(6571); //cape
            player.Backpack.AddItem(1713); //amulet
            player.Backpack.AddItem(2567); //ring
            player.Backpack.AddItem(4972); //legs
            player.Backpack.AddItem(4968); // chest
            player.Backpack.AddItem(894); // arrow
            player.Backpack.AddItem(89); //boots
            player.Backpack.AddItem(2933); //gloves

            return player;
        }

        public async Task<bool> PrePlay(ConnectionContext context, int pid)
        {
            if (IsPlaying)
            {
                Server.Logs.Warning(this,
                    $"Called PrePlay on {Username} when already playing. Stack: {Environment.StackTrace}");
                return false;
            }

            if (Server == null)
                Server = context.Server;
            Connection = context;

            Position = new PlayerPosition(this);

            // fix temp containers
            if (_tempBackpack != null)
            {
                Containers.TryAdd(Server.Interfaces.ContainerIds.PlayerBackpack, _tempBackpack);
                _tempBackpack = null;
            }

            if (_tempEquip != null)
            {
                Containers.TryAdd(Server.Interfaces.ContainerIds.PlayerEquipment, _tempEquip);
                _tempEquip = null;
            }

            // fix parent references
            context.Player = this;
            Backpack.Player = this;
            Equipment.Player = this;
            Appearance.Player = this;

            PlayerId = pid;

            if (Server == null)
                Server = context.Server;

            await Position.Teleport(
                StaleGlobalPosX,
                StaleGlobalPosY,
                StaleGlobalPosZ);

            PlayerSync.SyncFlags |= PlayerSyncFlags.Appearance;
            JustLoggedIn = true;
            IsPlaying = true;

            SyncMachines.AddFirst(new PlayerSyncMachine(this));
            SyncMachines.AddFirst(new RegionSyncMachine(this));

            SendMessage(Server.Config.GreetingMessage);
            return true;
        }

        public void SendMessage(string msg)
        {
            SyncQueue.AddLast(new SyncChatMessage(msg));
        }

        private readonly object _syncQueueLock = new object();
        private ImmList<AbstractSynchronizationMachine> _syncMachines = ImmList<AbstractSynchronizationMachine>.Empty;

        public async Task SendMessageImmediate(string msg)
        {
            await Connection.SendMessage(new SyncChatMessage(msg));
        }

        public bool CanEquip(ItemStack item)
        {
            var def = Server.ItemRepo.Get(item.ItemId);
            if (def == null || def.EquipmentType == EquipType.None)
                return false;

            return true;
        }

        public void ResetUpdateBools()
        {
            PlayerSync.ResetAllFlags();
            Position.PostUpdate();
            Server.Logs.Debug(this, $"{this} reset update bools finished.");
        }

        public bool Equals(Player other)
            => CommonEqualChecks(other) || InternalEquals(other);

        private int[] DefaultTabs => new[]
        {
            Server.Interfaces.InterfaceIds.SwordAttack,
            Server.Interfaces.InterfaceIds.SkillMenu,
            Server.Interfaces.InterfaceIds.QuestMenu,
            Server.Interfaces.InterfaceIds.PlayerBackpack,
            Server.Interfaces.InterfaceIds.Equipment,
            Server.Interfaces.InterfaceIds.PrayerMenu,
            Server.Interfaces.InterfaceIds.SpellbookStandard,
            -1,
            Server.Interfaces.InterfaceIds.FriendsList,
            Server.Interfaces.InterfaceIds.IgnoresList,
            Server.Interfaces.InterfaceIds.LogoutTab,
            IsLowDetail ? Server.Interfaces.InterfaceIds.OptionsLowDetail : Server.Interfaces.InterfaceIds.OptionsHighDetail,
            Server.Interfaces.InterfaceIds.PlayerControls
        };

        public async Task ForceShowDefaultTabs()
        {
            var tabs = DefaultTabs;
            for (var i = 0; i < tabs.Length; i++)
            {
                var sidebar = new SidebarInterface(tabs[i], i, new CarefreeInterfaceOverrideStrategy());
                NewInterfaces.SetSidebar(i, sidebar);
            }

            NewInterfaces.Sidebar[3].ContextRetriever.GetContext(this).Container = Backpack;
            NewInterfaces.Sidebar[4].ContextRetriever.GetContext(this).Container = Equipment;

            /*
            // todo : send an attack menu that matches the type of the player's currently equipped weapon
            var tabs = DefaultTabs;
            for (var i = 0; i < tabs.Length; i++)
            {
                var id = tabs[i];
                var side = await Interface.ShowSidebar(new GenericSidebarConfig(i, id), true);

                /*
                 * Link containers and setup button handlers.

                if (id == Server.Interfaces.InterfaceIds.Equipment)
                    side.Container = Equipment;
                else if (id == Server.Interfaces.InterfaceIds.PlayerBackpack)
                    side.Container = Backpack;
                else if (id == Server.Interfaces.InterfaceIds.LogoutTab)
                {
                    side.ButtonHandlersAsync[Server.Interfaces.ButtonIds.LogoutButtonId] =
                        async () => { await Server.PlayerProvider.LogoutPlayer(this); };
                }
                else if (id == Server.Interfaces.InterfaceIds.PlayerControls)
                {
                    side.ButtonHandlers[Server.Interfaces.ButtonIds.RunButtonId] = () => { Position.IsRunning = true; };
                    side.ButtonHandlers[Server.Interfaces.ButtonIds.WalkButtonId] =
                        () => { Position.IsRunning = false; };
                }
            }
        */
        }

        public async Task SendDefaultRightClickOptions()
        {
            await SendRightClickOption(5, false, "Trade with");
            await SendRightClickOption(4, false, "Follow");
            await SendRightClickOption(3, false, "null");
            await SendRightClickOption(2, false, "null");
            await SendRightClickOption(1, false, "null");
        }

        public async Task SendRightClickOption(int index, bool isOnTop, string msg)
        {
            await Connection.Send(OutPacketType.PlayerOptions, b =>
            {
                b.WriteByte(index)
                    .WriteByte(isOnTop ? 1 : 0)
                    .WriteString(msg);
            });
        }

        public bool Equals(IEntity other)
            => Equals((object)other);

        public override bool Equals(object other)
        {
            if (CommonEqualChecks(other))
                return true;
            var player = other as Player;
            if (player != null)
                InternalEquals(player);

            return false;
        }

        private bool CommonEqualChecks(object other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(other, this))
                return true;

            return false;
        }

        private bool InternalEquals(Player other)
            => other.Username.Equals(Username, StringComparison.Ordinal);

        public override int GetHashCode()
            => unchecked(Username.GetHashCode()*13);

        public override string ToString()
        {
            return $"[Player: (Name: {Username} Id: {PlayerId} ConId: {Connection.ConnectionId})]";
        }
    }
}