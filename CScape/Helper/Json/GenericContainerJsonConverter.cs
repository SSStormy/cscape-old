﻿using System;
using System.Linq;
using CScape.Game.Container;
using CScape.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CScape.Helper.Json
{
    public class GenericContainerJsonConverter : JsonConverter
    {
        private readonly ScapeServer _server;

        public GenericContainerJsonConverter(ScapeServer server)
        {
            _server = server;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var inv = (GenericContainer)value;
            var array = new JArray();
            var arrayWriter = array.CreateWriter();

            foreach (var entry in inv.Slots)
                serializer.Serialize(arrayWriter, entry);

            array.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var array = JArray.Load(reader);
            var list = array.Select(
                entry => serializer.Deserialize<ContainerSlot>(entry.CreateReader())).ToArray();

            return Activator.CreateInstance(objectType, _server, list);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(GenericContainer).IsAssignableFrom(objectType);
        }
    }
}
