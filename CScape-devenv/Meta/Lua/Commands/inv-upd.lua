﻿REQUIRE = { "player" }

function command (player)
	update (player.backpack)
	update (player.equipment)
	player.sendMessage("Set all slots to dirty.")
end

function update(container)
	for itm in container.slots do
		container.addToDirtyQueue (itm.Index)
	end
end

