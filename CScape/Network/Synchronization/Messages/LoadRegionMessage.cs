using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class LoadRegionMessage : ISynchronizationMessage
    {
        public int RegionX { get; }
        public int RegionY { get; }

        public LoadRegionMessage(int regionX, int regionY)
        {
            RegionX = regionX;
            RegionY = regionY;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var updRegion = new PacketBuilder(receiver.Player, OutPacketType.RegionInit);
            updRegion.WriteWord(RegionX + 6);
            updRegion.WriteWord(RegionY + 6);
            return updRegion;;
        }
    }
}