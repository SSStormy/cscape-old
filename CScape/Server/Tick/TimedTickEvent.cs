﻿namespace CScape.Server.Tick
{
    public abstract class AbstractTimedTickEvent : AbstractServerTickEvent
    {
        public abstract int TickInterval { get; }
        private int _curTick;
        private bool _shouldFire;

        public override bool ShouldFire => _shouldFire;

        public override void TickOccured()
        {
            _shouldFire = false;
            _curTick++;

            if (_curTick == TickInterval)
            {
                _curTick = 0;
                _shouldFire = true;
            }
        }

        public AbstractTimedTickEvent(ScapeServer server) : base(server)
        {
        }
    }
}
