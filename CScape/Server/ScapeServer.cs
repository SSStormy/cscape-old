﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CScape.Game;
using CScape.Game.Items;
using CScape.Lua;
using CScape.Network;
using CScape.Network.Packet;
using CScape.Server.Repository;
using CScape.Server.Tick;
using CScapeCache;

namespace CScape.Server
{
    public sealed class ScapeServer : IDisposable
    {
        public const ushort MaxBacklog = 1024;

        public PlayerProvider PlayerProvider { get; }
        public PlayerIo PlayerIo { get; }
        public NpcProvider NpcProvider { get; }
        public World World { get; private set; }
        public PwdLookup Pwd { get; }
        public CacheReader Cache { get; }
        internal LoginCryptoManager LoginCrypto { get; }

        public IRepository<int, int> InboundSchemas { get; private set; }
        public IRepository<int, int> OutboundSchemas { get; private set; }
        public PacketHandlers InboundPacketHandlers { get; }

        public IRepository<LuaScript, string> LuaRepo { get; }
        public IRepository<ItemDef, int> ItemRepo { get; }
        public InterfaceIdConfig Interfaces { get; }

        public TickActions Tick { get; }
        public LogManager Logs { get; }
        public Socket MainSocket { get; }
        public SocketManager ConnectionManager { get; }

        public bool IsLive { get; private set; }

        public ServerConfig Config { get; set; }

        public ServerStats Stats { get; }

        /// <summary>
        /// Returns the time when the server began serving in the local time format.
        /// </summary>
        public DateTime StartTime { get; private set; }

        private readonly EndPoint _endpoint;
        private readonly List<IServerTickHandler> _tickHandlers;

        public ScapeServer(ServerConfig config, EndPoint endpoint, ProtocolType protocol = ProtocolType.IP)
        {
            Config = config;

            MainSocket = new Socket(SocketType.Stream, protocol);
            _endpoint = endpoint;

            LoginCrypto = new LoginCryptoManager(this);
            LoginCrypto.ReadPrivateKeyFromConfig();
            Logs = new LogManager(this);
            Cache = new CacheReader(Config.CacheDataDir, Config.CacheIndexBaseDir);
            PlayerIo = new PlayerIo(this);
            PlayerIo.LoadAvailablePlayers();
            Pwd = new PwdLookup(this);

            PlayerProvider = new PlayerProvider(this);
            NpcProvider = new NpcProvider(this);

            ConnectionManager = new SocketManager(this);
            InboundPacketHandlers = new PacketHandlers();

            Interfaces = new InterfaceIdConfig(this);
            LuaRepo = new LuaRepository(this);
            ItemRepo = new ItemRepository(this);
            Tick = new TickActions(this);
            Stats = new ServerStats(this);

            _tickHandlers = new List<IServerTickHandler>
            {
                new TickActionTickHandler(this),
                new CleanupZombiesTickEvent(this),
                new SyncPlayerTickEvent(this),
                new NpcUpdateCleanTickEvent(this),
                new DataSaveTickEvent(this)
            };
        }

        public void AddTickHandler(IServerTickHandler handler)
        {
            if (handler == null) throw new ArgumentNullException(nameof(handler));
            _tickHandlers.Add(handler);
        }

        public bool RemoveTickHandler(IServerTickHandler reference)
        {
            if (reference == null) throw new ArgumentNullException(nameof(reference));
            return _tickHandlers.Remove(reference);
        }

        public async Task LoadSchemas()
        {
            InboundSchemas = new PacketSizeRepository(this, Config.InPacketSchemaDir);
            await InboundSchemas.Load();
            OutboundSchemas = new PacketSizeRepository(this, Config.OutPacketSchemaDir);
            await OutboundSchemas.Load();
        }

        public async Task StartServing()
        {
            if (IsLive)
                throw new InvalidOperationException("Already serving.");
            StartTime = DateTime.Now;
            World = new World(this);

            Logs.Normal(this, "* Loading cache.");
            Cache.ItemCache.Load();
            Cache.InterfaceCache.Load();

            Logs.Normal(this, "* Loading collision.");
            World.LoadCollision();

            Logs.Normal(this, "* Loading packet schemas.");
            await LoadSchemas();

            Logs.Normal(this, "* Loading pwds.");
            Pwd.Load();

            Logs.Normal(this, "* Loading interfaces.");
            Interfaces.Load();

            Logs.Normal(this, "* Loading lua.");
            await LuaRepo.Load();

            Logs.Normal(this, "* Load itemdef schema.");
            await ItemRepo.Load();

            Logs.Normal(this, "* Init done.");

            MainSocket.Bind(_endpoint);
            MainSocket.Listen(MaxBacklog);

            IsLive = true;
            ConnectionManager.Listen();

            await MainLoop();
        }

        public async Task MainLoop()
        {
            const int tickrate = 600;

            /* 
             * Time how long a tick takes
             * Subtract the milliseconds from the wait at the bottom.
             */
            var watch = new Stopwatch();

            while (true)
            {
                try
                {
                    watch.Start();

                    foreach (var tick in _tickHandlers)
                    {
                        if (tick.ShouldFire)
                            await tick.Process();

                        tick.TickOccured();
                    }
                }
                catch (Exception e)
                {
                    Debug.Fail("Main loop exception." + e);
                }

                watch.Stop();
                var tickTime = Stats.CurrentTick.TickProcessTime = (int)watch.ElapsedMilliseconds;
                watch.Reset();

                if (tickTime < tickrate)
                    await Task.Delay(tickrate - tickTime);
                else
                    Logs.Warning(this, $"Tick took > {tickTime}ms. Proc time: {tickTime}ms. Skipping tick delay.");

                Stats.ResetTickData();
            }
        }

        public void Dispose()
        {
            MainSocket?.Dispose();
            Pwd.Save();
            PlayerIo.SaveAllPlayers();
        }
    }
}