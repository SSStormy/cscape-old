﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using CScape.Helper;

namespace CScape.Server
{
    public sealed class LogManager
    {
        public ScapeServer Server { get; }

        public enum LogTypeEnum 
        {
            Warning,
            Debug,
            Fatal,
            Exception,
            Normal
        }

        public sealed class LogDataEventArgs : EventArgs
        {
            public DateTime Time {get;}
            public string Message { get; }
            public LogTypeEnum LogType { get; }

            /// <summary>
            /// Returns an exception if there was one associated with the log. Null otherwise.
            /// </summary>
            public Exception Exception { get; }

            [DebuggerStepThrough]
            internal LogDataEventArgs(string message, LogTypeEnum logType, Exception exception, DateTime time)
            {
                Message = message;
                LogType = logType;
                Exception = exception;
                Time = time;
            }
        }

        public event EventHandler<LogDataEventArgs> LogReceived = delegate { };

        internal LogManager(ScapeServer server)
        {
            Server = server;
        }

        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Warning(object sender, string message, Exception ex = null, int conid = -1)
            => Log(sender, message, LogTypeEnum.Warning, ex, conid);

        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Normal(object sender, string message, Exception ex = null, int conid = -1)
            => Log(sender, message, LogTypeEnum.Normal, ex, conid);

        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Debug(object sender, string message, Exception ex = null, int conid = -1)
            => Log(sender, message, LogTypeEnum.Debug, ex, conid);

        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Fatal(object sender, string message, Exception ex = null, int conid = -1)
            => Log(sender, message, LogTypeEnum.Fatal, ex, conid);

        [DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal void Exception(object sender, string message, Exception ex, int conid = -1)
            => Log(sender, message, LogTypeEnum.Exception, ex, conid);

        [DebuggerStepThrough]
        internal void Log(object sender, string message, LogTypeEnum type, Exception ex, int conid)
            => LogReceived.Raise(sender, new LogDataEventArgs(message, type, ex, DateTime.Now));
    }
}
