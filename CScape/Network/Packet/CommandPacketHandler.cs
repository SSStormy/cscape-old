﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Objects;
using CScape.IO;
using CScape.Lua;
using CScapeCache.Caches.Collision;
using MoonSharp.Interpreter;

namespace CScape.Network.Packet
{
    public sealed class CommandPacketHandler : IPacketHandler
    {
        public const string Require = "REQUIRE";
        public const string RequirePlayer = "player";
        public const string RequireServer = "server";
        public const string RequireContext = "context";
        public const string RequireArgs = "args";
        public const string RequireStream = "stream";
        public const string RequireBuilder = "builder";

        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            string rawCall;
            if (!packetStream.ReadRsString(byte.MaxValue, out rawCall))
                return PacketResult.KillConnection;

            await HandleCommand(rawCall, context, packetStream);
            return PacketResult.AllOk;
        }

        private async Task HandleCommand(string rawCall, ConnectionContext context, RsPacketStream stream)
        {
            var call = rawCall.ToLowerInvariant().Trim().Split(' ');
            if (!call.Any())
                return;
            var cmd = call[0];

            if (cmd == "c")
            {
                await context.Player.SendMessageImmediate($"{PositionController.GetDirection(Convert.ToInt32(call[1]), Convert.ToInt32(call[2]))}");
            }

            call = call.Skip(1).ToArray();

            var lua = context.Parent.Parent.LuaRepo.Get(cmd) as LuaCommandScript;

            if (lua == null)
            {
                context.Player.SendMessage($"Unknown command: {cmd}");
                return;
            }

            // todo : work on handling exceptions in commands.
            var argResult = BuildArgs(call, lua, stream, context);

            if (argResult != null)
                await lua.Call("command", argResult);

            return;
        }

        // can't have an ienumerable due to moonsharp trying to pass it to the scripts and not it's values.
        private object[] BuildArgs(string[] call, LuaCommandScript script, RsPacketStream packetStream, ConnectionContext context)
        {
            var require = script.Lua.Globals[Require] as Table;
            if (require == null)
                return null;

            var retval = new List<object>();

            foreach (var entry in require.Values)
            {
                if (entry.Type != DataType.String)
                    continue;

                switch (entry.String)
                {
                    case RequireContext:
                        retval.Add(context);
                        break;
                    case RequirePlayer:
                        retval.Add(context.Player);
                        break;
                    case RequireServer:
                        retval.Add(context.Player.Server);
                        break;
                    case RequireArgs:
                        string err = null;
                        Table result = null;
                        if (!script.ArgProvider.TryParseArgs(call, ref err, ref result))
                        {
                            context.Player.SendMessage(err);
                            return null;
                        }
                        retval.Add(result);
                        break;
                    case RequireStream:
                        retval.Add(packetStream);
                        break;
                    case RequireBuilder:
                        retval.Add(context.Builder());
                        break;
                    default:
                        context.Logs.Warning(this, $"Unknown require: {entry.String} in {call[0]}");
                        break;
                }
            }

            return  retval.ToArray();
        }
    }
}

