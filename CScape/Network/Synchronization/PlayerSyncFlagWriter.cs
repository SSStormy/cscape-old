using System;
using CScape.Game.Objects.Player;
using CScape.IO;
using CScape.Server;

namespace CScape.Network.Synchronization
{
    public sealed class PlayerSyncFlagWriter : ISyncFlagWriter
    {
        public Player Player { get; }
        public PlayerSyncFlags PlayerSyncFlags => Player.PlayerSync.SyncFlags;
        public LogManager Logs => Player.Server.Logs;

        private int _localSet;
        private int _localUnset;

        public void SetFirstContactValues()
        {
            _localSet |= (int)PlayerSyncFlags.Appearance;
            _localSet |= (int)PlayerSyncFlags.FacingTile;
        }

        public int SyncFlags => (int) PlayerSyncFlags;

        public PlayerSyncFlagWriter(Player player)
        {
            Player = player;
        }

        // todo : implement PlayerSyncFlagWriter

        public bool NeedsWriting => ((SyncFlags | _localSet) & ~_localUnset) != 0;

        public void Write(RsByteWriter writer)
        {
            var flags = SyncFlags;
            flags |= _localSet;
            flags &= ~_localUnset;

            if (flags == 0)
                return;

            if (flags >= 0x100)
            {
                flags |= 0x40;
                writer.WriteWord(flags);
            }
            else
                writer.WriteByte(flags);

            if ((flags & (int)PlayerSyncFlags.MovementAnim) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.UpdateGraphics) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.StartAnimation) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.OverheadText) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.ChatText) != 0)
            {
                var msg = Player.LastSentPublicChatMessage;
                writer.WriteWord(((byte)msg.Color << 8) +
                                 (byte)msg.Effects);
                writer.WriteByte((int) Player.Title);
                writer.WriteString(msg.Message);
                Logs.Debug(this, $"{Player} wrote update flag ChatText: (Color: {msg.Color} Effect: {msg.Effects} Message: {msg.Message})");
            }
            if ((flags & (int)PlayerSyncFlags.InteractingObject) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.Appearance) != 0)
            {
                WriteAppearance(writer);
            }
            if ((flags & (int)PlayerSyncFlags.FacingTile) != 0)
            {
                var fx = Player.Position.FacingX;
                var fy = Player.Position.FacingY;

                writer.WriteWord(fx* 2 + 1); // 1538
                writer.WriteWord(fy* 2 + 1); // 1539
                Logs.Debug(this, $"{Player} wrote update flag FacingTile: (FX: {fx} FY: {fy})");
            }
            if ((flags & (int)PlayerSyncFlags.Health) != 0)
            {
                throw new InvalidOperationException();
            }
            if ((flags & (int)PlayerSyncFlags.SpecialHealth) != 0)
            {
                throw new InvalidOperationException();
            }
        }

        public void UnsetLocal(int flag)
        {
            _localUnset |= flag;
        }

        public void SetLocal(int flag)
        {
            _localSet |= flag;
        }

        private void WriteAppearance(RsByteWriter writer)
        {
            const int equipSlotSize = 12;

            const int plrObjMagic = 0x100;
            const int itemMagic = 0x200;

            var sizePos = writer.Position++;

            writer.WriteByte((int)Player.Appearance.Gender);
            writer.WriteByte((int)Player.Appearance.Overhead);

            /* 
             * todo : some equipped items conflict with body parts 
             * write body model if chest doesn't conceal the body
             * write head model if head item doesn't fully conceal the head.
             * write beard model if head item doesn't fully conceal the head.
             */
            for (var i = 0; i < equipSlotSize; i++)
            {
                if (!Player.Equipment[i].IsEmpty)
                    writer.WriteWord(Player.Equipment[i].ItemId - 1 + itemMagic);
                else if (Player.Appearance[i] != null)
                    writer.WriteWord(Player.Appearance[i].Value + plrObjMagic);
                else
                    writer.WriteByte(0);
            }

            writer.WriteByte(Player.Appearance.HairColor);
            writer.WriteByte(Player.Appearance.TorsoColor);
            writer.WriteByte(Player.Appearance.LegColor);
            writer.WriteByte(Player.Appearance.FeetColor);
            writer.WriteByte(Player.Appearance.SkinColor);

            // player animation indices
            writer.WriteWord(0x328); // standAnimIndex
            writer.WriteWord(0x337); // standTurnAnimIndex
            writer.WriteWord(0x333); // walkAnimIndex
            writer.WriteWord(0x334); // turn180AnimIndex
            writer.WriteWord(0x335); // turn90CWAnimIndex
            writer.WriteWord(0x336); // turn90CCWAnimIndex
            writer.WriteWord(0x338); // runAnimIndex

            writer.WriteQWord(StringToLong(Player.Username));
            writer.WriteByte(128); //cmb
            writer.WriteWord(0); // ...skill???

            writer.RawBuffer[sizePos] = (byte)(writer.Position - sizePos - 1);

            // todo : proper calculation of cmb lvl
        }

        //smh
        private static long StringToLong(string s)
        {
            var l = 0L;

            foreach (var c in s)
            {
                l *= 37L;
                if (c >= 'A' && c <= 'Z') l += 1 + c - 65;
                else if (c >= 'a' && c <= 'z') l += 1 + c - 97;
                else if (c >= '0' && c <= '9') l += 27 + c - 48;
            }

            while (l % 37L == 0L && l != 0L)
                l /= 37L;

            return l;
        }
    }
}