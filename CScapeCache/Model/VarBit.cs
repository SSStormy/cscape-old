﻿using CScapeCache.Core;

namespace CScapeCache.Model
{
    public class VarBit
    {
        public static int Count { get; private set; }

        public int Variable { get; private set; }
        public int LowerBit { get; private set; }
        public int UpperBit { get; private set; }
        public bool ABoolean651 { get; private set; }

        private VarBit()
        {
            ABoolean651 = false;
        }

        public static void UnpackVariables(CacheReader loadedCache, DataBuffer dataFile, CacheReader cache)
        {
            var cacheSize = dataFile.ReadShort();
            if (cache.VarBitCache == null)
                cache.VarBitCache = new VarBit[cacheSize];
            Count = cacheSize;
            for (var j = 0; j < cacheSize; j++)
            {
                if (cache.VarBitCache[j] == null)
                    cache.VarBitCache[j] = new VarBit();
                cache.VarBitCache[j].ReadVariable(dataFile);
                //if (cache[j].aBoolean651)
                //	Varp.cache[cache[j].anInt648].aBoolean713 = true;
            }
        }

        private void ReadVariable(DataBuffer stream)
        {
            do
            {
                int j = stream.ReadByte();
                if (j == 0)
                    return;
                if (j == 1)
                {
                    Variable = stream.ReadShort();
                    LowerBit = stream.ReadByte();
                    UpperBit = stream.ReadByte();
                }
                else if (j == 10)
                    stream.ReadString();
                else if (j == 2)
                    ABoolean651 = true;
                else if (j == 3)
                    stream.ReadInteger();
                else if (j == 4)
                    stream.ReadInteger();
            } while (true);
        }
    }
}