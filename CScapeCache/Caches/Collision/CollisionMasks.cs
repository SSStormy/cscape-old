﻿using System;

namespace CScapeCache.Caches.Collision
{
    [Flags]
    public enum CollisionMasks
    {
        Open = 0,
        Closed = 0xFFFFFF,
        Uninitialized = 0x1000000,
        Occupied = 0x100,
        Solid = 0x20000,
        Blocked = 0x200000,

        North = 0x2,
        East = 0x8,
        South = 0x20,
        West = 0x80,

        Northeast = 0x4,
        Southeast = 0x10,
        Southwest = 0x40,
        Northwest = 0x1,

        EastNorth = East | North,
        EastSouth = East | South,
        WestSouth = West | South,
        WestNorth = West | North,

        BlockedNorth = 0x400,
        BlockedEast = 0x1000,
        BlockedSouth = 0x4000,
        BlockedWest = 0x10000,

        BlockedNortheast = 0x800,
        BlockedSoutheast = 0x2000,
        BlockedNorthwest = 0x200,
        BlockedSouthwest = 0x8000,

        BlockedEastNorth = BlockedEast | BlockedNorth,
        BlockedEastSouth = BlockedEast | BlockedSouth,
        BlockedWestSouth = BlockedWest | BlockedSouth,
        BlockedWestNorth = BlockedWest | BlockedNorth
    }
}
