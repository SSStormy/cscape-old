﻿using CScapeCache.Caches;
using CScapeCache.Core;

namespace CScapeCache.Model
{
    public class ObjectModel
    {
        public GenericConfigCache<ObjectModel> Cache { get; }
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string[] Actions { get; private set; }
        public bool HasActions { get; private set; }
        public int GuessedType { get; private set; }
        public int[] SubObjectIDs { get; private set; }
        public int[] ModelRecolorIndexes { get; private set; }
        public int[] ModelRecolorColors { get; private set; }
        public sbyte LightIntensity { get; private set; }
        public sbyte LightDistance { get; private set; }
        public int[] ModelIndexes { get; private set; }
        public int ScaleX { get; private set; }
        public int ScaleY { get; private set; }
        public int ScaleZ { get; private set; }
        public int TranslationX { get; private set; }
        public int TranslationY { get; private set; }
        public int TranslationZ { get; private set; }
        public bool MirrorAlongX { get; private set; }
        public bool FlatShading { get; private set; }
        public int AnimationId { get; private set; }
        public int MinimapIcon { get; private set; }
        public bool RotateOnGround { get; private set; }
        public int TileSizeX { get; private set; }
        public int TileSizeY { get; private set; }
        public bool Clips { get; private set; }
        public bool ABoolean736 { get; private set; }
        public int MinimapFunction { get; private set; }
        public int AnInt749 { get; private set; }
        public bool ABoolean757 { get; private set; }
        public int AnInt760 { get; private set; }
        public bool ABoolean764 { get; private set; }
        public bool ABoolean766 { get; private set; }
        public int AnInt768 { get; private set; }
        public int AnInt774 { get; private set; }
        public int AnInt775 { get; private set; }
        public int[] AnIntArray776 { get; private set; }
        public bool ABoolean779 { get; private set; }

        internal ObjectModel(GenericConfigCache<ObjectModel> cache, int id)
        {
            Cache = cache;
            Id = id;
            ModelIndexes = null;
            AnIntArray776 = null;
            Name = null;
            Description = null;
            ModelRecolorIndexes = null;
            ModelRecolorColors = null;
            TileSizeX = 1;
            TileSizeY = 1;
            Clips = true;
            ABoolean757 = true;
            HasActions = false;
            RotateOnGround = false;
            FlatShading = false;
            ABoolean764 = false;
            AnimationId = -1;
            AnInt775 = 16;
            LightIntensity = 0;
            LightDistance = 0;
            Actions = null;
            MinimapFunction = -1;
            MinimapIcon = -1;
            MirrorAlongX = false;
            ABoolean779 = true;
            ScaleX = 128;
            ScaleY = 128;
            ScaleZ = 128;
            AnInt768 = 0;
            TranslationX = 0;
            TranslationY = 0;
            TranslationZ = 0;
            ABoolean736 = false;
            ABoolean766 = false;
            AnInt760 = -1;
            AnInt774 = -1;
            AnInt749 = -1;
            SubObjectIDs = null;
        }

        internal void ReadObject(DataBuffer stream)
        {
            var i = -1;

            do
            {
                int j;
                do
                {
                    j = stream.ReadByte();
                    if (j == 0)
                        goto Finalize;
                    if (j == 1)
                    {
                        int k = stream.ReadByte();
                        if (k > 0)
                        {
                            if (ModelIndexes == null)
                            {
                                AnIntArray776 = new int[k];
                                ModelIndexes = new int[k];
                                for (var k1 = 0; k1 < k; k1++)
                                {
                                    ModelIndexes[k1] = stream.ReadShort();
                                    AnIntArray776[k1] = stream.ReadByte();
                                }
                            }
                            else
                            {
                                stream.Location += k * 3;
                            }
                        }
                    }
                    else if (j == 2)
                        Name = stream.ReadString();
                    else if (j == 3)
                        Description = stream.ReadString();
                    else if (j == 5)
                    {
                        int l = stream.ReadByte();
                        if (l > 0)
                        {
                            if (ModelIndexes == null)
                            {
                                AnIntArray776 = null;
                                ModelIndexes = new int[l];
                                for (var l1 = 0; l1 < l; l1++)
                                    ModelIndexes[l1] = stream.ReadShort();
                            }
                            else
                            {
                                stream.Location += l * 2;
                            }
                        }
                    }
                    else if (j == 14)
                        TileSizeX = stream.ReadByte();
                    else if (j == 15)
                        TileSizeY = stream.ReadByte();
                    else if (j == 17)
                        Clips = false;
                    else if (j == 18)
                        ABoolean757 = false;
                    else if (j == 19)
                    {
                        i = stream.ReadByte();
                        if (i == 1)
                            HasActions = true;
                    }
                    else if (j == 21)
                        RotateOnGround = true;
                    else if (j == 22)
                        FlatShading = true;
                    else if (j == 23)
                        ABoolean764 = true;
                    else if (j == 24)
                    {
                        AnimationId = stream.ReadShort();
                        if (AnimationId == 65535)
                            AnimationId = -1;
                    }
                    else if (j == 28)
                        AnInt775 = stream.ReadByte();
                    else if (j == 29)
                        LightIntensity = stream.ReadSignedByte();
                    else if (j == 39)
                        LightDistance = stream.ReadSignedByte();
                    else if (j >= 30 && j < 39)
                    {
                        if (Actions == null)
                            Actions = new string[5];
                        Actions[j - 30] = stream.ReadString();
                        if (Actions[j - 30].ToLower().Equals("hidden"))
                            Actions[j - 30] = null;
                    }
                    else if (j == 40)
                    {
                        int i1 = stream.ReadByte();
                        ModelRecolorIndexes = new int[i1];
                        ModelRecolorColors = new int[i1];
                        for (var i2 = 0; i2 < i1; i2++)
                        {
                            ModelRecolorIndexes[i2] = stream.ReadShort();
                            ModelRecolorColors[i2] = stream.ReadShort();
                        }
                    }
                    else if (j == 60)
                        MinimapFunction = stream.ReadShort();
                    else if (j == 62)
                        MirrorAlongX = true;
                    else if (j == 64)
                        ABoolean779 = false;
                    else if (j == 65)
                        ScaleX = stream.ReadShort();
                    else if (j == 66)
                        ScaleY = stream.ReadShort();
                    else if (j == 67)
                        ScaleZ = stream.ReadShort();
                    else if (j == 68)
                        MinimapIcon = stream.ReadShort();
                    else if (j == 69)
                        AnInt768 = stream.ReadByte();
                    else if (j == 70)
                        TranslationX = stream.ReadSignedShort();
                    else if (j == 71)
                        TranslationY = stream.ReadSignedShort();
                    else if (j == 72)
                        TranslationZ = stream.ReadSignedShort();
                    else if (j == 73)
                        ABoolean736 = true;
                    else if (j == 74)
                    {
                        ABoolean766 = true;
                    }
                    else
                    {
                        if (j != 75)
                            continue;
                        AnInt760 = stream.ReadByte();
                    }
                } while (j != 77);

                AnInt774 = stream.ReadShort();
                if (AnInt774 == 65535)
                    AnInt774 = -1;
                AnInt749 = stream.ReadShort();
                if (AnInt749 == 65535)
                    AnInt749 = -1;
                int j1 = stream.ReadByte();
                SubObjectIDs = new int[j1 + 1];
                for (var j2 = 0; j2 <= j1; j2++)
                {
                    SubObjectIDs[j2] = stream.ReadShort();
                    if (SubObjectIDs[j2] == 65535)
                        SubObjectIDs[j2] = -1;
                }
            } while (true);

            Finalize:
            if (i == -1)
            {
                HasActions = ModelIndexes != null && (AnIntArray776 == null || AnIntArray776[0] == 10);
                if (Actions != null)
                    HasActions = true;
            }
            if (ABoolean766)
            {
                Clips = false;
                ABoolean757 = false;
            }
            if (AnInt760 == -1)
                AnInt760 = Clips ? 1 : 0;
        }
    }
}