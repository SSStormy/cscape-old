﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Helper;
using CScape.IO;
 
namespace CScape.Network.State
{
    public sealed class ConnectionInitStateHandler : IStateHandler
    {
        private enum ConnectionInitType
        {
            NormalLogin = 14,
            Reconnecting = 18
        }

        public const int ResposeSize = 17;

        public ConnectionContext Context { get; }

        public async Task Handle(RsPacketStream reader)
        {
            if (reader.Length <= 0)
            {
                Console.WriteLine($"Empty init packet from {Context.ConnectionId}");
                return;
            }

            var initType = (ConnectionInitType) reader.ReadSignedByte();

            switch (initType)
            {
                case ConnectionInitType.NormalLogin:
                case ConnectionInitType.Reconnecting:
                    byte ack = 0;

                    var builder = Context.Builder(ResposeSize);
                    var key = new byte[sizeof(long)];
                    StaticRandom.NextBytes(key);

                    builder.Fill(0, 8);
                    builder.WriteByte(ack);
                    builder.WriteBytes(key, 0, sizeof(long));

                    await Context.SendPacket(builder);

                    Context.Handler = new CredentialsStateHandler(Context, initType == ConnectionInitType.Reconnecting);
                    break;
                default:
                    Debug.Fail("Received unknown init type.");
                    break;
            }
        }

        public ConnectionInitStateHandler(ConnectionContext context)
        {
            Context = context;
        }
    }
}