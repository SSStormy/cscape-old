﻿using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    class NpcUpdateCleanTickEvent : IServerTickHandler
    {
        public bool ShouldFire => true;

        public void TickOccured()
        {
            
        }

        public ScapeServer Server { get; }

        public NpcUpdateCleanTickEvent(ScapeServer server)
        {
            Server = server;
        }

        public Task Process()
        {
            foreach (var npc in Server.NpcProvider.Npcs)
                npc.NpcSync.ResetAllFlags();

            return Task.CompletedTask;
        }
    }
}
