using CScape.Network.Synchronization.Messages;

namespace CScape.Network.Synchronization
{
    public interface ISyncMessageBuilder
    {
        ISynchronizationMessage BuildMessage();
    }
}