﻿using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Game.Container;
using CScape.IO;

namespace CScape.Network.Packet
{
    public enum ItemOptionType
    {
        Invalid = -1,
        Option1,
        Option2,
        Option3,
        Equip,
        Unequip
    }

    public sealed class ItemOptionPacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var iid = packetStream.ReadUnsignedWord();
            var slotId = packetStream.ReadUnsignedWord();
            var itemId = packetStream.ReadUnsignedWord();


            IContainer container;
            if (!context.Player.Containers.TryGetValue(iid, out container))
            {
                context.Logs.Warning(this, $"{context.Player} tried to use item in unregistered iid {iid}. Killing.");
                return PacketResult.KillConnection;
            }

            if (slotId < 0 || slotId >= container.Capacity)
            {
                context.Logs.Warning(this, $"{context.Player} tried to go over the capacity of container {iid} ({container.Capacity} by giving us slotId {slotId})");
                return PacketResult.AllOk;
            }

            var slot = container[slotId];

            if (slot.ItemId != itemId + 1)
            {
                context.Logs.Warning(this,
                    $"{context.Player} tried to use invalid itemId in iid {iid} at slotId {slotId}) by passing itemId {itemId} while it is {slot.ItemId}");
                return PacketResult.AllOk;
            }

            if(slot.IsEmpty)
                return PacketResult.AllOk;

            var handler = container as IOptionItemHandler;
            if (handler == null)
            {
                context.Logs.Warning(this,
                    $"{context.Player} tried to use itemId {itemId }in iid {iid} at slotId {slotId}) while the container does not implement IOptionItemHandler");
                return PacketResult.AllOk;
            }


            var opt = TranslateOpcodeToOptionIndex(opcode);
            if (opt == ItemOptionType.Invalid)
            {
                context.Logs.Warning(this,
                    $"{context.Player} tried to use itemId {itemId }in iid {iid} at slotId {slotId}) but translation of opcode {opcode} yielded invalid");
                return PacketResult.IncompleteRead;
            }

            if (handler.ShouldIgnoreOpAt[slotId])
                return PacketResult.AllOk;

            handler.ShouldIgnoreOpAt[slotId] = true;
            await handler.HandleOption(opt, slotId);

            return PacketResult.AllOk;
        }

        private ItemOptionType TranslateOpcodeToOptionIndex(byte opcode)
        {
            switch ((InPacketType) opcode)
            {
                case InPacketType.ItemOption1:
                    return ItemOptionType.Option1;
                case InPacketType.ItemOption2:
                    return ItemOptionType.Option2;
                case InPacketType.ItemOption3:
                    return ItemOptionType.Option3;
                case InPacketType.WearItem:
                    return ItemOptionType.Equip;
                case InPacketType.UnequipItem:
                    return ItemOptionType.Unequip;
                default:
                    Debug.Fail(
                        $"???? invalid opcode {opcode} passed to ItemOption opcode -> item option translator ??!?!?!?");
                    break;
            }
            return ItemOptionType.Invalid;
        }
    }
}
