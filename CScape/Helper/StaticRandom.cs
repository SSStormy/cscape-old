﻿using System;

namespace CScape.Helper
{
    /// <summary>
    ///     Thread-safe equivalent of System.Random, using just static methods.
    ///     If all you want is a source of random numbers, this is an easy class to
    ///     use. If you need to specify your own seeds (eg for reproducible sequences
    ///     of numbers), use System.Random.
    /// </summary>
    public static class StaticRandom
    {
        private static readonly Random Rng = new Random();
        private static readonly object MyLock = new object();

        /// <summary>
        ///     Returns a nonnegative random number.
        /// </summary>
        /// <returns>A 32-bit signed integer greater than or equal to zero and less than Int32.MaxValue.</returns>
        public static int Next()
        {
            lock (MyLock)
            {
                return Rng.Next();
            }
        }

        /// <summary>
        ///     Returns a nonnegative random number less than the specified maximum.
        /// </summary>
        /// <returns>
        ///     A 32-bit signed integer greater than or equal to zero, and less than maxValue;
        ///     that is, the range of return values includes zero but not maxValue.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">maxValue is less than zero.</exception>
        public static int Next(int max)
        {
            lock (MyLock)
            {
                return Rng.Next(max);
            }
        }

        /// <summary>
        ///     Returns a random number within a specified range.
        /// </summary>
        /// <param name="min">The inclusive lower bound of the random number returned. </param>
        /// <param name="max">
        ///     The exclusive upper bound of the random number returned.
        ///     maxValue must be greater than or equal to minValue.
        /// </param>
        /// <returns>
        ///     A 32-bit signed integer greater than or equal to minValue and less than maxValue;
        ///     that is, the range of return values includes minValue but not maxValue.
        ///     If minValue equals maxValue, minValue is returned.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">minValue is greater than maxValue.</exception>
        public static int Next(int min, int max)
        {
            lock (MyLock)
            {
                return Rng.Next(min, max);
            }
        }

        /// <summary>
        ///     Returns a random number between 0.0 and 1.0.
        /// </summary>
        /// <returns>A double-precision floating point number greater than or equal to 0.0, and less than 1.0.</returns>
        public static double NextDouble()
        {
            lock (MyLock)
            {
                return Rng.NextDouble();
            }
        }

        /// <summary>
        ///     Returns a random number between min and max.
        /// </summary>
        /// <returns>A double-precision floating point number greater than or equal to min, and less than max.</returns>
        public static double NextDouble(double min, double max)
        {
            lock (MyLock)
            {
                return min + (Rng.NextDouble() * (max - min));
            }
        }

        /// <summary>
        ///     Fills the elements of a specified array of shorts with random numbers.
        /// </summary>
        /// <param name="buffer">An array of shorts to contain random numbers.</param>
        /// <exception cref="ArgumentNullException">buffer is a null reference (Nothing in Visual Basic).</exception>
        public static void NextBytes(byte[] buffer)
        {
            lock (MyLock)
            {
                Rng.NextBytes(buffer);
            }
        }

        /// <summary>
        /// Rolls a dice 1-100 and checks if the rolled value is less or equal then the given chance.
        /// </summary>
        /// <returns>True if it rolled value is less or equal to chance. false if not.</returns>
        public static bool Roll(float chance)
        {
            lock (MyLock)
            {
                int val = Next(1, 101);
                return val <= chance;
            }
        }

        /// <summary>
        /// 50/50 chance to return a true or false bool.
        /// </summary>
        /// <returns>True or false.</returns>
        public static bool Bool()
        {
            lock (MyLock)
            {
                int val = Rng.Next(0, 2);
                return Convert.ToBoolean(val);
            }
        }
    }
}