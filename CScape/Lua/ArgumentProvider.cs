﻿using System;
using System.Collections.Generic;
using System.Linq;
using CScape.IO;
using CScape.Server;
using MoonSharp.Interpreter;

// ReSharper disable once CheckNamespace
namespace CScape.Lua
{ 
    public class ArgumentProvider
    {
        public const string ArgsTable = "PARAMS";

        public Dictionary<string, LuaArg> Args { get; }
        public int MinRequiredArgs { get; }

        public LuaScript Script { get; }

        private ArgumentProvider(Dictionary<string, LuaArg> args, int minReq, LuaScript script)
        {
            Args = args;
            MinRequiredArgs = minReq;
            Script = script;
        }

        public static ArgumentProvider Build(ScapeServer server, LuaScript script)
        {
            var logs = server.Logs;
            var args = script.Lua.Globals[ArgsTable] as Table;
            var minReq = 0;

            if (args == null)
                return null;

            var retval = new Dictionary<string, LuaArg>();

            foreach (var entry in args.Pairs)
            {
                if (entry.Key.Type != DataType.String ||
                    entry.Value.Type != DataType.UserData ||
                    !(entry.Value.UserData.Object is LuaArg))
                {
                    logs.Warning(typeof(ArgumentProvider),
                        $"Invalid ARGS type in {script.ScriptDirectory}: key: {entry.Key.ToPrintString()} val: {entry.Value.ToPrintString()}");
                    return null;
                }

                var arg = (LuaArg) entry.Value.UserData.Object;
                if (arg.ArgType == ArgumentType.Required)
                    minReq++;

                if (arg.ArgType == ArgumentType.Unparsed)
                {
                    if (arg.ValType != ArgumentValueType.String)
                    {
                        logs.Warning(typeof(ArgumentProvider), "Unparsed args can only strings.");
                        return null;
                    }
                }

                // do arg type checking.
                if (retval.Any())
                {
                    var last = retval.Last().Value.ArgType;

                    switch (last)
                    {
                        case ArgumentType.Required:
                            // ignored.
                            break;
                        case ArgumentType.Optional:
                            if (arg.ArgType != ArgumentType.Optional)
                                throw new InvalidOperationException(
                                    "Each argument after an optional argument must be optional.");
                            break;
                        case ArgumentType.Unparsed:
                            throw new InvalidOperationException("Unparsed arguments must be the last in line.");
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                retval.Add(entry.Key.String, arg);
            }
            return new ArgumentProvider(retval, minReq, script);
        }

        public bool TryParseArgs(string[] args, ref string err, ref Table result)
        {
            if (args.Length < MinRequiredArgs)
            {
                result = null;
                err = "Not enough arguments.";
                return false;
            }


            var table = new Table(Script.Lua);

            var argIndex = 0;
            var done = false;

            foreach (var arg in Args)
            {
                if (done)
                    break;

                switch (arg.Value.ArgType)
                {
                    case ArgumentType.Required:
                        if (!RegularParseArg(arg, args, ref argIndex, ref result, ref err, ref table))
                            return false;

                        break;
                    case ArgumentType.Optional:
                        if (argIndex >= args.Length)
                        {
                            if (arg.Value.ContainsDefaultValue)
                                table[arg.Key] = arg.Value.DefaultValue;

                            done = true;
                            break;
                        }

                        if (!RegularParseArg(arg, args, ref argIndex, ref result, ref err, ref table))
                            return false;

                        break;
                    case ArgumentType.Unparsed:
                        done = true;
                        table[arg.Key] = string.Join(" ", args);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            err = null;
            result = table;
            return true;
        }

        private bool RegularParseArg(KeyValuePair<string, LuaArg> arg, string[] args, 
            ref int argIndex, ref Table result, ref string err, ref Table retval)
        {
            var res = arg.Value.TryConverter(args[argIndex++]);
            if (!res.Item1)
            {
                result = null;
                err = $"Failed parsing argument {arg.Key}";
                return false;
            }
            retval[arg.Key] = res.Item2;
            return true;
        }
    }
}
