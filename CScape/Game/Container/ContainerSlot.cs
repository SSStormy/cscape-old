﻿using CScape.Game.Items;
using Newtonsoft.Json;

namespace CScape.Game.Container
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ContainerSlot
    {
        [JsonProperty]
        public ItemStack Item { get; set; }

        [JsonProperty]
        public int Index { get; }


        [JsonConstructor]
        public ContainerSlot(ItemStack item, int index)
        {
            Item = item;
            Index = index;
        }

        public ContainerSlot(int index)
        {
            Item = ItemStack.Empty;
            Index = index;
        }
    }
}