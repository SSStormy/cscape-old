using System.Diagnostics;
using CScape.Game.Container;
using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public class FullContainerUpdateMessage : ISynchronizationMessage
    {
        public IContainer Container { get; }

        public FullContainerUpdateMessage(IContainer container)
        {
            Container = container;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            if (Container.Items == 0)
                return new ClearContainerMessage(Container).GetPacket(receiver);

            var upperBound = 0;
            var endOfPayload = 0;

            var payload = new RsByteWriter(Container.Capacity * 7);

            for (var i = 0; i < Container.Capacity; i++)
            {
                var item = Container[i];

                if (item.Amount > byte.MaxValue)
                    payload.WriteDWord((int)item.Amount);
                else
                    payload.WriteByte((byte)item.Amount);

                payload.WriteWord(item.ItemId);

                if (!item.IsEmpty)
                {
                    upperBound = i;
                    endOfPayload = payload.Position;
                }
            }

            Debug.Assert(endOfPayload != 0);
            Debug.Assert(upperBound != 0);

            var header = new PacketBuilder(receiver.Player, 8 + endOfPayload);
            header.OpcodeAndSizePlaceholder(OutPacketType.SendItemChunk);
            header.WriteWord(Container.ContainerId);
            header.WriteWord(upperBound);

            payload.Position = endOfPayload;
            header.WriteBytes(payload.ToByteArray(false, false));
            header.WriteSize();
            return header;
        }
    }
}