using System;
using CScape.Game.Objects.Player;

namespace CScape.Network.Synchronization
{
    public sealed class PlayerSyncFlagContainer : ISyncFlagFacade
    {
        public Player Player { get; }

        public PlayerSyncFlags SyncFlags { get; set; }

        public PlayerSyncFlagContainer(Player player)
        {
            Player = player;
        }

        public void FacingTile()
        {
            SyncFlags |= PlayerSyncFlags.FacingTile;
            Player.Server.Logs.Debug(this, $"{Player} Flag container sync FacingTile");
        }

        public void ResetAllFlags()
        {
            SyncFlags = 0;
        }
    }
}