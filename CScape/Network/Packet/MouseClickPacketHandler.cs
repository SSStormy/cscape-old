﻿using System.Threading.Tasks;
using CScape.Helper;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class MouseClickPacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var dword = packetStream.ReadDWord();
            var isRightClick = Utils.IsBitSet(dword, 19);

            return PacketResult.AllOk;
        }
    }
}
