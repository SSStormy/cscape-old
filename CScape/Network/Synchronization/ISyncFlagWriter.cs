using System;
using CScape.IO;

namespace CScape.Network.Synchronization
{
    public interface ISyncFlagWriter
    {
        /// <summary>
        /// Locally (only for this writer) sets the flags that should be synchronized when the 
        /// observer sees this facade for the first time.
        /// </summary>
        void SetFirstContactValues();
        int SyncFlags { get; }
        bool NeedsWriting { get; }
        void Write(RsByteWriter writer);
        void UnsetLocal(int flag);
        void SetLocal(int flag);
    }
}
 