﻿using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.State
{
    public interface IStateHandler
    {
        ConnectionContext Context { get; }
        Task Handle(RsPacketStream packetStream);
    }
}
