﻿using CScape.Network.Synchronization;
using Newtonsoft.Json;

namespace CScape.Game.Objects.Player
{
    public enum OverheadType : byte
    {
        None = 0
        // todo : overheads
    }

    [JsonObject(MemberSerialization.OptIn)]
    public sealed class PlayerAppearance
    {
        public enum GenderType
        {
            Male,
            Female
        }

        public const int ChestIndex = 4;
        public const int ArmIndex = 6;
        public const int LegIndex = 7;
        public const int HeadIndex = 8;
        public const int HandIndex = 9;
        public const int FeetIndex = 10;
        public const int BeardIndex = 11;

        public int? this[int index]
        {
            get
            {
                switch (index)
                {
                    case ChestIndex:
                        return Chest;
                    case ArmIndex:
                        return Arms;
                    case LegIndex:
                        return Legs;
                    case HeadIndex:
                        return Head;
                    case HandIndex:
                        return Hands;
                    case FeetIndex:
                        return Feet;
                    case BeardIndex:
                        if (Gender == GenderType.Male)
                            return Beard;
                        return null;
                    default:
                        return null;
                }
            }
        }

        public int Head
        {
            get { return _head; }
            set { SetValue(value, ref _head); }
        }

        public int Chest
        {
            get { return _chest; }
            set { SetValue(value, ref _chest); }
        }

        public int Arms
        {
            get { return _arms; }
            set { SetValue(value, ref _arms); }
        }

        public int Hands
        {
            get { return _hands; }
            set { SetValue(value, ref _hands); }
        }

        public int Legs
        {
            get { return _legs; }
            set { SetValue(value, ref _legs); }
        }

        public int Feet
        {
            get { return _feet; }
            set { SetValue(value, ref _feet); }
        }

        public int Beard
        {
            get { return _beard; }
            set { SetValue(value, ref _beard); }
        }

        public GenderType Gender
        {
            get { return _gender; }
            set { SetValue(value, ref _gender); }
        }

        public OverheadType Overhead
        {
            get { return _overhead; }
            set { SetValue(value, ref _overhead); }
        }

        public int SkinColor
        {
            get { return _skinColor; }
            set { SetValue(value, ref _skinColor); }
        }

        public int HairColor
        {
            get { return _hairColor; }
            set { SetValue(value, ref _hairColor); }
        }

        public int TorsoColor
        {
            get { return _torsoColor; }
            set { SetValue(value, ref _torsoColor); }
        }

        public int LegColor
        {
            get { return _legColor; }
            set { SetValue(value, ref _legColor); }
        }

        public int FeetColor
        {
            get { return _feetColor; }
            set { SetValue(value, ref _feetColor); }
        }

        public Player Player { get; set; }

        [JsonProperty]
        private int _arms;
        [JsonProperty]
        private int _beard;
        [JsonProperty]
        private int _chest;
        [JsonProperty]
        private int _feet;
        [JsonProperty]
        private int _feetColor;


        [JsonProperty]
        private GenderType _gender;
        [JsonProperty]
        private int _hairColor;
        [JsonProperty]
        private int _hands;
        [JsonProperty]
        private int _head;
        [JsonProperty]
        private int _legColor;
        [JsonProperty]
        private int _legs;
        [JsonProperty]
        private OverheadType _overhead;
        [JsonProperty]
        private int _skinColor;
        [JsonProperty]
        private int _torsoColor;

        private PlayerAppearance()
        {
            
        }

        public PlayerAppearance(Player player)
        {
            Player = player;
        }

        public static PlayerAppearance Default(Player player)
            => new PlayerAppearance(player)
            {
                _gender = GenderType.Male,
                _hairColor = 0,
                _torsoColor= 0,
                _legColor=  0,
                _feetColor= 0,
                _skinColor = 0,
               _head = 0,
                _chest = 18,
                _arms = 26,
                _hands = 33,
                _legs = 36,
                _feet = 42,
                _beard = 10,
            };

        private void SetValue<T>(T value, ref T field)
        {
            if (value.Equals(field))
                return;

            field = value;
            Player.PlayerSync.SyncFlags |= PlayerSyncFlags.Appearance;
        }
    }
}