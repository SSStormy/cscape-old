using CScape.IO;

namespace CScape.Network.Synchronization.Segments
{
    public sealed class EntityRemoveSegment : IUpdateSegment
    {
        public void WriteInto(PacketBuilder packet)
        {
            packet.WriteBits(1, 1);
            packet.WriteBits(2, 3);
        }
    }
}