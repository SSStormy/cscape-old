﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using CScape.Server;
using Newtonsoft.Json;
using Nito.AsyncEx;

namespace CScape_devenv
{
    class Program
    {
        private static readonly BlockingCollection<LogManager.LogDataEventArgs> LogQueue 
            = new BlockingCollection<LogManager.LogDataEventArgs>();

        private static ScapeServer _server;

        static void Main()
        {
            // load config from json
            const string configFilename = "Meta//server-config.json";
            var config = JsonConvert.DeserializeObject<ServerConfig>(File.ReadAllText(configFilename));

            // start server
            _server = new ScapeServer(config, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 43594));

            _server.Logs.LogReceived += (s, l) => LogQueue.Add(l);
            ThreadPool.QueueUserWorkItem(o =>
            {
                foreach (var log in LogQueue.GetConsumingEnumerable())
                    WriteLog(log);
            });

            AsyncContext.Run(async () => await _server.StartServing());
        }

        private static void WriteLog(LogManager.LogDataEventArgs log)
        {
            var time = log.Time - _server.StartTime;
#if DEBUG
            if (log.LogType == LogManager.LogTypeEnum.Debug)
                WriteIntoDelegate(w => Debug.Write(w), log, time.TotalSeconds);
            else
#endif
                WriteIntoDelegate(Console.Write, log, time.TotalSeconds);
        }

        private static void WriteIntoDelegate(Action<string> writeDel, LogManager.LogDataEventArgs l, double sec)
        {
            string header = $"[{sec,4:N6}] ";
            writeDel(header + l.Message);

            if (l.Exception != null)
            {
                writeDel(Environment.NewLine);
                writeDel(new string(' ', header.Length+1) + $"-> Exception: {l.Exception}");
            }

            writeDel(Environment.NewLine);
        }
    }
}
