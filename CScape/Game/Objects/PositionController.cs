﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CScape.Helper;
using CScape.Server;
using CScapeCache.Caches.Collision;
using Imms;

namespace CScape.Game.Objects
{
    public enum MovementType : byte
    {
        NoUpdate = 0,
        Walking = 1,
        Running = 2,
        PostTeleport = 3
    }

    public enum MoveDirectionType
    {
        None = -1,
        NorthWest = 0,
        North = 1,
        NorthEast = 2,
        West = 3,
        East = 4,
        SouthWest = 5,
        South = 6,
        SouthEast = 7
    }

    public abstract class PositionController
    {
        public static readonly Dictionary<MoveDirectionType, MoveDirectionType> Mirror = new Dictionary<MoveDirectionType, MoveDirectionType>()
        {
            {MoveDirectionType.None, MoveDirectionType.None },
            { MoveDirectionType.North, MoveDirectionType.South },
            { MoveDirectionType.East, MoveDirectionType.West},
            { MoveDirectionType.South,MoveDirectionType.North },
            { MoveDirectionType.West, MoveDirectionType.East},

            { MoveDirectionType.NorthEast, MoveDirectionType.SouthWest},
            { MoveDirectionType.SouthEast, MoveDirectionType.NorthWest },
            { MoveDirectionType.NorthWest, MoveDirectionType.SouthEast },
            { MoveDirectionType.SouthWest, MoveDirectionType.NorthEast },
        };

        public class FollowController
        {
            private IEntity _followedEntity;
            private PositionKey _prevFollowedEntPos;

            public PositionController Position { get; }

            public IEntity FollowedEntity
            {
                get { return _followedEntity; }
                set
                {
                    if (value == null)
                    {
                        _followedEntity = null;
                        _prevFollowedEntPos = null;
                    }
                    else
                    {
                        _followedEntity = value;
                    }
                }
            }

            public FollowController(PositionController pos)
            {
                Position = pos;
            }

            public void GenerateMovement()
            {
                if (Position.HasProcessedThisTick)
                    return;

                if (_followedEntity == null)
                    return;

                if (!_followedEntity.Position.IsInLocalRange(Position) ||
                    _followedEntity.Position.GlobalZ != Position.GlobalZ)
                {
                    FollowedEntity = null;
                    return;
                }

                Position.SyncSetFacingTile(_followedEntity.Position.GlobalX, _followedEntity.Position.GlobalY);

                if (_prevFollowedEntPos == null || 
                    !_prevFollowedEntPos.Equals(_followedEntity.Position.GlobalPos))
                {
                    /*
                     * The smarter way of doing this (i.e. not resetting queued movements)
                     * results in errors in the direction representation of deltas.
                     *
                     * All combined movement directions towards a goal not 
                     * always get the player to the goal but the deltas and 
                     * the expected coordinates of each waypoint do.
                     * 
                     *   Reproduction :
                     * Player A follows Player B.
                     * Player B runs around the map while Player A walk-follows B.
                     * After a lot of running, Player B stops.
                     * The combined movement directions of player A towards Player B will most frequently be invalid
                     * but the movement deltas and expected positions will not, therefore
                     * the position of player A is exactly behind player B on the server side
                     * while on the client side, for both players, player A is somewhere else
                     * completely. 
                     * 
                     * Possibly caused by the call to GetDirection resulting in a direction
                     * that doesn't accurately represent the delta?
                     * 
                     * -- 
                     * Further testing shows that Expected locals are good
                     * and the deltas are good. Only the direction is bad.
                     */

                    if (Position.QueuedMoveData.Any())
                        Position.QueuedMoveData = ImmList<MovementData>.Empty;

                    _prevFollowedEntPos = _followedEntity.Position.GlobalPos;

                    // translate the global position of the target into our entities local plane
                    var delta = GetDelta(Mirror[_followedEntity.Position.Dir1]);
                    var lx = (_followedEntity.Position.GlobalX - Position.GlobalX) + Position.LocalX + delta.Item1;
                    var ly = (_followedEntity.Position.GlobalY - Position.GlobalY) + Position.LocalY + delta.Item2;

                    // gen
                    Position.GenerateInterpolationForLocalDelta(lx, ly);
                }
            }
        }

        public ScapeServer Server { get; }
        public IEntity ParentEntity { get; }
        public World World => Server.World;
        public LogManager Logs => Server.Logs;

        public const MoveDirectionType DefaultFacingDirection = MoveDirectionType.South;

        public const int MaxTiles = 50;

        private const int MapMatrixSize = 3;

        protected ImmList<MovementData> QueuedMoveData { get; set; } = ImmList<MovementData>.Empty;

        protected bool HasProcessedThisTick { get; set; }

        protected Region[,] MapMatrix { get; private set; } = new Region[MapMatrixSize, MapMatrixSize];

        public int GlobalX { get; protected set; }
        public int GlobalY { get; protected set; }
        public int GlobalZ { get; protected set; }

        public PositionKey GlobalPos => new PositionKey(GlobalX, GlobalY, GlobalZ);

        public int LocalX { get; protected set; }
        public int LocalY { get; protected set; }

        public int RegionX { get; protected set; }
        public int RegionY { get; protected set; }

        public bool IsRegionDirty { get; set; }

        public virtual bool IsRunning { get; set; }

        public MovementType MoveType { get; protected set; }

        public MoveDirectionType Dir1 { get; protected set; } = MoveDirectionType.South;
        public MoveDirectionType Dir2 { get; protected set; }

        public bool DidJustTeleport { get; protected set; }
        public bool DidJustMove { get; protected set; }

        public FollowController Follow { get; }

        public int FacingX { get; set; }
        public int FacingY { get; set; }

        private static readonly MoveDirectionType[,] DirectionMatrix =
        {
            {MoveDirectionType.NorthEast, MoveDirectionType.East, MoveDirectionType.SouthEast},
            {MoveDirectionType.North, MoveDirectionType.None, MoveDirectionType.South},
            {MoveDirectionType.NorthWest, MoveDirectionType.West, MoveDirectionType.SouthWest}
        };

        public static Tuple<int, int> GetDelta(MoveDirectionType dir)
        {
            /*
             *  N =  0  1
             *  S =  0 -1
             *  W = -1  0 
             *  E =  1  0
            */
            const int dn = 1;
            const int ds = -1;
            const int dw = -1;
            const int de = 1;

            switch (dir)
            {
                case MoveDirectionType.None:
                    return Tuple.Create(0, 0);
                case MoveDirectionType.NorthWest:
                    return Tuple.Create(dw, dn);
                case MoveDirectionType.North:
                    return Tuple.Create(0, dn);
                case MoveDirectionType.NorthEast:
                    return Tuple.Create(de, dn);
                case MoveDirectionType.West:
                    return Tuple.Create(dw, 0);
                case MoveDirectionType.East:
                    return Tuple.Create(de, 0);
                case MoveDirectionType.SouthWest:
                    return Tuple.Create(dw, ds);
                case MoveDirectionType.South:
                    return Tuple.Create(0, ds);
                case MoveDirectionType.SouthEast:
                    return Tuple.Create(de, ds);
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }

        public static MoveDirectionType GetDirection(int deltaX, int deltaY)
        {
            deltaX = Utils.Clamp(++deltaX, 0, 2);
            deltaY = Utils.Clamp(++deltaY, 0, 2);

            return DirectionMatrix[deltaX, deltaY];
        }

        public PositionController(ScapeServer server, IEntity parentEntity)
        {
            Server = server;
            ParentEntity = parentEntity;
            Follow = new FollowController(this);
        }

        public virtual void PostUpdate()
        {
       //     IsRegionDirty = false;
            HasProcessedThisTick = false;
            DidJustMove = false;
            DidJustTeleport = false;
        }

        public void ResetWaypointQueue()
        {
            _stopInterpGen = true;
            Follow.FollowedEntity = null;
            this.SyncSetFaceTileInDirection(DefaultFacingDirection);

            if (QueuedMoveData.Any())
                QueuedMoveData = ImmList<MovementData>.Empty;
        }

        public async Task Teleport(int posX, int posY, int posZ)
        {
            GlobalZ = posZ;
            await Teleport(posX, posY);
        }

        public async Task Teleport(int posX, int posY)
        {
            IsRegionDirty = true;
            if (RegionX != -1 && RegionY != -1)
            {
                int relX = posX - RegionX*8, relY = posY - RegionY*8;
                if (relX >= 2*8 && relX < 11*8 && relY >= 2*8 && relY < 11*8)
                    IsRegionDirty = false;
            }
            if (IsRegionDirty)
            {
                RegionX = (posX >> 3) - 6;
                RegionY = (posY >> 3) - 6;
            }
            LocalX = posX - 8*RegionX;
            LocalY = posY - 8*RegionY;
            GlobalX = posX;
            GlobalY = posY;
            ResetWaypointQueue();

            if (IsRegionDirty)
                RecalcMaps();

            await SetTeleportState();
        }

        public virtual Task SetTeleportState()
        {
            DidJustTeleport = true;
            ResetWaypointQueue();
            QueuedMoveData = QueuedMoveData.AddLast(MovementData.Teleport());
            return Task.CompletedTask;
        }

        private readonly object _moveReqLock = new object();
        private bool _stopInterpGen;

        public virtual Task<bool> SubmitMovementRequest(List<int> wayX, List<int> wayY)
        {
            ResetWaypointQueue();

            Debug.Assert(wayX.Count == wayY.Count);

            lock (_moveReqLock)
            {
                for (var i = 0; i < wayX.Count; i++)
                    GenerateInterpolationForLocalDelta(wayX[i], wayY[i]);
            }

            if (QueuedMoveData.Length >= MaxTiles)
            {
                ResetWaypointQueue();
                return Task.FromResult(false);
            }

            return Task.FromResult(true);
        }

        public int GetMaxDelta(PositionController end)
            => Math.Max(Math.Abs(GlobalX - end.GlobalX), 
                Math.Abs(GlobalY - end.GlobalY));

        private void GenerateInterpolationForLocalDelta(int tolx, int toly)
        {
            _stopInterpGen = false;
            int deltaX;
            int deltaY;

            if (QueuedMoveData.Any())
            {
                var data = QueuedMoveData.Last();
                deltaX = data.ExpectedX - tolx;
                deltaY = data.ExpectedY - toly;
            }
            else
            {
                deltaX = LocalX - tolx;
                deltaY = LocalY - toly;
            }

            // translate deltas into a direction.
            var dir = GetDirection(deltaX, deltaY);

            // find max moves to first waypoint.
            var dxAbs = Math.Abs(deltaX);
            var dyAbs = Math.Abs(deltaY);
            var mvsTofsWaypoint = Math.Max(dxAbs, dyAbs);

            for (var i = 0; i < mvsTofsWaypoint; i++)
            {
                var stepX = 0;
                var stepY = 0;

                if (dxAbs > 0)
                {
                    stepX = -Math.Sign(deltaX);
                    dxAbs--;
                }

                if (dyAbs > 0)
                {
                    stepY = -Math.Sign(deltaY);
                    dyAbs--;
                }

                MovementData goal;

                if (QueuedMoveData.Any())
                {
                    var last = QueuedMoveData.Last;
                    goal = MovementData.Walk(dir, stepX, stepY, QueuedMoveData.Last().ExpectedX + stepX, QueuedMoveData.Last().ExpectedY + stepY);

                    if (!CanMove(new PositionKey((RegionX << 3) + last.ExpectedX, (RegionY << 3) + last.ExpectedY, GlobalZ), new PositionKey((RegionX << 3) + goal.ExpectedX, (RegionY << 3) + goal.ExpectedY, GlobalZ), dir))
                    {
                            ResetWaypointQueue();
                        return;
                    }
                }
                else
                {
                    goal = MovementData.Walk(dir, stepX, stepY, LocalX + stepX, LocalY + stepY);

                    if (!CanMove(GlobalPos, new PositionKey((RegionX << 3) + goal.ExpectedX, (RegionY << 3) + goal.ExpectedY, GlobalZ), dir))
                    {
                            ResetWaypointQueue();
                        return;
                    }
                }

                if (_stopInterpGen)
                    break;

                QueuedMoveData = QueuedMoveData.AddLast(goal);
            }
            _stopInterpGen = false;
        }

        protected bool CanMove(PositionKey currentPos, PositionKey target, MoveDirectionType direction)
        {
            var currColl = Server.World.GetCollision(currentPos);
            var targetColl = Server.World.GetCollision(target);
            /*
            Logs.Normal(this, $"direction: {direction}");
            Logs.Normal(this, $"currColl: {currColl.CollisionMask}");
            Logs.Normal(this, $"targetColl: {targetColl.CollisionMask}");
            */
            if (currColl.IsCompletely(CollisionMasks.Open) && targetColl.IsCompletely(CollisionMasks.Open))
                return true;

            if (currColl.IsBlocked || targetColl.IsBlocked)
                return false;

            if (CannotMoveInContextOf(direction, currColl))
                return false;

            /*
             *  _ = wall
             *   
             *  ...
             *  ... = open
             *  ...
             *  
             *    ... ... ...
             *  1 ... ... ...
             *    ___ ___ ___
             *  
             *    ... ___ ...
             *  0 ... ... ...
             *    ... ... ...
             *    
             *  If the player is in row 0 the server will allow them to move
             *  vertically into row 1.
             *  
             *  To counteract this, we need to mirror the blocDir and after 
             *  we check if blocDir is blocked in currColl, we
             *  check if the mirror of blocDir is blocked in targetColl
             */

            if (CannotMoveInContextOf(Mirror[direction], targetColl))
                return false;

            return true;
        }

        private bool CannotMoveInContextOf(MoveDirectionType dir, CollisionData context)
        {
            if (dir == MoveDirectionType.North || dir == MoveDirectionType.NorthEast || dir == MoveDirectionType.NorthWest)
            {
                if (context.Has(CollisionMasks.BlockedNorth))
                    return true;
            }

            if (dir == MoveDirectionType.South || dir == MoveDirectionType.SouthEast || dir == MoveDirectionType.SouthWest)
            {
                if (context.Has(CollisionMasks.BlockedSouth))
                    return true;
            }

            if (dir == MoveDirectionType.East || dir == MoveDirectionType.SouthEast || dir == MoveDirectionType.NorthEast)
            {
                if (context.Has(CollisionMasks.BlockedEast))
                    return true;
            }

            if (dir == MoveDirectionType.West || dir == MoveDirectionType.SouthWest || dir == MoveDirectionType.NorthWest)
            {
                if (context.Has(CollisionMasks.BlockedWest))
                    return true;
            }

            if (dir == MoveDirectionType.SouthEast)
                return context.Has(CollisionMasks.BlockedSoutheast);

            if (dir == MoveDirectionType.NorthEast)
                return context.Has(CollisionMasks.BlockedNortheast);

            if (dir == MoveDirectionType.NorthWest)
                return context.Has(CollisionMasks.BlockedNorthwest);

            if (dir == MoveDirectionType.SouthWest)
                return context.Has(CollisionMasks.BlockedSouthwest);

            return false;
        }

        protected MoveDirectionType ApplyInterpolation(MovementData data)
        {
            LocalX += data.MoveDeltaX;
            LocalY += data.MoveDeltaY;

            GlobalX += data.MoveDeltaX;
            GlobalY += data.MoveDeltaY;

            this.SyncSetFaceTileInDirection(data.Direction);

            int deltaX = 0, deltaY = 0;
            if (LocalX < 2*8)
            {
                deltaX = 4*8;
                RegionX -= 4;
                IsRegionDirty = true;
            }
            else if (LocalX >= 11*8)
            {
                deltaX = -4*8;
                RegionX += 4;
                IsRegionDirty = true;
            }
            if (LocalY < 2*8)
            {
                deltaY = 4*8;
                RegionY -= 4;
                IsRegionDirty = true;
            }
            else if (LocalY >= 11*8)
            {
                deltaY = -4*8;
                RegionY += 4;
                IsRegionDirty = true;
            }

            if (IsRegionDirty)
            {
                LocalX += deltaX;
                LocalY += deltaY;
            }

            RecalcMaps();

            DidJustMove = true;
            return data.Direction;
        }

        private int _prevMapX;
        private int _prevMapY;

        protected void RecalcMaps()
        {
            /*
             * Construct the new matrix
             */

            var center = World.GetByTileCoordinate(GlobalX, GlobalY);
            if (center.X == _prevMapX && center.Y == _prevMapY)
                return;

            _prevMapX = center.X;
            _prevMapY = center.Y;

            var oldMatrix = MapMatrix;
            MapMatrix = new Region[MapMatrixSize, MapMatrixSize];
            MapMatrix[1, 1] = center;

            // enter top map
            MapMatrix[0, 1] = World.GetByMapCoordinate(center.X, center.Y + 1);
            // enter bottom map
            MapMatrix[2, 1] = World.GetByMapCoordinate(center.X, center.Y - 1);

            // enter right map
            MapMatrix[1, 2] = World.GetByMapCoordinate(center.X + 1, center.Y);

            // enter left map
            MapMatrix[1, 0] = World.GetByMapCoordinate(center.X - 1, center.Y);

            /*
             * Handle the maps in the corners
             */

            // top-right map
            HandleCorner(1, 2, 0, 1, 0, 2, center.X + 1, center.Y + 1);

            // bottom-right
            HandleCorner(2, 1, 1, 2, 2, 2, center.X + 1, center.Y - 1);

            // bottom-left
            HandleCorner(2, 1, 1, 0, 2, 0, center.X - 1, center.Y - 1);

            // top-left
            HandleCorner(1, 0, 1, 1, 0, 0, center.X - 1, center.Y + 1);

            /*
             * Compare the new and the old map matrices 
             */

            for (var x = 0; x < MapMatrixSize; x++)
            {
                for (var y = 0; y < MapMatrixSize; y++)
                {
                    var curMap = MapMatrix[x, y];
                    var oldMap = oldMatrix[x, y];

                    if (curMap == null)
                    {
                        if (oldMap != null)
                            LeaveMap(oldMap);
                    }
                    else
                    {
                        if (oldMap != null)
                        {
                            if (!curMap.Equals(oldMap))
                                EnterMap(curMap);
                        }
                        else
                            EnterMap(curMap);
                    }
                }
            }
        }

        protected abstract void EnterMap(Region map);
        protected abstract void LeaveMap(Region map);

        /*
         * l - local
         * h - horizontal
         * v - vertical
         * t - target
         */

        public void LeaveAllMaps()
        {
            foreach (var map in MapMatrix)
            {
                if (map == null)
                    continue;

                LeaveMap(map);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void HandleCorner(int lhX, int lhY, int lvX, int lvY, int ltX, int ltY, int offX, int offY)
        {
            if (MapMatrix[lhX, lhY] != null && MapMatrix[lvX, lvY] != null)
            {
                MapMatrix[ltX, ltY] = World.GetByMapCoordinate(offX, offY);
            }
        }

        public abstract void ProcessMovement();

        protected sealed class MovementData
        {
            public MovementType MoveType { get; }

            public MoveDirectionType Direction { get; }

            public int MoveDeltaX { get; }
            public int MoveDeltaY { get; }

            public int ExpectedX { get; }
            public int ExpectedY { get; }

            private MovementData(MovementType moveType, MoveDirectionType currentDirection = MoveDirectionType.None, int moveDeltaX = 0, int moveDeltaY = 0, int ex = 0, int ey = 0)
            {
                MoveType = moveType;
                MoveDeltaX = moveDeltaX;
                MoveDeltaY = moveDeltaY;
                ExpectedX = ex;
                ExpectedY = ey;
                Direction = currentDirection;
            }

            public static MovementData Teleport() => new MovementData(MovementType.PostTeleport);

            public static MovementData Walk(MoveDirectionType direction, int deltaX, int deltaY, int ex, int ey) => new MovementData(MovementType.Walking, direction, deltaX, deltaY, ex, ey);
        }
    }
}