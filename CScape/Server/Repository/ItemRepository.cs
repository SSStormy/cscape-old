﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Items;
using Newtonsoft.Json;

namespace CScape.Server.Repository
{
    public sealed class ItemRepository : IRepository<ItemDef, int>
    {
        public LogManager Logs => Server.Logs;

        public string ItemDefDir { get; set; }

        private ItemDef[] _itemDefs;
        public ScapeServer Server { get; }

        public ItemDef Get(int key)
        {
            key--;
            if (key < 0 || key >= _itemDefs.Length)
                return null;

            return _itemDefs[key];
        }


        public IEnumerable<ItemDef> SearchFor(Func<ItemDef, bool> predicate)
            => _itemDefs.Where(predicate);

        public IEnumerable<ItemDef> GetAll()
            => Array.AsReadOnly(_itemDefs);

        public Task Load()
        {
            InternalLoad();
            return Task.CompletedTask;
        }

        private void InternalLoad()
        {
            if (!File.Exists(ItemDefDir))
            {
                Logs.Warning(this, $"Itemdefs not found at {ItemDefDir}");
                _itemDefs = new ItemDef[0];
                return;
            }
            // newtonsoft.json doesn't expose any async methods smh
            _itemDefs = JsonConvert.DeserializeObject<ItemDef[]>(File.ReadAllText(ItemDefDir));
        }

        public ItemRepository(ScapeServer server)
        {
            Server = server;
            ItemDefDir = server.Config.ItemDefDir;
        }
    }
}