﻿using System;
using System.Collections.Generic;

namespace CScapeCache.Caches
{
    public abstract class AbstractCache
    {
        public CacheReader Reader { get; }
        public bool IsLoaded { get; private set; }

        public AbstractCache(CacheReader reader)
        {
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            Reader = reader;
        }

        protected void ThrowIfNotLoaded()
        {
            if (!IsLoaded)
                throw new InvalidOperationException("You must load this cache first before accessing it.");
        }

        protected abstract void InternalLoad();

        public void Load()
        {
            if(IsLoaded)
                throw new InvalidOperationException("Cannot load cache again after it's been loaded.");
            IsLoaded = true;
            InternalLoad();
        }
    }

    public abstract class AbstractLookupCache<TKey, TVal> : AbstractCache
    {
        protected Dictionary<TKey, TVal> ValueLookup { get; set; } = new Dictionary<TKey, TVal>();

        public IEnumerable<TVal> All => ValueLookup.Values;

        public int Total => ValueLookup.Count;

        public AbstractLookupCache(CacheReader reader) : base(reader)
        {
        }
    }
}
