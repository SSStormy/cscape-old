﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Network;
using CScape.Server;

namespace CScape.Game.Interface
{
    public sealed class PlayerInterfaces
    {
        public Player Player { get; }
        public ScapeServer Server => Player.Server;
        public LogManager Logs => Server.Logs;

        public const int MaxSidebarInterfaceAmount = 14;

        public MainInterfaceController MainInterface { get; set; }
        public WalkableInterfaceController WalkableInterface { get; set; }

        public SidebarInterfaceControler[] SideInterfaces { get; }
        = new SidebarInterfaceControler[MaxSidebarInterfaceAmount];

        public bool CanOpenMainInterface()
        {
            if (MainInterface != null)
            {
                Player.SendMessage("You have a conflicting interface open.");
                return false;
            }
            return true;
        }

        public async Task HandleButton(int buttonId)
        {
            if (MainInterface != null)
                await MainInterface.HandleButton(buttonId);
            if (WalkableInterface != null)
                await WalkableInterface.HandleButton(buttonId);
            foreach (var side in SideInterfaces)
            {
                if (side == null)
                    continue;
                await side.HandleButton(buttonId);
            }
        }

        public async Task<MainInterfaceController> ShowMain(IShowableInterface config, 
            bool showImmediate = true, bool forceShow = false)
        {
            if (!await CanShow(forceShow, MainInterface))
                return null;

            MainInterface = new MainInterfaceController(this, config);

            if(showImmediate)
                await MainInterface.Show();
            SetupCanBeCloseable(MainInterface, config);

            return MainInterface;
        }

        private async Task GenericCloseableHandler(ICloseableInterface close, InterfaceController controller)
        {
            var callbackOverride = close as ICloseCallbackOverride;
            var callback = close as ICloseCallback;

            if (callbackOverride != null)
                await callbackOverride.CloseCallbackOverride(Player);
            else
            {
                await controller.Close();

                if (callback != null)
                    await callback.CloseCallback(Player);
            }
        }

        private void SetupCanBeCloseable(InterfaceController controller, IShowableInterface config)
        {
            var close = config as ICloseableInterface;
            if (close == null)
                return;

            controller.ButtonHandlersAsync.TryAdd(close.CloseButtonId,
                async () => await GenericCloseableHandler(close, controller));
        }

        public async Task<MainInterfaceController> ShowMainAndTab(IShowMainAndTabInterfaceConfig config,
            bool showImmediate = true, bool forceShow = false)
        {
            if (!await CanShow(forceShow, MainInterface))
                return null;

            MainInterface = new MainInterfaceController(this, config);

            if(showImmediate)
                await MainInterface.Show();
            SetupCanBeCloseable(MainInterface, config);

            return MainInterface;
        }

        public async Task<WalkableInterfaceController> ShowWalkable(IShowableInterface config,
            bool showImmediate = true, bool forceShow = false)
        {
            if (!await CanShow(forceShow, WalkableInterface))
                return null;

            WalkableInterface = new WalkableInterfaceController(this, config);

            if(showImmediate)
                await WalkableInterface.Show();
            SetupCanBeCloseable(WalkableInterface, config);
            

            return WalkableInterface;
        }

        public async Task<SidebarInterfaceControler> ShowSidebar(IShowSidebarInterfaceConfig config,
            bool showImmediate = true, bool forceShow = false)
        {
            if (config.TabIndex < 0 || config.TabIndex>= MaxSidebarInterfaceAmount)
            {
                Logs.Warning(this, $"Attempted to show interface {config.InterfaceId} at out-of-range tab index {config.TabIndex}.");
                return null;
            }

            if (!await CanShow(forceShow, SideInterfaces[config.TabIndex]))
                return null;

            var retval = SideInterfaces[config.TabIndex] = new SidebarInterfaceControler(this, config);

            if(showImmediate)
                await retval.Show();
            SetupCanBeCloseable(retval, config);

            return retval;
        }

        private static async Task<bool> CanShow(bool forceState, InterfaceController controller)
        {
            if (forceState)
            {
                if (controller != null)
                    await controller.Close();
                return true;
            }
            return controller == null;
        }

        public PlayerInterfaces(Player player)
        {
            Player = player;
        }

        public async Task SetText(ITextConfig text)
        {
            foreach (var pair in text.ChildMessages)
            {
                await Player.Connection.Send(OutPacketType.SetInterfaceText, b =>
                {
                    b.WriteString(pair.Value)
                        .WriteWord(pair.Key);
                }, pair.Value.Length + 6);
            }
        }

        private readonly Queue<IQueueableDialogInterface> _dialogQueue 
            = new Queue<IQueueableDialogInterface>();

        public IQueueableDialogInterface CurrentDialogInterface { get; private set; }

        public async Task QueueDialog(IQueueableDialogInterface dialog, bool showImmediate = true)
        {
            _dialogQueue.Enqueue(dialog);

            if (showImmediate)
                await ShowDialogQueue();
        }

        public async Task ShowDialogQueue()
        {
            if (CurrentDialogInterface == null)
                await ForceShowNextDialogEntry();
        }

        private async Task ForceShowNextDialogEntry()
        {
            if (!_dialogQueue.Any())
            {
                CurrentDialogInterface = null;
                return;
            }

            var dialog = _dialogQueue.Dequeue();

            CurrentDialogInterface = dialog;

            var interf = await ShowMain(dialog, false);
            await SetText(dialog.TextConfig);

            interf.ButtonHandlersAsync[dialog.CloseButtonId] = async () =>
            {
                await GenericCloseableHandler(dialog, interf);
                await ForceShowNextDialogEntry();
            };
            

            await interf.Show();
        }

        public void ClearDialogQueue()
            => _dialogQueue.Clear();

        public async Task ForceCloseAllNonSideInterfaces()
        {
            ClearDialogQueue();
            CurrentDialogInterface = null;
            if (MainInterface != null)
                await MainInterface.Close();
            if (WalkableInterface != null)
                await WalkableInterface.Close();
        }

        // todo : handle packet 130
    }
}
