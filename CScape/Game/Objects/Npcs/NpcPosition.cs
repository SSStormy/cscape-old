﻿using System.Diagnostics;
using System.Linq;
using CScape.Server;

namespace CScape.Game.Objects.Npcs
{
    public class NpcPosition : PositionController
    {
        public Npc Npc { get; }

        public NpcPosition(Npc npc, ScapeServer server) : base(server, npc)
        {
            Npc = npc;
        }

        protected override void EnterMap(Region map)
        {
            map.Npcs.Add(Npc);
        }

        protected override void LeaveMap(Region map)
        {
            map.Npcs.Remove(Npc);
        }

        public override void ProcessMovement()
        {
            if (HasProcessedThisTick)
                return;

            HasProcessedThisTick = true;

            if (!QueuedMoveData.Any())
            {
                MoveType = MovementType.NoUpdate;
                return;
            }

            var data = QueuedMoveData.First();
            QueuedMoveData = QueuedMoveData.RemoveFirst();

            Debug.Assert(data.MoveType == MovementType.Walking);
            Dir1 = ApplyInterpolation(data);
            RecalcMaps();
        }
    }
}
