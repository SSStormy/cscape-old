﻿using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public interface ISynchronizationMessage
    {
        PacketBuilder GetPacket(ConnectionContext receiver);
    }
}
