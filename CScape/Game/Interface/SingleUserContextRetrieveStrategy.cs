using CScape.Game.Objects.Player;
using CScape.Network.Synchronization;

namespace CScape.Game.Interface
{
    public sealed class SingleUserContextRetrieveStrategy : IInterfaceSyncContextStrategy
    {
        private readonly InterfaceSyncContext _context;

        public SingleUserContextRetrieveStrategy(IInterface interf, IInterfaceCanOverrideStrategy overridestrat = null)
        {
            _context = new InterfaceSyncContext(interf, overridestrat);
        }

        public InterfaceSyncContext GetContext(Player player)
        {
            return _context;
        }
    }
}