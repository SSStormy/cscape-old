﻿using System;
using System.Diagnostics;
using CScapeCache.Caches;
using CScapeCache.Core;

namespace CScapeCache.Model
{
    public sealed class FloCache : GenericConfigCache<FloModel>
    {
        public FloCache(CacheReader cache, DataBuffer dataFile, ICacheFactory<FloModel, GenericConfigCache<FloModel>> factory) : base(cache, dataFile, null, factory)
        {
        }

        protected override void InternalLoad()
        {
            var cacheSize = DataFile.ReadShort();
            InternalCache = new FloModel[cacheSize];
            TotalObjects = cacheSize;

            for (int j = 0; j < cacheSize; j++)
            {
                if (InternalCache[j] == null)
                    InternalCache[j] = new FloModel(this, j);
                InternalCache[j].ReadItem(DataFile);
            }
        }
    }

    public sealed class FloModel
    {
        public GenericConfigCache<FloModel> Cache { get; }

        public int Id { get; }

        /*
         * Controls minimap tile color in certain areas.
         * 
         * Two states: any value, value of 0xff00ff.
         * 
         * If state is 0xff0ff, then
         * k23 = 0
         * j23 = -2
         * i23 = -1,
         * are set before rendering minimap.
         * 
         * If it's any value other then 0xff0ff,
         * j23 = method177(flo_2.anInt394, flo_2.anInt395, flo_2.anInt396);
	     * k23 = Texture.anIntArray1482[method185(flo_2.anInt399, 96)];
         * 
         * Images (anInt390 == 0xff0ff)
         * 
         * One of the certain areas the value is set to 0xff00ff is near the corner of the map in morytania
         * 
         * https://i.imgur.com/qc3Xn8d.png (k23 set to Texture.anIntArray1482[10])
         * https://i.imgur.com/zGQbcVM.png (k23 set to Texture.anIntArray1482[50])
         * 
         * Take note of the minimap tile colors.
         */
        public int anInt390 { get; private set; } 

        /*
         * 0 >= n > 50,
         * Passed to Texture.method369, which probably decodes the color values
         * 
         */
        public int anInt391 { get; private set; }

        /* 
         * something to do with tileOverlayClippingPaths?
         * sets anIntArrayArrayArray135[l][l6][k17] |= 0x924; (current tile in rendering)
         */
        public bool aBoolean393 { get; private set; }
        public int anInt394 { get; private set; } // possibly a color value
        public int anInt395 { get; private set; } // possibly a color value
        public int anInt396 { get; private set; } // possibly a color value
        public int anInt397 { get; private set; } // (int)(d5 * (double)anInt398);
        public int anInt398 { get; private set; } // derived from d7 and d6

        /*
         * Encoded color, decoded by method185(value, 96);
         * From testing, seems like this changes floor grays (floors, stone pathways etc)
         */
        public int anInt399 { get; private set; }

        private readonly Random _rng;

        internal FloModel(GenericConfigCache<FloModel> cache, int id)
        {
            Cache = cache;
            Id = id;
            anInt391 = -1;
            aBoolean393 = true;
            _rng = new Random();
        }

        internal void ReadItem(DataBuffer buffer)
        {
            do
            {
                int i = buffer.ReadByte();
                bool dummy;
                if (i == 0)
                    return;
                if (i == 1)
                {
                    anInt390 = buffer.Read3Bytes();
                    method262(anInt390);
                }
                else if (i == 2)
                    anInt391 = buffer.ReadByte();
                else if (i == 3)
                    dummy = true;
                else if (i == 5)
                    aBoolean393 = false;
                else if (i == 6)
                    buffer.ReadString();
                else if (i == 7)
                {
                    var j = anInt394;
                    var k = anInt395;
                    var l = anInt396;
                    var i1 = anInt397;
                    var j1 = buffer.Read3Bytes();
                    method262(j1);
                    anInt394 = j;
                    anInt395 = k;
                    anInt396 = l;
                    anInt397 = i1;
                    anInt398 = i1;
                }
                else
                    Debug.Fail("Error unrecognised config code: " + i);
            } while (true);
        }

        private void method262(int i)
        {
            var d = (i >> 16 & 0xff)/256D;
            var d1 = (i >> 8 & 0xff)/256D;
            var d2 = (i & 0xff)/256D;
            var d3 = d;
            if (d1 < d3)
                d3 = d1;
            if (d2 < d3)
                d3 = d2;
            var d4 = d;
            if (d1 > d4)
                d4 = d1;
            if (d2 > d4)
                d4 = d2;
            var d5 = 0.0D;
            var d6 = 0.0D;
            var d7 = (d3 + d4)/2D;
            if (d3 != d4)
            {
                if (d7 < 0.5D)
                    d6 = (d4 - d3)/(d4 + d3);
                if (d7 >= 0.5D)
                    d6 = (d4 - d3)/(2D - d4 - d3);
                if (d == d4)
                    d5 = (d1 - d2)/(d4 - d3);
                else if (d1 == d4)
                    d5 = 2D + (d2 - d)/(d4 - d3);
                else if (d2 == d4)
                    d5 = 4D + (d - d1)/(d4 - d3);
            }
            d5 /= 6D;
            anInt394 = (int) (d5*256D);
            anInt395 = (int) (d6*256D);
            anInt396 = (int) (d7*256D);
            if (anInt395 < 0)
                anInt395 = 0;
            else if (anInt395 > 255)
                anInt395 = 255;
            if (anInt396 < 0)
                anInt396 = 0;
            else if (anInt396 > 255)
                anInt396 = 255;
            if (d7 > 0.5D)
                anInt398 = (int) ((1.0D - d7)*d6*512D);
            else
                anInt398 = (int) (d7*d6*512D);
            if (anInt398 < 1)
                anInt398 = 1;
            anInt397 = (int) (d5*anInt398);
            var k = anInt394 + (int) (_rng.NextDouble()*16D) - 8;
            if (k < 0)
                k = 0;
            else if (k > 255)
                k = 255;
            var l = anInt395 + (int) (_rng.NextDouble()*48D) - 24;
            if (l < 0)
                l = 0;
            else if (l > 255)
                l = 255;
            var i1 = anInt396 + (int) (_rng.NextDouble()*48D) - 24;
            if (i1 < 0)
                i1 = 0;
            else if (i1 > 255)
                i1 = 255;
            anInt399 = method263(k, l, i1);
        }

        private int method263(int i, int j, int k)
        {
            if (k > 179)
                j /= 2;
            if (k > 192)
                j /= 2;
            if (k > 217)
                j /= 2;
            if (k > 243)
                j /= 2;
            return (i/4 << 10) + (j/32 << 7) + k/2;
        }
    }
}