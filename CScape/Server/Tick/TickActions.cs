﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Imms;
using MoonSharp.Interpreter;

namespace CScape.Server.Tick
{
    public sealed class TickActions
    {
        public ScapeServer Server { get; }

        private ImmList<ITickAction> _actions = ImmList<ITickAction>.Empty;
        private ImmMap<int, ITickAction> _uniqueActions = ImmMap<int, ITickAction>.Empty();

        private readonly object _actLock = new object();
        private readonly object _uniqLock = new object();

        public TickActions(ScapeServer server)
        {
            Server = server;
        }

        public void ScheduleNext(ITickAction action)
        {
            lock (_actions)
                _actions = _actions.AddLast(action);
        }

        public void ScheduleUniqueNext(ITickAction uniqueAction)
        {
            lock (_uniqLock)
                _uniqueActions = _uniqueActions.Set(uniqueAction.HashCode, uniqueAction);
        }

        public bool NeedsHandling()
        {
            return _actions.Any() || _uniqueActions.Any();
        }

        public async Task HandleAll()
        {
            var act = _actions;
            var uniqAct = _uniqueActions;

            _uniqueActions = ImmMap<int, ITickAction>.Empty();
            _actions = ImmList<ITickAction>.Empty;

            foreach (var action in act)
                await action.Handle();

            foreach (var uniq in uniqAct)
                await uniq.Value.Handle();
        }
    }

    public interface ITickAction
    {
        int HashCode { get; }
        Task Handle();
    }

    public static class TickActionFactory
    {
        public static ITickAction Create(Action action, int hashcode = 0)
            => new SynchronousTickAction(action, hashcode);

        public static ITickAction Create(Func<Task> task, int hashcode = 0)
            => new AsyncTickAction(task, hashcode);

        public static ITickAction Create(DynValue val, int hashcode = 0, object[] args = null)
            => new DynValueFuncCallTickAction(val, args, hashcode);
    }

    public class SynchronousTickAction : ITickAction
    {
        public Action Action { get; }

        public int HashCode { get; }

        public Task Handle()
        {
            Action();
            return Task.CompletedTask;
        }

        public SynchronousTickAction(Action action, int hashCode)
        {
            Action = action;
            HashCode = hashCode;
        }
    }

    public class AsyncTickAction : ITickAction
    {
        public Func<Task> FuncTask { get; }

        public int HashCode { get; }

        public async Task Handle()
            => await FuncTask();

        public AsyncTickAction(Func<Task> fTask, int hashCode)
        {
            FuncTask = fTask;
            HashCode = hashCode;
        }
    }

    public class DynValueFuncCallTickAction : ITickAction
    {
        public DynValue Dyn { get; }
        private readonly object[] _args;

        public int HashCode { get; }

        public async Task Handle()
        {
            if (_args == null)
                await Dyn.Function.CallAsync();
            else
                await Dyn.Function.CallAsync(_args);
        }

        public DynValueFuncCallTickAction(DynValue funcVal, object[] args, int hashCode)
        {
            if (funcVal.Type != DataType.Function)
                throw new ArgumentException($"Cannot call DynVal type {funcVal.Type} call.");

            Dyn = funcVal;
            _args = args;
            HashCode = hashCode;
        }
    }
}