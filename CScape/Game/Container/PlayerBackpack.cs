﻿using System;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Network.Packet;
using CScape.Server;

namespace CScape.Game.Container
{
    public class PlayerBackpack : GenericListContainer, IMoveItemHandler, IOptionItemHandler
    {
        public Player Player { get; set; }

        public const int BackpackSize = 28;

        public bool[] ShouldIgnoreOpAt { get; set; } = new bool[BackpackSize];

        public PlayerBackpack(ScapeServer server, ContainerSlot[] slots) : base(server, server.Interfaces.ContainerIds.PlayerBackpack, slots)
        {
        }

        public PlayerBackpack(Player player)
            : base(player.Server, BackpackSize, player.Server.Interfaces.ContainerIds.PlayerBackpack)
        {
            Player = player;
        }

        public Task Swap(int target, int slot)
        {
            var slotTemp = this[slot];

            this[slot] = this[target];
            this[target] = slotTemp;

            SyncSlot(target);
            SyncSlot(slot);

            return Task.CompletedTask;
        }

        private Task HandleEquipping(int slotId)
        {
            var item = Player.Backpack[slotId];

            if (!Player.CanEquip(item))
            {
                Player.SendMessage("You cannot equip this item.");
                Player.Backpack.SyncSlot(slotId);
                return Task.CompletedTask;
            }

            var info = Player.Equipment.CanAddItem(item);
            var repItem = Player.Equipment.Equip(item, info);

            if (repItem != null)
                Player.Backpack[slotId] = repItem;
            else if (info.ShouldChangeAmount)
            {
                Player.Backpack[slotId].Amount = info.AmountRemainder;
                Player.Backpack.SyncSlot(slotId);
            }

            return Task.CompletedTask;
        }

        public async Task HandleOption(ItemOptionType option, int slotId)
        {
            switch (option)
            {
                case ItemOptionType.Option1:
                    break;
                case ItemOptionType.Option2:
                    break;
                case ItemOptionType.Option3:
                    break;
                case ItemOptionType.Equip:
                    await HandleEquipping(slotId);
                    break;
                default:
                    ShouldIgnoreOpAt[slotId] = false;
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }
        }
    }
}
