﻿using System.Linq;

namespace CScapeCache.Caches.Collision
{
    public sealed class CollisionCache : AbstractLookupCache<PositionKey, CollisionData>
    {
        internal CollisionCache(CacheReader cache) : base(cache)
        {
          
        }

        protected override void InternalLoad()
        {
            foreach (var map in Reader.MapIndexCache.All.Select(index => Reader.MapCache.GetMap(index, 0, 0)))
            {
                foreach (var obj in map.Objects)
                {
                    var def = Reader.ObjectCache.GetId(obj.Id);

                    if (!def.Clips)
                        continue;

                    if (obj.Type == 22) // ground decoration
                    {
                        if (def.HasActions)
                        {
                            GetCollision(obj.GlobalX, obj.GlobalY, obj.Plane).Add(CollisionMasks.Blocked);
                        }
                    }
                    else if (obj.Type == 10 || obj.Type == 11)
                    {
                        MarkSolidOccupant(obj.GlobalX, obj.GlobalY, obj.Plane, def.TileSizeX, def.TileSizeY,
                            obj.Rotation /* face() */, def.ABoolean757);
                    }
                    else if (obj.Type >= 12)
                    {
                        MarkSolidOccupant(obj.GlobalX, obj.GlobalY, obj.Plane, def.TileSizeX, def.TileSizeY,
                            obj.Rotation, def.ABoolean757);
                    }
                    else if (obj.Type == 0)
                    {
                        MarkWall(obj.GlobalX, obj.GlobalY, obj.Plane, obj.Type, obj.Rotation, def.ABoolean757);
                    }
                    else if (obj.Type == 1)
                    {
                        MarkWall(obj.GlobalX, obj.GlobalY, obj.Plane, obj.Type, obj.Rotation, def.ABoolean757);
                    }
                    else if (obj.Type == 2)
                    {
                        MarkWall(obj.GlobalX, obj.GlobalY, obj.Plane, obj.Type, obj.Rotation, def.ABoolean757);
                    }
                    else if (obj.Type == 3)
                    {
                        MarkWall(obj.GlobalX, obj.GlobalY, obj.Plane, obj.Type, obj.Rotation, def.ABoolean757);
                    }
                    else if (obj.Type == 9)
                    {
                        MarkSolidOccupant(obj.GlobalX, obj.GlobalY, obj.Plane, def.TileSizeX, def.TileSizeY,
                            obj.Rotation, def.ABoolean757);
                    }
                }
            }
        }

        private void MarkSolidOccupant(int x, int y, int plane, int width, int height, int orientation, bool blocked)
        {
            if (orientation == 1 || orientation == 3)
            {
                var tmp = width;
                width = height;
                height = tmp;
            }

            for (var tX = x; tX < x + width; tX++)
            {
                for (var tY = y; tY < y + height; tY++)
                {
                    Set(tX, tY, plane)
                        .Add(CollisionMasks.Occupied)
                        .Add(CollisionMasks.Solid, blocked);
                }
            }
        }

        public CollisionData GetCollision(int x, int y, int plane)
        {
            PositionKey ignored;
            return GetCollision(x, y, plane, out ignored);
        }

        public CollisionData GetCollision(int x, int y, int plane, out PositionKey key)
        {
            ThrowIfNotLoaded();

            key = new PositionKey(x, y, plane);
            return !ValueLookup.ContainsKey(key) ? CollisionData.None : ValueLookup[key];
        }

        private CollisionData Set(int x, int y, int z)
        {
            var key = new PositionKey(x, y, z);
            if (!ValueLookup.ContainsKey(key))
                ValueLookup.Add(key, CollisionData.None);
            return ValueLookup[key];
        }

        private void MarkWall(int x, int y, int plane, int position, int orientation, bool blocked)
        {
            if (position == 0)
            {
                if (orientation == 0)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.West) /* wall west */
                        .Add(CollisionMasks.BlockedWest, blocked); /* blocked wall west */
                    Set(x - 1, y, plane)
                        .Add(CollisionMasks.East) /* wall east */
                        .Add(CollisionMasks.BlockedEast, blocked); /* blocked wall east */
                }
                else if (orientation == 1)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.North) /* wall north */
                        .Add(CollisionMasks.BlockedNorth, blocked); /* blocked wall north */
                    Set(x, y + 1, plane)
                        .Add(CollisionMasks.South) /* wall south */
                        .Add(CollisionMasks.BlockedSouth, blocked); /* blocked wall south */
                }
                else if (orientation == 2)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.East) /* wall east */
                        .Add(CollisionMasks.BlockedEast, blocked); /* blocked wall east */
                    Set(x + 1, y, plane)
                        .Add(CollisionMasks.West) /* wall west */
                        .Add(CollisionMasks.BlockedWest, blocked); /* blocked wall west */
                }
                else if (orientation == 3)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.South) /* wall south */
                        .Add(CollisionMasks.BlockedSouth, blocked); /* blocked wall south */
                    Set(x, y - 1, plane)
                        .Add(CollisionMasks.North) /* wall north */
                        .Add(CollisionMasks.BlockedNorth, blocked); /* blocked wall north */
                }
            }
            else if (position == 1 || position == 3)
            {
                if (orientation == 0)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.Northwest) /* wall northwest */
                        .Add(CollisionMasks.BlockedNorthwest, blocked); /* blocked wall northwest */
                    Set(x - 1, y + 1, plane)
                        .Add(CollisionMasks.Southeast) /* wall southeast */
                        .Add(CollisionMasks.BlockedSoutheast, blocked); /* blocked wall southeast */
                }
                else if (orientation == 1)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.Northeast) /* wall northeast */
                        .Add(CollisionMasks.BlockedNortheast, blocked); /* blocked wall northeast */
                    Set(x + 1, y + 1, plane)
                        .Add(CollisionMasks.Southwest) /* wall southwest */
                        .Add(CollisionMasks.BlockedSouthwest, blocked); /* blocked wall southwest */
                }
                else if (orientation == 2)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.Southeast) /* wall southeast */
                        .Add(CollisionMasks.BlockedSoutheast, blocked); /* blocked wall southeast */
                    Set(x + 1, y - 1, plane)
                        .Add(CollisionMasks.Northwest) /* wall northwest */
                        .Add(CollisionMasks.BlockedNorthwest, blocked); /* blocked wall northwest */
                }
                else if (orientation == 3)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.Southwest) /* wall southwest */
                        .Add(CollisionMasks.BlockedSouthwest, blocked); /* blocked wall southwest */
                    Set(x - 1, y - 1, plane)
                        .Add(CollisionMasks.Northeast) /* wall northeast */
                        .Add(CollisionMasks.BlockedNortheast, blocked); /* blocked wall northeast */
                }
            }
            else if (position == 2)
            {
                if (orientation == 0)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.WestNorth) /* wall west | north */
                        .Add(CollisionMasks.BlockedWestNorth, blocked); /* blocked wall west | north */
                    Set(x - 1, y, plane)
                        .Add(CollisionMasks.East) /* wall east */
                        .Add(CollisionMasks.BlockedEast, blocked); /* blocked wall east */
                    Set(x, y + 1, plane)
                        .Add(CollisionMasks.South) /* wall south */
                        .Add(CollisionMasks.BlockedSouth, blocked); /* blocked wall south */
                }
                else if (orientation == 1)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.EastNorth) /* wall east | north */
                        .Add(CollisionMasks.BlockedEastNorth, blocked); /* blocked wall east | north */
                    Set(x, y + 1, plane)
                        .Add(CollisionMasks.South) /* wall south */
                        .Add(CollisionMasks.BlockedSouth, blocked); /* blocked wall south */
                    Set(x + 1, y, plane)
                        .Add(CollisionMasks.West) /* wall west */
                        .Add(CollisionMasks.BlockedWest, blocked); /* blocked wall west */
                }
                else if (orientation == 2)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.EastSouth) /* wall east | south */
                        .Add(CollisionMasks.BlockedEastSouth, blocked); /* blocked wall east | south */
                    Set(x + 1, y, plane)
                        .Add(CollisionMasks.West) /* wall west */
                        .Add(CollisionMasks.BlockedWest, blocked); /* blocked wall west */
                    Set(x, y - 1, plane)
                        .Add(CollisionMasks.North) /* wall north */
                        .Add(CollisionMasks.BlockedNorth, blocked); /* blocked wall north */
                }
                else if (orientation == 3)
                {
                    Set(x, y, plane)
                        .Add(CollisionMasks.WestSouth) /* wall east | south */
                        .Add(CollisionMasks.BlockedWestSouth, blocked); /* blocked wall west | south */
                    Set(x, y - 1, plane)
                        .Add(CollisionMasks.North) /* wall north */
                        .Add(CollisionMasks.BlockedNorth, blocked); /* blocked wall north */
                    Set(x - 1, y, plane)
                        .Add(CollisionMasks.East) /* wall east */
                        .Add(CollisionMasks.BlockedEast, blocked); /* blocked wall east */
                }
            }
        }
    }
}