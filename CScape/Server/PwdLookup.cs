﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace CScape.Server
{
    public sealed class PwdLookup
    {
        public ScapeServer Server { get; }
        public LogManager Logs => Server.Logs;

        // username : password
        private Dictionary<string, string> _lookup = new Dictionary<string, string>();

        public PwdLookup(ScapeServer server)
        {
            Server = server;
        }

        public void RegisterNewPwd(string username, string password)
        {
            if (_lookup.ContainsKey(username))
            {
                Logs.Warning(this, $"Tried to register new psswd for {username} when it already has a password.");
                return;
            }

            _lookup[username] = PasswordStorage.CreateHash(password);

            Logs.Debug(this, $"Registered new passwd for plr: {username}");
        }

        public bool VerifyPwd(string username, string password)
        {
            if (!_lookup.ContainsKey(username))
            {
                Logs.Warning(this, $"Tried to verify passwd for unregistered user {username}");
                return false;
            }

            var cached = _lookup[username];
            return PasswordStorage.VerifyPassword(password, cached);
        }

        public void Load()
        {
            if (!File.Exists(Server.Config.PassLookupDir))
            {
                Logs.Warning(this, "Password lookup doesn't exist. Creating new one.");
                return;
            }

            _lookup = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(Server.Config.PassLookupDir));
            Logs.Debug(this, $"Loaded {_lookup.Count} pass entries.");
        }

        public void Save()
            => File.WriteAllText(Server.Config.PassLookupDir, JsonConvert.SerializeObject(_lookup));
    }
}
