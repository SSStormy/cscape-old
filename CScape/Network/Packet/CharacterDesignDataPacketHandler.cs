﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.IO;
using CScape.Network.Synchronization;

namespace CScape.Network.Packet
{
    public sealed class CharacterDesignDataPacketHandler : IPacketHandler
    {
        private static readonly Dictionary<int, Tuple<int, int>> _common = new Dictionary<int, Tuple<int, int>>
        {
            // gender
            {0, Tuple.Create(0, 1)},
            // colors
            {8, Tuple.Create(0, 11)},
            {9, Tuple.Create(0, 15)},
            {10, Tuple.Create(0, 15)},
            {11, Tuple.Create(0, 5)},
            {12, Tuple.Create(0, 7)},
        };

        private static Dictionary<int, Tuple<int, int>> _maleBody = new Dictionary<int, Tuple<int, int>>(_common)
        {
            {1, Tuple.Create(0, 8)},
            {2, Tuple.Create(10, 17)},
            {3, Tuple.Create(18, 25)},
            {4, Tuple.Create(26, 31)},
            {5, Tuple.Create(33, 34)},
            {6, Tuple.Create(36, 40)},
            {7, Tuple.Create(42, 43)},
        };

        private static Dictionary<int, Tuple<int, int>> _femaleBody = new Dictionary<int, Tuple<int, int>>(_common)
        {
            {1, Tuple.Create(45, 54)},
            {2, Tuple.Create(255, 255)},
            {3, Tuple.Create(56, 60)},
            {4, Tuple.Create(61, 65)},
            {5, Tuple.Create(67, 68)},
            {6, Tuple.Create(70, 77)},
            {7, Tuple.Create(79, 80)},
        };

        public Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            if (context.Player.Interface.MainInterface == null ||
                context.Player.Interface.MainInterface.Interface.InterfaceId !=
                context.Server.Interfaces.InterfaceIds.CharacterDesign)
            {
                context.Logs.Warning(this,
                    $"Player {context.Player.Username} sent us character design data when char design screen isn't open.");
                return Task.FromResult(PacketResult.AllOk);
            }

            var bytesRead = 0;
            var app = context.Player.Appearance;

            app.Gender = (PlayerAppearance.GenderType) ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Head = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Beard = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Chest = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Arms = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Hands = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Legs = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.Feet = ClampRead(packetStream, ref bytesRead, app.Gender);

            app.HairColor = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.TorsoColor = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.LegColor = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.FeetColor = ClampRead(packetStream, ref bytesRead, app.Gender);
            app.SkinColor = ClampRead(packetStream, ref bytesRead, app.Gender);

            context.Player.PlayerSync.SyncFlags |= PlayerSyncFlags.Appearance;

            return Task.FromResult(PacketResult.AllOk);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int ClampRead(RsPacketStream stream, ref int bytesRead, PlayerAppearance.GenderType gender)
        {
            var tuple = gender == PlayerAppearance.GenderType.Male 
                ? _maleBody[bytesRead++] 
                : _femaleBody[bytesRead++];
            return Utils.Clamp(stream.ReadUnsignedByte(), tuple.Item1, tuple.Item2);
        }
    }
}

    /*
Console.WriteLine($"--------------------");

var gender = packetStream.ReadUnsignedByte();
Console.WriteLine($"Gender: {gender}");
var bytes1 = new byte[7];
var bytes2 = new byte[5];

for (int i = 0; i < 7; i++)
{
    bytes1[i] = packetStream.ReadUnsignedByte();
    Console.WriteLine($"bytes1[{i}]: {bytes1[i]}");
}

for (int i = 0; i < 5; i++)
{
    bytes2[i] = packetStream.ReadUnsignedByte();
    Console.WriteLine($"bytes2[{i}]: {bytes2[i]}");
}

return Task.FromResult(PacketResult.AllOk);
*/

