using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.Network.Synchronization.Messages;

namespace CScape.Network.Synchronization.Machine
{
    public sealed class RegionSyncMachine : AbstractSynchronizationMachine
    {
        public RegionSyncMachine(Player player) : base(player)
        {
        }

        public override async Task Synchronize()
        {
            if (Player.Position.DidJustTeleport)
                Player.ViewingDistance = Globals.MaxViewingDistance;

            if (Player.Position.IsRegionDirty)
            {
                Player.Position.IsRegionDirty = false;
                await Player.Connection.SendMessage(new LoadRegionMessage(Player.Position.RegionX, Player.Position.RegionY));

                Player.Connection.Logs.Normal(this,
                    $"Synced region with {Player.Username}: Rx: {Player.Position.RegionX} + 6 Ry: {Player.Position.RegionY} + 6");
            }
        }
    }
}