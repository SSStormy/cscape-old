﻿using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.IO;
using CScape.Network.Synchronization;

namespace CScape.Network.Packet
{
    public sealed class ChatMessagePacketHandler : IPacketHandler
    {
        public Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var effects = packetStream.ReadUnsignedByte();
            var color = packetStream.ReadUnsignedByte();

            string msg;

            if (!packetStream.ReadRsString(Globals.MaxChatMsgLength, out msg))
            {
                context.Logs.Warning(this, "Failed parsing message.", conid: context.ConnectionId);
                return Task.FromResult(PacketResult.IncompleteRead);
            }

            context.Player.PlayerSync.SyncFlags |= PlayerSyncFlags.ChatText;
            context.Player.LastSentPublicChatMessage = new PublicChatMessage(
                context.Player, msg, color, effects);

            return Task.FromResult(PacketResult.AllOk);
        }
    }
}
