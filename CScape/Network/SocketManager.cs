﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using CScape.Server;

namespace CScape.Network
{
    /// <summary>
    /// Manages new player connections.
    /// </summary>
    public sealed class SocketManager
    {
        public int MaxConnections { get; }
        private const int ExtraContexts = 1024;

        public ScapeServer Parent { get; }
        public LogManager Logs => Parent.Logs;

        public ConnectionContext[] Contexts { get; }

        public bool IsListening { get; private set; }

        private Socket MainSocket => Parent.MainSocket;

        public SocketManager(ScapeServer parent)
        {
            Parent = parent;
            MaxConnections = Parent.PlayerProvider.MaxPlayers + ExtraContexts;

            Contexts = new ConnectionContext[MaxConnections];
        }

        public void Listen()
        {
            if (IsListening)
                throw new InvalidOperationException();

            IsListening = true;

            Task.Run(InternalListen);
        }

        private async Task InternalListen()
        {
            while (true)
            {
                try
                {
                    var connection = await Task.Factory.FromAsync(
                        MainSocket.BeginAccept(null, null),
                        MainSocket.EndAccept);

                    if (ShouldIgnore(connection.RemoteEndPoint))
                    {
                        connection.Disconnect(false);
                        Logs.Debug(this, $"Too many connect attempts for {connection.RemoteEndPoint}, ignoring.");
                        continue;
                    }

                    if (Parent.PlayerProvider.Players.Length >= MaxConnections)
                    {
                        connection.Disconnect(false);
                        continue;
                    }

                    var conid = GetConnectionId();

                    var context = new ConnectionContext(connection, conid, this);
                    Contexts[conid] = context;
                    context.StartReceive();
                }
                catch (Exception ex)
                {
                    Debug.Fail($"Accept exception:\r\n{ex}");
                }
            }
        }

        private int GetConnectionId()
        {
            for (int i = 0; i < Contexts.Length; i++)
            {
                if (Contexts[i] == null || Contexts[i].IsDead)
                    return i;

            }
            Debug.Fail("Could not find free conid.");
            return -1;
        }

        private bool ShouldIgnore(EndPoint end)
        {
            return false;
        }
    }
}
