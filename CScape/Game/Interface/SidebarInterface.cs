using CScape.Network.Synchronization;
using CScape.Network.Synchronization.Messages;

namespace CScape.Game.Interface
{
    public class SidebarInterface : IInterface
    {
        private sealed class SidebarOpenMessageBuilder : ISyncMessageBuilder
        {
            public SidebarInterface Sidebar { get; }

            public SidebarOpenMessageBuilder(SidebarInterface sidebar)
            {
                Sidebar = sidebar;
            }

            public ISynchronizationMessage BuildMessage()
            {
                return new ShowSidebarInterfaceMessage(Sidebar.Id, Sidebar.SidebarId);
            }
        }

        private sealed class SidebarCloseMessageBuilder : ISyncMessageBuilder
        {
            public SidebarInterface Sidebar { get; }

            public SidebarCloseMessageBuilder(SidebarInterface sidebar)
            {
                Sidebar = sidebar;
            }

            public ISynchronizationMessage BuildMessage()
            {
                return new ShowSidebarInterfaceMessage(-1, Sidebar.SidebarId);
            }
        }

        public int Id { get; }
        public int SidebarId { get; }
        public IInterfaceSyncContextStrategy ContextRetriever { get; }
        public ISyncMessageBuilder OpenMessageBuilder { get; }
        public ISyncMessageBuilder CloseMessageBuilder { get; }
        public IInterfaceCanOverrideStrategy BaseOverrideStrategy { get; } 
        public IInterfaceButtonHandler ButtonHandler { get; }

        public SidebarInterface(int id, int sidebarId, IInterfaceCanOverrideStrategy overrideStrategy, IInterfaceButtonHandler buttonHandler = null)
        {
            Id = id;
            SidebarId = sidebarId;
            OpenMessageBuilder = new SidebarOpenMessageBuilder(this);
            CloseMessageBuilder = new SidebarCloseMessageBuilder(this);
            ContextRetriever = new SingleUserContextRetrieveStrategy(this);
            BaseOverrideStrategy = overrideStrategy;
            ButtonHandler = buttonHandler;
        }
    }

    /*
         * 
        public void SendConfig()
            // 36

        public void Offset()
            // 70

        public void ClearInventory()
            // 72   

        public void SetScrollbarPosition()
            // 79

        public void SetButtonState()
            // 87

        public void SetColor()
            // 122

        public void PlayAnimation()
            // 200

        public void SetComponentZoomRotation()
            // 230

        public void DisplayItem()
            // 246

        public void ShowPlayerHead()
            // 185

        public void ShowNpcHead()
            // 75

        public void MoveComponent()
            // 70

        public void ShowModelOnComponent()
            // 8

        public void RecolorComponent()
            // 112
        */
}