﻿using System;
using CScape.Network.Synchronization;
using CScape.Server;
using CScapeCache.Caches.Collision;

namespace CScape.Game.Objects.Npcs
{
    public class Npc : IEntity, IEquatable<Npc>
    {
        int IEntity.Id => NpcId;
        ISyncFlagFacade IEntity.Sync => NpcSync;

        public NpcSyncFlagContainer NpcSync { get; }

        public int NpcId { get; }
        public int NpcDefinitionId { get; }

        public ScapeServer Server { get; }
        public NpcPosition Position { get; }

        PositionController IEntity.Position => Position;

        public Npc(ScapeServer server, int npcDefinitionId, int npcId)
        {
            NpcDefinitionId = npcDefinitionId;
            NpcId = npcId;
            Server = server;
            Position = new NpcPosition(this, server);
            NpcSync = new NpcSyncFlagContainer(this);
        }

        public bool Equals(IEntity other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            var npc = other as Npc;
            return npc != null && InternalEquals(npc);
        }

        public bool Equals(Npc other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return InternalEquals(other);
        }

        private bool InternalEquals(Npc other)
            => other.NpcId == NpcId;

        public override int GetHashCode()
            => unchecked(NpcId*293);

        public override string ToString()
        {
            return $"[NPC: (Id: {NpcId} DefId: {NpcDefinitionId})]";}
    }
}
