﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Objects.Npcs;
using CScape.Network;
using Imms;

namespace CScape.Game.Objects.Player
{
    public sealed class PlayerPosition : PositionController
    {
        public Player Player { get; }

        private ImmSet<Player> _cachedLocals;
        private ImmSet<Npc> _cachedNpcs;

        private readonly object _getLocalsLock = new object();
        private readonly object _getLocalNpcLock = new object();

        public ImmSet<Player> GetLocalClusterPlayers()
        {
            lock (_getLocalsLock)
            {
                if (_cachedLocals != null)
                    return _cachedLocals;

                var retval = ImmSet.Empty<Player>();

                foreach (var map in MapMatrix)
                {
                    if (map == null)
                        continue;

                    foreach (var local in map.Players.List)
                    {
                        if (Player.Equals(local))
                            continue;

                        retval = retval.Add(local);
                    }
                }

                _cachedLocals = retval;
                return retval;
            }
        }

        public ImmSet<Npc> GetLocalClusterNpcs()
        {
            lock (_getLocalNpcLock)
            {
                if (_cachedNpcs != null)
                    return _cachedNpcs;

                var retval = ImmSet.Empty<Npc>();

                foreach (var map in MapMatrix)
                {
                    if (map == null)
                        continue;

                    foreach (var local in map.Npcs.List)
                        retval = retval.Add(local);
                }

                _cachedNpcs = retval;
                return retval;
            }
        }

        public override void PostUpdate()
        {
            _cachedLocals = null;
            _cachedNpcs = null;
            base.PostUpdate();
        }

        public PlayerPosition(Player player) : base(player.Server, player)
        {
            Player = player;
        }

        protected override void EnterMap(Region map)
        {
            map.Players.Add(Player);
        }

        protected override void LeaveMap(Region map)
        {
            map.Players.Remove(Player);
        }

        public override void ProcessMovement()
        {
            if (HasProcessedThisTick)
                return;

            HasProcessedThisTick = true;

            if (!QueuedMoveData.Any())
            {
                MoveType = MovementType.NoUpdate;
                return;
            }

            var data = QueuedMoveData.First();
            QueuedMoveData = QueuedMoveData.RemoveFirst();
            if (data.MoveType == MovementType.PostTeleport)
            {
                MoveType = MovementType.PostTeleport;
                DidJustTeleport = true;
                return;
            }

            Dir1 = ApplyInterpolation(data);
            if (IsRunning && QueuedMoveData.Any())
            {
                MoveType = MovementType.Running;
                var runData = QueuedMoveData.First();
                QueuedMoveData = QueuedMoveData.RemoveFirst();

                Dir2 = ApplyInterpolation(runData);
            }
            else
                MoveType = MovementType.Walking;
        }

        public override async Task SetTeleportState()
        {
            await Player.Interface.ForceCloseAllNonSideInterfaces();
            await base.SetTeleportState();
        }

        public override async Task<bool> SubmitMovementRequest(List<int> wayX, List<int> wayY)
        {
            await Player.Interface.ForceCloseAllNonSideInterfaces();
            var result = await base.SubmitMovementRequest(wayX, wayY);

            if (!result)
                await Player.Connection.Send(OutPacketType.ResetFlag);

            return result;
        }
    }
}
