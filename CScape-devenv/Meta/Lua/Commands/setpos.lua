﻿REQUIRE = {"player", "args"}

PARAMS = 
{
	x = arg.new(valType.int, argType.required),
	y = arg.new(valType.int, argType.required),
    z = arg.new(valType.int, argType.optional)
}

function command (player, args)
	local z = player.position.globalZ;
	if args.z ~= nill then
		z = args.z;
	end

	player.position.teleport (args.x, args.y, args.z)
end

