﻿using System;
using System.IO;
using CScapeCache.BZip2;

namespace CScapeCache.Core
{
    //aka StreamLoader
    public class SubArchive
    {
        public byte[] MainBuffer { get; private set; }
        public byte[] CompressedBuffer { get; private set; }
        public int FileCount { get; private set; }
        public int[] FileNames { get; private set; }
        public int[] UncompressedFileSizes { get; private set; }
        public int[] CompressedFileSizes { get; private set; }
        public int[] FileLocations { get; private set; }
        public byte[][] FileBuffers { get; private set; }

        /*
		 * Unlike the client, we keep an array of file buffers here.
		 * When a file is extracted, it is written to the file buffer.
		 * Due to how the cache is unpacked, we must recompose the archive to maximize space efficiency.
		 * This requires extracting every file and replacing them, which is annoying and can be costly given the speed of BZip2.
		 * Unfortunately could have been avoided if this used the same format as the typical archive, which allowed data to be broken out in 512 byte blocks. Oh well.
		 */

        public bool ArchiveDecompressed { get; private set; }

        public SubArchive(byte[] buffer)
        {
            LoadBuffer(buffer);
        }

        private void LoadBuffer(byte[] buffer)
        {
            CompressedBuffer = buffer;
            if (buffer == null || buffer.Length == 0)
            {
                MainBuffer = buffer;
                FileCount = 0;
                FileNames = new int[FileCount];
                UncompressedFileSizes = new int[FileCount];
                CompressedFileSizes = new int[FileCount];
                FileLocations = new int[FileCount];
                FileBuffers = new byte[0][];
                return;
            }
            var readBuffer = new DataBuffer(buffer);
            var uncompressedSize = readBuffer.Read3Bytes();
            var compressedSize = readBuffer.Read3Bytes();

            if (uncompressedSize != compressedSize) //no rhyme or reason as to when this is used
            {
                MainBuffer = new byte[uncompressedSize];
                var temp = new byte[buffer.Length - 6];
                Array.Copy(buffer, 6, temp, 0, temp.Length);
                var ms = new MemoryStream(temp);
                var bz = new BZip2InputStream(ms);
                bz.Read(MainBuffer, 0, uncompressedSize);
                //BZ2Decompressor.decompress(Buffer, uncompressed_size, buffer, compressed_size, 6);
                ArchiveDecompressed = true;
                readBuffer = new DataBuffer(MainBuffer);
            }
            else
            {
                MainBuffer = buffer;
                ArchiveDecompressed = false;
            }

            FileCount = readBuffer.ReadShort();
            FileNames = new int[FileCount];
            UncompressedFileSizes = new int[FileCount];
            CompressedFileSizes = new int[FileCount];
            FileLocations = new int[FileCount];
            FileBuffers = new byte[FileCount][];

            var pos = readBuffer.Location + FileCount*10;
            for (var i = 0; i < FileCount; i++)
            {
                FileNames[i] = readBuffer.ReadInteger();
                UncompressedFileSizes[i] = readBuffer.Read3Bytes();
                CompressedFileSizes[i] = readBuffer.Read3Bytes();
                FileLocations[i] = pos;
                pos += CompressedFileSizes[i];
            }
        }

        public int GetFileIndex(int name)
        {
            for (var i = 0; i < FileNames.Length; i++)
            {
                if (FileNames[i] == name)
                    return i;
            }

            return -1;
        }

        public int GetFileIndex(string name)
        {
            name = name.ToUpper();
            var i = 0;
            for (var j = 0; j < name.Length; j++)
                i = i*61 + name[j] - 32;

            return GetFileIndex(i);
        }

        public static int GetHashCode(string name)
        {
            name = name.ToUpper();
            var i = 0;
            for (var j = 0; j < name.Length; j++)
                i = i*61 + name[j] - 32;
            return i;
        }

        public bool FileExists(string name)
        {
            var i = 0;
            name = name.ToUpper();
            for (var j = 0; j < name.Length; j++)
                i = i*61 + name[j] - 32;

            for (var k = 0; k < FileCount; k++)
            {
                if (FileNames[k] == i)
                {
                    return true;
                }
            }

            return false;
        }

        public DataBuffer ExtractFile(string name)
        {
            var i = 0;
            name = name.ToUpper();
            for (var j = 0; j < name.Length; j++)
                i = i*61 + name[j] - 32;

            uint meme = (uint) i;

            for (var k = 0; k < FileCount; k++)
            {
                if (FileNames[k] == i)
                {
                    return ExtractFile(k);
                }
            }

            return null;
        }

        public DataBuffer ExtractFile(int k)
        {
            if (k >= FileCount || k < 0)
                return null;

            if (FileBuffers[k] != null)
                return new DataBuffer(FileBuffers[k]);

            FileBuffers[k] = new byte[UncompressedFileSizes[k]];
            if (!ArchiveDecompressed)
            {
                var temp = new byte[CompressedFileSizes[k]];
                FileBuffers[k] = new byte[UncompressedFileSizes[k]];
                Array.Copy(MainBuffer, FileLocations[k], temp, 0, temp.Length);
                var ms = new MemoryStream(temp);
                var bz = new BZip2InputStream(ms);
                bz.Read(FileBuffers[k], 0, UncompressedFileSizes[k]);
            }
            else
            {
                Array.Copy(MainBuffer, FileLocations[k], FileBuffers[k], 0, UncompressedFileSizes[k]);
            }
            return new DataBuffer(FileBuffers[k]);
        }

        public bool RenameFile(int index, int newName)
        {
            if (index < 0 || index >= FileNames.Length)
                return false;
            for (var i = 0; i < FileNames.Length; i++)
            {
                if (FileNames[i] == newName)
                    return false;
            }

            FileNames[index] = newName;
            return true;
        }

        public bool RenameFile(int index, string newName)
        {
            return RenameFile(index, GetHashCode(newName));
        }

        public bool RenameFile(string oldName, string newName)
        {
            return RenameFile(GetFileIndex(oldName), GetHashCode(newName));
        }

        public void WriteFile(int index, byte[] data)
        {
            if (index < 0 || index >= FileCount)
                return;
            UncompressedFileSizes[index] = data.Length;
            FileBuffers[index] = data;
        }

        public void WriteFile(string name, byte[] data)
        {
            WriteFile(GetFileIndex(name), data);
        }

        public bool RewriteArchive()
        {
            var dataBuffer = new DataBuffer(new byte[8192]);
            for (var i = 0; i < FileCount; i++)
            {
                if (FileBuffers[i] == null)
                    ExtractFile(i);
                PackFile(ref dataBuffer, i, FileBuffers[i], !ArchiveDecompressed);
            }

            var header = new DataBuffer(new byte[2 + FileCount*10]);

            header.WriteShort(FileCount);
            for (var i = 0; i < FileCount; i++)
            {
                header.WriteInteger(FileNames[i]);
                header.Write3Bytes(UncompressedFileSizes[i]);
                header.Write3Bytes(CompressedFileSizes[i]);
                //pos += CompressedFileSizes[i];
            }


            if (ArchiveDecompressed)
            {
                var d = new DataBuffer(new byte[dataBuffer.Buffer.Length + header.Buffer.Length]);
                //d.Write3Bytes(data_buffer.Buffer.Length);
                //d.Write3Bytes(0);
                d.Write(header.Buffer, 0, header.Location);
                d.Write(dataBuffer.Buffer, 0, dataBuffer.Location);
                var ms = new MemoryStream();
                var os = new BZip2OutputStream(ms, 1);
                os.Write(d.Buffer, 0, d.Location);
                os.Close();

                var c = ms.GetBuffer();
                var final = new DataBuffer(new byte[os.BytesWritten + 6]);
                final.Write3Bytes(d.Buffer.Length);
                final.Write3Bytes(os.BytesWritten);
                final.Write(c, 0, os.BytesWritten);
                MainBuffer = final.Buffer;
            }
            else
            {
                var final = new DataBuffer(new byte[dataBuffer.Buffer.Length + header.Buffer.Length + 100000]);
                final.Write3Bytes(dataBuffer.Buffer.Length);
                if (ArchiveDecompressed)
                    final.Write3Bytes(0);
                else
                    final.Write3Bytes(dataBuffer.Buffer.Length);
                final.Write(header.Buffer, 0, header.Location);
                final.Write(dataBuffer.Buffer, 0, dataBuffer.Location);
                MainBuffer = final.Buffer;
            }
            LoadBuffer(MainBuffer);

            return true;
        }

        private bool PackFile(ref DataBuffer dest, int index, byte[] data, bool compress)
        {
            if (!compress)
            {
                if (dest.Location + data.Length >= dest.Buffer.Length)
                {
                    var nbuffer = new byte[(dest.Buffer.Length + data.Length)/1024*1024 + 4096];
                    Array.Copy(dest.Buffer, nbuffer, dest.Buffer.Length);
                    dest.Buffer = nbuffer;
                }

                FileLocations[index] = dest.Location;
                CompressedFileSizes[index] = UncompressedFileSizes[index];
                dest.Write(data);
                return true;
            }
            //using streams is really stupid
            var ms = new MemoryStream();
            var output = new BZip2OutputStream(ms, 1);
            output.Write(data, 0, data.Length);
            output.Close();
            var compressed = ms.ToArray();
            CompressedFileSizes[index] = output.BytesWritten;

            if (dest.Location + CompressedFileSizes[index] >= dest.Buffer.Length)
            {
                var nbuffer = new byte[dest.Buffer.Length + CompressedFileSizes[index] + 4096];
                Array.Copy(dest.Buffer, nbuffer, dest.Buffer.Length);
                dest.Buffer = nbuffer;
            }

            dest.Write(compressed, 0, compressed.Length);

            return true;
        }

        public void CreateFile(string name)
        {
            FileNames = ExpandArray(FileNames);
            UncompressedFileSizes = ExpandArray(UncompressedFileSizes);
            CompressedFileSizes = ExpandArray(CompressedFileSizes);
            FileLocations = ExpandArray(FileLocations);
            FileBuffers = ExpandArray(FileBuffers);
            //FileCount;

            FileNames[FileCount] = GetHashCode(name);
            FileBuffers[FileCount] = new byte[512];
            UncompressedFileSizes[FileCount] = 512;
            FileCount++;
            RewriteArchive();
        }

        private static T[] ExpandArray<T>(T[] src)
        {
            var n = new T[src.Length + 1];
            Array.Copy(src, n, src.Length);
            return n;
        }
    }
}