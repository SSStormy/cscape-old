﻿using System;
using System.Diagnostics;
using System.Text;
using CScape.Helper;

namespace CScape.IO
{
    public class RsByteWriter
    {
        protected byte[] _buffer;
        public byte[] RawBuffer => _buffer;

        public int Size { get; protected set; }
        public int Position { get; set; }

        private int _bitPosition;

        public RsByteWriter(int size)
        {
            _buffer = new byte[size];
            Size = size;
        }

        public RsByteWriter Fill(byte value, int amount)
        {
            var vbuf = new byte[amount];

            for (var i = 0; i < amount; i++)
                vbuf[i] = value;

            Buffer.BlockCopy(vbuf, 0, _buffer, Position, amount);
            Position += amount;
            return this;
        }

        public RsByteWriter WriteBytes(ByteBuffer buffer)
            => WriteBytesAt(buffer.Buffer, buffer.Start, buffer.Size, Position);

        public RsByteWriter WriteBytes(byte[] buffer, int start, int size)
            => WriteBytesAt(buffer, start, size, Position);

        public RsByteWriter WriteBytesAt(byte[] buffer, int bufferStart, int copyByteAmount, int writePosition)
        {
            if (buffer == null || copyByteAmount <= 0 || bufferStart < 0)
                return this;

            if (_buffer.Length < Position + copyByteAmount)
            {
                var old = _buffer;
                _buffer = new byte[_buffer.Length + copyByteAmount];
                Buffer.BlockCopy(old, 0, _buffer, 0, old.Length);
            }

            Buffer.BlockCopy(buffer, bufferStart, _buffer, writePosition, copyByteAmount);
            Position += copyByteAmount;
            return this;
        }

        public ByteBuffer ToByteArray(bool reuseForBitwise = false, bool reset = true)
        {
            if (Position <= 0)
                return ByteBuffer.Empty;

            var size = Position;
            var packet = new byte[size];

            Buffer.BlockCopy(_buffer, 0, packet, 0, size);

            if (reset)
                Reset(reuseForBitwise);
            return new ByteBuffer(packet, 0, size);
        }

        public virtual RsByteWriter Reset(bool reuseForBitwise = false)
        {
            _bitPosition = 0;
            Position = 0;
            if (reuseForBitwise)
                _buffer = new byte[Size];
            return this;
        }

        public RsByteWriter WriteSmart(int val)
        {
            val = val & 0xff;
            if (val < 128)
                _buffer[Position++] = (byte) val;
            else
                WriteWord(val);
            return this;
        }

        public RsByteWriter WriteByte(int i)
        {
            _buffer[Position++] = (byte) i;
            return this;
        }
        public RsByteWriter WriteWord(int i)
        {
            _buffer[Position++] = (byte) (i >> 8);
            _buffer[Position++] = (byte) i;
            return this;
        }

        public RsByteWriter WriteDWord(int i)
        {
            _buffer[Position++] = (byte) (i >> 24);
            _buffer[Position++] = (byte) (i >> 16);
            _buffer[Position++] = (byte) (i >> 8);
            _buffer[Position++] = (byte) i;
            return this;
        }

        public RsByteWriter WriteQWord(long l)
        {
            _buffer[Position++] = (byte) (int) (l >> 56);
            _buffer[Position++] = (byte) (int) (l >> 48);
            _buffer[Position++] = (byte) (int) (l >> 40);
            _buffer[Position++] = (byte) (int) (l >> 32);
            _buffer[Position++] = (byte) (int) (l >> 24);
            _buffer[Position++] = (byte) (int) (l >> 16);
            _buffer[Position++] = (byte) (int) (l >> 8);
            _buffer[Position++] = (byte) (int) l;
            return this;
        }

        public RsByteWriter WriteString(string s)
        {
            var sbuf = Encoding.ASCII.GetBytes(s);
            Buffer.BlockCopy(sbuf, 0, _buffer, Position, sbuf.Length);
            Position += sbuf.Length;
            _buffer[Position++] = Globals.NullTerminator;
            return this;
        }

        public RsByteWriter FinishBitAccess()
        {
            Position = (_bitPosition + 7)/8;
            return this;
        }

        public RsByteWriter InitBitAccess()
        {
            _bitPosition = Position*8;
            return this;
        }

        public RsByteWriter WriteBits(int numBits, int value)
        {
            int bytePos = _bitPosition >> 3;
            int bitOffset = 8 - (_bitPosition & 7);
            _bitPosition += numBits;

            for (; numBits > bitOffset; bitOffset = 8)
            {
                _buffer[bytePos] &= (byte) ~Globals.MaskForBit[bitOffset];
                _buffer[bytePos++] |= (byte) ((value >> (numBits - bitOffset)) & Globals.MaskForBit[bitOffset]);

                numBits -= bitOffset;
            }
            if (numBits == bitOffset)
            {
                _buffer[bytePos] &= (byte) ~Globals.MaskForBit[bitOffset];
                _buffer[bytePos] |= (byte) (value & Globals.MaskForBit[bitOffset]);
            }
            else
            {
                _buffer[bytePos] &= (byte) ~(Globals.MaskForBit[numBits] << (bitOffset - numBits));
                _buffer[bytePos] |= (byte) ((value & Globals.MaskForBit[numBits]) << (bitOffset - numBits));
            }
            return this;
        }
    }
}
