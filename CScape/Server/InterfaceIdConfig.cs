﻿using System.IO;
using CScape.Game.Container;
using CScape.Game.Interface;
using Newtonsoft.Json;

namespace CScape.Server
{
    public sealed class InterfaceIdConfig
    {
        public ScapeServer Server { get; }

        public InterfaceId InterfaceIds { get; private set; }
        public TextId TextIds { get; private set; }
        public ButtonId ButtonIds { get; private set; }
        public ContainerId ContainerIds { get; private set; }

        public InterfaceIdConfig(ScapeServer server)
        {
            Server = server;
        }

        public void Load()
        {
            InterfaceIds = JsonConvert.DeserializeObject<InterfaceId>(File.ReadAllText(Server.Config.InterfaceIdDir));
            ButtonIds= JsonConvert.DeserializeObject<ButtonId>(File.ReadAllText(Server.Config.ButtonIdDir));
            TextIds = JsonConvert.DeserializeObject<TextId>(File.ReadAllText(Server.Config.TextIdDir));
            ContainerIds = JsonConvert.DeserializeObject<ContainerId>(File.ReadAllText(Server.Config.ContainerId));
        }
    }
}
