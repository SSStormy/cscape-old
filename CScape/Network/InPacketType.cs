﻿namespace CScape.Network
{
    // Client -> Server
    public enum InPacketType : byte
    {
        ChatMessage = 4,
        WearItem = 41,
        WalkingRequest1 = 98,
        ChatCommand = 103,
        UnequipItem = 145,
        WalkingRequest2 = 164,
        ButtonPressed = 185,
        MoveItem = 214,
        MapWalk = 248,
        ObjectAction1 = 132,
        ObjectAction2 = 234,
        ObjectAction3 = 70,
        MouseClick = 241,
        DialogContinue = 40,
        ItemOption1 = 122,
        ItemOption2 = 16,
        ItemOption3 = 75,
        NpcOption2= 17,
        NpcOption4 = 18,
        NpcOption3 = 21,
        NpcAttack = 72,
        CloseInterface = 130,
        CharacterDesignData = 101,
        TradeRequest = 39, // todo : trading
        FollowRequest = 139,
    }
}