﻿REQUIRE = { "player", "args" }

PARAMS =
{
	itemId = arg.new (valType.int, argType.required),
	amount = arg.new (valType.int, argType.optional, 1),
}

function command (player, args)
	player.backpack.addItem (itemStack.__new (args.itemId, args.amount))
	player.sendMessage ("Give item id: " .. args.itemId .. " amnt: " .. args.amount)
end
