using System.Diagnostics;
using CScape.Game.Container;
using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class UpdateContainerItemsMessage : ISynchronizationMessage
    {
        public IContainer Container { get; }
        public int Index { get; }

        public UpdateContainerItemsMessage(IContainer container, int index)
        {
            Container = container;
            Index = index;
            Debug.Assert(Container.Capacity > index && index >= 0);
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            // todo : aggregate these somehow?
            var packet = new PacketBuilder(receiver.Player, 16);
            packet.OpcodeAndSizePlaceholder(OutPacketType.UpdateItems);
            packet.WriteWord(Container.ContainerId);
            packet.WriteSmart(Index);
            var item = Container[Index];
            packet.WriteWord(item.ItemId);
            if (item.Amount > byte.MaxValue)
                packet.WriteDWord((int)item.Amount);
            else
                packet.WriteByte((byte)item.Amount);
            packet.WriteSize();
            return packet;
        }
    }
}