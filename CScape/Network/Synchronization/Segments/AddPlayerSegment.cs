using CScape.Game.Objects;
using CScape.Game.Objects.Player;
using CScape.IO;

namespace CScape.Network.Synchronization.Segments
{
    public sealed class AddPlayerSegment : IEntityUpdateSegment
    {
        private readonly Player _us;

        public AddPlayerSegment(Player newPlayer, Player us)
        {
            _us = us;
            FlagWriter = new PlayerSyncFlagWriter(newPlayer);
            FlagWriter.SetFirstContactValues();
            Entity = newPlayer;
        }

        public void WriteInto(PacketBuilder packet)
        {
            packet.WriteBits(11, Entity.Id);
            packet.WriteBits(1, FlagWriter.NeedsWriting ? 1 : 0);
            packet.WriteBits(1, 1); // clear walk queue
            packet.WriteBits(5, Entity.Position.GlobalY - _us.Position.GlobalY);
            packet.WriteBits(5, Entity.Position.GlobalX - _us.Position.GlobalX);
        }

        public ISyncFlagWriter FlagWriter { get; }
        public IEntity Entity { get; }
    }
}