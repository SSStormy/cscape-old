﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CScapeCache;
using CScapeCache.Caches;
using CScapeCache.Caches.Collision;

namespace CollisionCalculator
{
    public static class CollisionProvider
    {
        public static Dictionary<PositionKey, CollisionData> CalculateCollision(CacheReader reader)
        {
            if (reader == null) throw new ArgumentNullException(nameof(reader));

            LoadIfNotLoaded(reader.MapIndexCache);
            LoadIfNotLoaded(reader.MapCache);
            LoadIfNotLoaded(reader.ObjectCache);
            LoadIfNotLoaded(reader.CollisionCache);

            var prop =
                reader.CollisionCache.GetType()
                    .GetRuntimeProperties()
                    .FirstOrDefault(p => p.Name.Equals("ValueLookup", StringComparison.InvariantCultureIgnoreCase));

            if (prop == null)
                throw new InvalidOperationException("Could not reflect CollisionCache's ValueLookup method info.");

            return (Dictionary<PositionKey, CollisionData>) prop.GetValue(reader.CollisionCache);
        }

        private static void LoadIfNotLoaded(AbstractCache cache)
        {
            if(!cache.IsLoaded)
                cache.Load();
        }
    }
}
