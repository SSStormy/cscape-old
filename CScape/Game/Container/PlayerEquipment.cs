﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Items;
using CScape.Game.Objects.Player;
using CScape.Network.Packet;
using CScape.Network.Synchronization;
using CScape.Server;
using CScape.Server.Repository;

namespace CScape.Game.Container
{
    public sealed class PlayerEquipment : GenericContainer, IOptionItemHandler
    {
        public const int EquipmentSize = 14;

        public Player Player { get; set; }
        public IRepository<ItemDef, int> Repo => Server.ItemRepo;

        public bool[] ShouldIgnoreOpAt { get; set; } = new bool[EquipmentSize];

        public PlayerEquipment(ScapeServer server, ContainerSlot[] slots) : base(server, server.Interfaces.ContainerIds.PlayerEquipment, slots)
        {
            Debug.Assert(slots.Length == EquipmentSize);
        }

        public PlayerEquipment(Player player) : base(EquipmentSize, player.Server.Interfaces.ContainerIds.PlayerEquipment, player.Server)
        {
            Player = player;
        }

        public override IEnumerator<ItemStack> GetEnumerator()
            => Slots.Select(s => s.Item).GetEnumerator();

        public override ItemStack this[int i]
        {
            get { return InternalSlots[i].Item; }
            set
            {
                Debug.Assert(value != null);
                Debug.Assert(i >= 0 && i < Capacity);
                if (CanAddItem(value).IsValidOperation)
                    SetItem(value, i);
            }
        }

        public override ContainerChangeInfo CanAddItem(ItemStack item)
        {
            var def = Repo.Get(item.ItemId);
            if (def == null)
                return ContainerChangeInfo.Invalid;

            var index = (int)def.EquipmentType;
            if (item.ItemId == this[index].ItemId && def.EquipmentType == EquipType.Arrows)
            {
                var info = GetAmountStackInfo(def, this[index], item, index);
                if (info.IsValidOperation)
                    return info;
            }

            return new ContainerChangeInfo(def.EquipmentType != EquipType.None, index);
        }

        public override ContainerChangeInfo CanRemoveItem(ItemStack item)
            => ContainerChangeInfo.Valid;

        /// <summary>
        /// Tries to equip the given item.
        /// </summary>
        /// <returns>If there was an item in the slot that we tried to equip the given item, we will return the one that the given item replaced, even it's its empty.</returns>
        public ItemStack Equip(ItemStack item, ContainerChangeInfo info)
        {
            var def = Repo.Get(item.ItemId);
            var index = (int)def.EquipmentType;
            ItemStack retval = null;

            if (info.ShouldChangeAmount)
            {
                this[index].Amount += item.Amount;
                SyncSlot(index);
            }
            else
            {
                retval = InternalSlots[index].Item;
                SetItem(item, index);
            }

            StateChanged();

            return retval;
        }

        /// <summary>
        /// Unequips the item at the given index, recalculates bonuses and returns it.
        /// </summary>
        /// <returns>The unequipped item, if it was even equipped.</returns>
        public ItemStack Unequip(int index)
        {
            var retval = (ItemStack)InternalSlots[index].Item.Clone();
            InternalSlots[index].Item.SetEmpty();
            Items--;

            SyncSlot(index);
            StateChanged();

            return retval;
        }

        private void StateChanged()
        {
            CalculateBonuses();
            Player.PlayerSync.SyncFlags |= PlayerSyncFlags.Appearance;
        }

        public void CalculateBonuses()
        {
            // todo : recalc bonuses
        }

        public Task HandleOption(ItemOptionType option, int slotId)
        {
            if (option != ItemOptionType.Unequip)
                return Task.CompletedTask;

            var equipSlot = this[slotId];

            if (!Player.Backpack.CanAddItem(equipSlot.ItemId).IsValidOperation)
            {
                Player.SendMessage("You do not have enough space in your inventory.");
                return Task.CompletedTask;
            }

            var addItem = Unequip(slotId);
            Player.Backpack.AddItem(addItem);

            return Task.CompletedTask;
        }
    }
}
