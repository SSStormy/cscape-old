﻿using System;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;

namespace CScape.Server
{
    public sealed class LoginCryptoManager
    {
        public ScapeServer Server { get; }

        private AsymmetricCipherKeyPair _keys;
        private IAsymmetricBlockCipher _crypto;

        public LoginCryptoManager(ScapeServer server)
        {
            Server = server;
        }

        public void ReadPrivateKeyFromConfig()
        {
            using (var file = File.Open(Server.Config.PrivateKeyDir, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var stream = new StreamReader(file))
                _keys = (AsymmetricCipherKeyPair) new PemReader(stream).ReadObject();

            _crypto = new OaepEncoding(new RsaEngine(), new Sha1Digest());
            _crypto.Init(false, _keys.Private);
        }

        public byte[] DecryptLogin(byte[] encryptedPayload, int start, int length)
        {
            if (encryptedPayload == null) throw new ArgumentNullException(nameof(encryptedPayload));
            if (_crypto == null) throw new InvalidOperationException();
            try
            {
                return _crypto.ProcessBlock(encryptedPayload, start, length);
            }
            catch (Exception ex)
            {
                Server.Logs.Exception(this, "Failed decrypting login packets.", ex);
                return null;
            }   
        }
    }
}
