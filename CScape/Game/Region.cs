﻿using System;
using CScape.Game.Objects.Npcs;
using CScape.Game.Objects.Player;
using Imms;

namespace CScape.Game
{
    public class Region : IEquatable<Region>
    {
        public class ImmObjectRepo<T>
        {
            private readonly object _modifyLock = new object();

            public ImmSet<T> List { get; private set; } = ImmSet<T>.Empty();

            public void Add(T obj)
            {
                lock (_modifyLock)
                    List = List.Add(obj);
            }

            public void Remove(T obj)
            {
                lock (_modifyLock)
                    List = List.Remove(obj);
            }
        }

        public const int Size = 8;

        public ImmObjectRepo<Player> Players { get;  } = new ImmObjectRepo<Player>();
        public ImmObjectRepo<Npc> Npcs { get; } = new ImmObjectRepo<Npc>();

        public int X { get; }
        public int Y { get; }

        protected Region(int regionX, int regionY)
        {
            X = regionX;
            Y = regionY;
        }

        public static void Allocate(int regionX, int regionY, World parent)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            var region = new Region(regionX, regionY);
            parent.Logs.Debug(region, $"allocmap mapX: {regionX} mapY: {regionY}");
            parent.SetRegion(region);
        }

        public bool Equals(Region other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return X == other.X &&
                   Y == other.Y;
        }
    }
}
