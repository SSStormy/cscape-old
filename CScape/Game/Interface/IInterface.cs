using CScape.Network.Synchronization;

namespace CScape.Game.Interface
{
    public interface IInterface
    {
        int Id { get; }
        IInterfaceSyncContextStrategy ContextRetriever { get; }
        ISyncMessageBuilder OpenMessageBuilder { get; }
        ISyncMessageBuilder CloseMessageBuilder { get; }
        IInterfaceCanOverrideStrategy BaseOverrideStrategy { get; }
        IInterfaceButtonHandler ButtonHandler { get; }
    }
}