using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class SyncChatMessage : ISynchronizationMessage
    {
        public string Message { get; }

        public SyncChatMessage(string message)
        {
            Message = message;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var packet = new PacketBuilder(receiver.Player, byte.MaxValue);
            packet.OpcodeAndSizePlaceholder(OutPacketType.SendServerMessage);
            packet.WriteString(Message);
            packet.WriteSize();
            return packet;
        }
    }
}