using System;
using CScape.Game.Objects.Npcs;

namespace CScape.Network.Synchronization
{
    public sealed class NpcSyncFlagContainer : ISyncFlagFacade
    {
        public Npc Npc { get; }

        public NpcSyncFlagContainer(Npc npc)
        {
            Npc = npc;
        }

        // todo : implement NpcSyncFlagContainer 

        public void FacingTile()
        {
            throw new NotImplementedException();
        }

        public void ResetAllFlags()
        {
            throw new NotImplementedException();
        }
    }
}