﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CScape.Server.Repository
{
    public interface IRepository<out TVal, in TKey>
    {
        ScapeServer Server { get; }

        IEnumerable<TVal> SearchFor(Func<TVal, bool> predicate);
        IEnumerable<TVal> GetAll();

        TVal Get(TKey key);
        Task Load();
    }
}
