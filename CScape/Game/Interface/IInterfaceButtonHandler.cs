using CScape.Game.Objects.Player;

namespace CScape.Game.Interface
{
    public interface IInterfaceButtonHandler
    {
        void OnButtonPressed(Player presser, int buttonId);
    }
}