﻿using System.Threading.Tasks;
using CScape.Game.Objects.Npcs;
using Imms;

namespace CScape.Server
{
    public sealed class NpcProvider
    {
        public int MaxNpcs => Server.Config.MaxNpcs;

        public ScapeServer Server { get; }
        public LogManager Logs => Server.Logs;

        private readonly object _modifyLock = new object();

        private readonly Npc[] _npcs; // o(1) by-id lookup
        public ImmList<Npc> Npcs { get; private set; } = ImmList<Npc>.Empty; // o(n) iterator (opposed to o(MaxNpcs))

        public NpcProvider(ScapeServer server)
        {
            Server = server;
            _npcs = new Npc[MaxNpcs];
        }

        public async Task<Npc> Spawn(int npcDefId, int x, int y, int z)
        {
            Npc npc;

            lock (_modifyLock)
            {
                var nid = -1;
                for (int i = 0; i < MaxNpcs; i++)
                {
                    if (_npcs[i] != null)
                        continue;

                    nid = i;
                    break;
                }

                if (nid == -1)
                {
                    Logs.Warning(this, $"Failed allocating npc id for def {npcDefId}. _npcs count: {_npcs.Length}");
                    return null;
                }

                npc = new Npc(Server, npcDefId, nid);
                _npcs[nid] = npc;
                Npcs = Npcs.AddLast(npc);
            }

            await npc.Position.Teleport(x, y, z);
            return npc;
        }

        public void Remove(Npc npc)
        {
            if (IsInvalidIndex(npc.NpcId))
                return;

            npc.Position.LeaveAllMaps();

            lock (_modifyLock)
            {
                _npcs[npc.NpcId] = null;


                var index = Npcs.FindIndex(npc);
                if (index.IsNone)
                {
                    Logs.Warning(this, $"Tried to remove unregistered npcId {npc.NpcId} npcDef {npc.NpcDefinitionId}");
                    return;
                }
                Npcs.RemoveAt(index.Value);
            }
        }

        public Npc GetById(int npcId)
            => IsInvalidIndex(npcId) ? null : _npcs[npcId];

        private bool IsInvalidIndex(int index)
        {
            if (index < 0 || index >= MaxNpcs)
            {
                Logs.Warning(this, $"Npc index is out of range : {index }");
                return true;
            }
            return false;
        }
    }
}

