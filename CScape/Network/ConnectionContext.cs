﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.IO;
using CScape.Network.State;
using CScape.Network.Synchronization.Messages;
using CScape.Server;

namespace CScape.Network
{
    public sealed class ConnectionContext : IDisposable
    {
        public const int BufferSize = 1024*32;
        public const int TimeoutMs = 3000;
        public byte[] ReceiveBuffer { get; } = new byte[BufferSize];

        public int ConnectionId { get; }
        public SocketManager Parent { get; }
        public Socket IO { get; }
        public LogManager Logs { get; }

        public bool IsReceiving { get; private set; }

        public IStateHandler Handler { get; set; }
        public IsaacRandomGen IsaacOut { get; set; }
        public Player Player { get; set; }
        public ScapeServer Server => Parent.Parent;

        public bool IsDead => Handler == null || !IO.Connected;

        public PacketBuilder Builder(int size = PacketBuilder.BufferSize)
            => Player == null ? 
            new PacketBuilder(Server, null, size) : 
            new PacketBuilder(Player, size);

        public bool IsDisposed { get; private set; }

        public ConnectionContext(Socket io, int connectionId, SocketManager parent)
        {
            Parent = parent;
            Handler = new ConnectionInitStateHandler(this);

            IO = io;
            ConnectionId = connectionId;
            Logs = parent.Parent.Logs;
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;
            IsDisposed = true;

            Logs.Debug(this, "Disposing connection.", conid: ConnectionId);
            Player = null;
            Parent.Contexts[ConnectionId] = null;
            Handler = null;
            IsReceiving = false;
            IO?.Close();
            IO?.Dispose();
        }

        private void LogoutOrDispose()
        {
            if (Player != null)
                Server.PlayerProvider.CleanupPlayer(Player);

            Dispose();
        }

        public void StartReceive()
        {
            if (IsReceiving)
                Debug.Fail("Already receiving.");

            IsReceiving = true;

            var task = Task.Run(InternalReceive);
            task.ContinueWith(t =>
            {
                if (t.Exception == null)
                {
                    if(!IsDisposed)
                        LogoutOrDispose();

                    return;
                }
#if DEBUG
                /*
                 * if you end up here, try:
                 * https://stackoverflow.com/questions/9218348/find-where-rethrown-exception-was-originally-thrown-using-visual-studio-c-sharp
                 */

                foreach(var ex in t.Exception.Flatten().InnerExceptions)
                    ExceptionDispatchInfo.Capture(ex).Throw();
#endif
#if RELEASE
                foreach (var ex in t.Exception.Flatten().InnerExceptions)
                    Logs.Exception(this, "InternalReceive exception", ex, ConnectionId);
#endif
            });
        }

        public async Task InternalReceive() 
        {
            try
            {
                int received;
                do
                {
                    if (IsDead)
                    {
                        Logs.Debug(this, "Stoped internal receiving (IsDead)", conid: ConnectionId);
                        LogoutOrDispose();
                        return;
                    }

                    var receiveTask =
                        Task.Factory.FromAsync(
                            IO.BeginReceive(ReceiveBuffer, 0, BufferSize, SocketFlags.None, null, null),
                            IO.EndReceive);

                    if (await DidTimeout(receiveTask))
                        return;

                    if (receiveTask.Exception != null)
                    {
                     //   ExceptionDispatchInfo.Capture(receiveTask.Exception.InnerException).Throw();
                        break;
                    }
                        received = receiveTask.Result;
                    Server.Stats.CurrentTick.BytesReceived += received;
                    Logs.Debug(this, $"Received {received} bytes.", conid: ConnectionId);

                    if (Handler == null)
                    {
                        Logs.Debug(this, "Stopped internal receiving (Null handler)", conid: ConnectionId);
                        break;
                    }
                    await Handler.Handle(new RsPacketStream(ReceiveBuffer, 0, received, Parent.Parent, this));

                } while (received != 0);

                Logs.Warning(this, "Received 0 bytes", conid: ConnectionId);
            }

            catch (SocketException socketEx)
            {
                Logs.Warning(this, "SocketEx in InternalReceive.", socketEx, ConnectionId);
            }
            catch (ObjectDisposedException objde)
            {
                Logs.Warning(this, "ObjDisposedEx in InternalReceive.", objde, ConnectionId);
            }
            LogoutOrDispose();
        }

        public async Task<int> SendMessage(ISynchronizationMessage msg)
        {
            if (IsDead)
                return 0;
            return await SendPacket(msg.GetPacket(this), false, false);
        }

        public async Task<int> SendPacket(PacketBuilder builder, bool reuseForBitwise = false, bool reset = true)
        {
            if (builder.Position == 0)
                return 0;

            var packet = builder.ToByteArray(reuseForBitwise, reset);
            return await SendBytes(packet.Buffer, packet.Start, packet.Size);
        }

        public async Task<int> SendBytes(byte[] buffer, int offset, int size)
        {
            try
            {
                if (IsDead)
                {
                    Logs.Debug(this, "Send bytes killed(IsDead)", conid: ConnectionId);
                    LogoutOrDispose();

                    return 0;
                }

                var sendTask = Task.Factory.FromAsync(
                    IO.BeginSend(buffer, offset, size, SocketFlags.None, null, null),
                    IO.EndSend);

                if (await DidTimeout(sendTask))
                    return 0;

                if (sendTask.Exception != null)
                    ExceptionDispatchInfo.Capture(sendTask.Exception.Flatten().InnerException).Throw();
                else
                {
                    var sent = sendTask.Result;
                    Server.Stats.CurrentTick.PacketsSent++;
                    Server.Stats.CurrentTick.BytesSent+=sent;
                    Logs.Debug(this, $"Sent {sent} bytes.", conid: ConnectionId);
                }
            }
            catch (SocketException socketEx)
            {
                Logs.Warning(this, "SocketEx in SendBytes.", socketEx, ConnectionId);
                LogoutOrDispose();
            }
            catch (ObjectDisposedException objde)
            {
                Logs.Warning(this, "ObjDisposedEx in SendBytes.", objde, ConnectionId);
                LogoutOrDispose();
            }
            catch (AggregateException aggrex)
            {
                Logs.Warning(this, "AggregateException in SendBytes", aggrex, ConnectionId);
                LogoutOrDispose();
            }

            return 0;
        }

        private async Task<bool> DidTimeout(Task task)
        {
            if (await Task.WhenAny(task, Task.Delay(TimeoutMs)) != task)
            {
                Logs.Warning(this, "Timeout.", conid: ConnectionId);
                Dispose();
                return true;
            }

            return false;
        }

        public async Task<int> Send(OutPacketType packet)
        {
            var builder = Builder(sizeof(byte));
            builder.Opcode(packet);
            return await SendPacket(builder);
        }

        public async Task<int> Send(OutPacketType packet, Action<PacketBuilder> builderFunc, int size = PacketBuilder.BufferSize)
        {
            var builder = Builder(size);

            builder.OpcodeAndSizePlaceholder(packet);
            builderFunc(builder);

            if (builder.SizeMeta == Globals.PacketSizeByte||
                builder.SizeMeta == Globals.PacketSizeShort)
                builder.WriteSize();

            return await SendPacket(builder);
        }
    }
}