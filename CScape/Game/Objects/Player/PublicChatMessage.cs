﻿namespace CScape.Game.Objects.Player
{
    public sealed class PublicChatMessage : IChatMessage
    {
        public enum TextColor : byte
        {
            Yellow = 0,
            Red = 1,
            Green = 2,
            Cyan = 3,
            Purple = 4,
            White = 5,
            Flash1 = 6,
            Flash2 = 7,
            Flash3 = 8,
            Glow1 = 9,
            Glow2 = 10,
            Glow3 = 11
        }

        public enum TextEffect : byte
        {
            Wave = 1,
            Wave2 = 2,
            Shake = 3,
            Scroll = 4,
            Slide = 5
        }

        public IEntity Sender { get; }
        public string Message { get; }

        public TextColor Color { get; }
        public TextEffect Effects { get; }

        public PublicChatMessage(IEntity sender, string message, byte color = 0, byte effects = 0)
        {
            Sender = sender;
            Message = message;
            if (color > 11)
                color = 0;
            Color = (TextColor) color;
            if (effects < 1 || effects > 5)
                effects = 0;
            Effects = (TextEffect) effects;
        }
    }
}
