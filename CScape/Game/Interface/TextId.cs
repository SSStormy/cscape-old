﻿using System.Collections.Generic;
namespace CScape.Game.Interface
{
    public class TextId
    {
        public int BookCloseWindow { get;  set; }
        public int BookTitle { get;  set; }
        public int BookFirstTextLine { get;  set; }
        public int BookPageOne { get;  set; }
        public int BookPageTwo { get;  set; }

        public Dictionary<int, int> LineOneLookup { get;  set; }
        public Dictionary<int, int> LineTwoLookup { get;  set; }
    }
}
