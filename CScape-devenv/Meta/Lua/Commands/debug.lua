﻿REQUIRE = {"player"}

syncMachines={}

function command (player)
	machine = syncMachines[player]
	player.sendMessage(machine == nil)
	if  machine == nil then
		machine = debugInfoSyncMachine.__new(player)
		player.syncMachines.addLast(machine)
		syncMachines[player] = machine
		player.sendMessage("Sending debug.")
	else
		player.syncMachines.remove(machine)
		machine.hide()
		player.sendMessage("Hiding debug.")
		syncMachines[player] = nil
	end
end