﻿using System;
using System.Threading.Tasks;
using CScape.Game.Container;
using CScape.Game.Objects.Player;
using CScape.Network.Synchronization.Messages;

namespace CScape.Server.Tick
{
    public sealed class SyncPlayerTickEvent : IServerTickHandler
    {
        public bool ShouldFire => true;
        public void TickOccured()
        {
            
        }

        public ScapeServer Server { get; }

        public SyncPlayerTickEvent(ScapeServer server)
        {
            Server = server;
        }

        public async Task Process()
        {
            foreach (var player in Server.PlayerProvider.Players)
            {
                try
                {
                    // sync
                    foreach (var smsg in player.SyncQueue.Flush())
                        await player.Connection.SendMessage(smsg);

                    foreach (var machine in player.SyncMachines)
                        await machine.Synchronize();
                }
                catch (NullReferenceException nrefe)
                {
                    Server.Logs.Warning(this, "NullRefEx on player sync.", nrefe);
                    continue;
                }

                player.Equipment.ShouldIgnoreOpAt = new bool[PlayerEquipment.EquipmentSize];
                player.Backpack.ShouldIgnoreOpAt = new bool[PlayerBackpack.BackpackSize];

                // post
                if (!player.HasExcessivePlayers)
                    player.ViewingDistance++;
                else
                {
                    player.ViewingDistance--;
                    player.HasExcessivePlayers = false;
                }
            }

            foreach (var player in Server.PlayerProvider.Players)
                player.ResetUpdateBools();
        }
    }
}

