using CScape.Game.Objects;

namespace CScape.Network.Synchronization.Segments
{
    public interface IEntityUpdateSegment : IUpdateSegment
    {
        ISyncFlagWriter FlagWriter { get; }
        IEntity Entity { get; }
    }
}