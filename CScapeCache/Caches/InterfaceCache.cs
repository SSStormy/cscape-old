﻿using System;
using System.Collections.Generic;
using CScapeCache.Core;
using CScapeCache.Model;

namespace CScapeCache.Caches
{
    public sealed class InterfaceCache : AbstractCache
    {
        private DataBuffer stream;

        private RsInterface[] _cache;

        public IEnumerable<RsInterface> All => Array.AsReadOnly(_cache);
        public int TotalObjects { get; private set; }

        public InterfaceCache(CacheReader reader) : base(reader)
        {
            stream = reader.SubArchives[3].ExtractFile("data");
        }

        protected override void InternalLoad()
        {
            int i = -1;
            TotalObjects = stream.ReadShort();
            _cache= new RsInterface[TotalObjects];

            while (stream.Location < stream.Buffer.Length)
            {
                int k = stream.ReadShort();
                if (k == 65535)
                {
                    i = stream.ReadShort();
                    k = stream.ReadShort();
                }
                var rsInterface = new RsInterface(this);
                rsInterface.Read(stream, k, i);
                _cache[k] =  rsInterface;
            }
        }
    }
}
