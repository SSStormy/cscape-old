﻿REQUIRE = { "player", "server" }

function command (player, server)
	if not player.interface.canOpenMainInterface() then
		return false
	end

	local cmds = getAvailableCommands (server)
	local index = 0

	local function redraw ()
		local cmdAmnt = cmds.count
		local tc = bookTextConfig.__new (server)
		local start = index * tc.maxLines

		if start >= cmdAmnt then
			index = 0
			start = 0
		end

		tc.pageOne = tostring (index + 1)
		tc.pageTwo = tostring (index + 2)
		tc.title = "Available commands:"
		tc.closeWindow = "Close window"

		for i=0, tc.maxLines, 1 do
			local cindx = i + start
			if cindx >= cmdAmnt then
				tc.setLine (i, " ")
			else
				tc.setLine (i, cmds[cindx].scriptName)
			end
		end

		player.interface.setText (tc)
	end

	redraw ()

	player.interface.showMain (genericCloseableConfig.__new (
		server.interfaces.buttonIds.bookCloseWindow,
		server.interfaces.interfaceIds.book), false, false)

	local menu = player.interface.mainInterface

	local function prevPage ()
		if index > 0 then
			index = index - 1
			redraw ()
		end
	end

	local function nextPage ()
		index = index + 1
		redraw()
	end

	menu.buttonHandlersLua.tryAdd (server.interfaces.buttonIds.bookNext, nextPage)
	menu.buttonHandlersLua.tryAdd (server.interfaces.buttonIds.bookPrevious, prevPage)

	menu.show()
end

function getAvailableCommands(server)
-- no permission checking right now, just return all commands
	return server.luaRepo.getAllCommands()
end
