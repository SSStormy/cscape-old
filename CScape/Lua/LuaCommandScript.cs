﻿using System;
using System.IO;
using System.Threading.Tasks;
using CScape.IO;
using CScape.Server;
using MoonSharp.Interpreter;

// ReSharper disable once CheckNamespace
namespace CScape.Lua
{
    public class LuaCommandScript : LuaScript
    {
        public ArgumentProvider ArgProvider { get; }

        public string ScriptName { get; }

        public LuaCommandScript(Script script, string dir, ScapeServer server) : base(script, dir)
        {
            ArgProvider = ArgumentProvider.Build(server, this);
            ScriptName = Path.GetFileNameWithoutExtension(dir);
        }

        public static async Task<LuaScript> FromFile(string fileDir, ScapeServer server)
        {
            if (!File.Exists(fileDir))
                throw new ArgumentException($"Script file at {fileDir} doesn't exist.");

            var script = new Script();

            script.Globals["argType"] = UserData.CreateStatic<ArgumentType>();
            script.Globals["valType"] = UserData.CreateStatic<ArgumentValueType>();
            script.Globals["arg"] = UserData.CreateStatic(typeof(LuaArgFactory));

            await script.DoStreamAsync(File.Open(fileDir, FileMode.Open));

            return new LuaCommandScript(script, fileDir, server);
        }
    }
}
