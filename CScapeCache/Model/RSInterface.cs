﻿using CScapeCache.Caches;
using CScapeCache.Core;

namespace CScapeCache.Model
{
    public class RsInterface
    {
        public AbstractCache Cache { get; }
        public bool ABoolean223 { get; private set; }
        public bool ABoolean227 { get; private set; }
        public bool ABoolean235 { get; private set; }
        public bool ABoolean259 { get; private set; }
        public bool ABoolean266 { get; private set; }
        public bool ABoolean268 { get; private set; }
        public byte AByte254 { get; private set; }
        public string[] Actions { get; private set; }
        public int AnInt214 { get; private set; }
        public int AnInt216 { get; private set; }
        public int AnInt219 { get; private set; }
        public int AnInt230 { get; private set; }
        public int AnInt233 { get; private set; }
        public int AnInt239 { get; private set; }
        public int AnInt255 { get; private set; }
        public int AnInt256 { get; private set; }
        public int AnInt257 { get; private set; }
        public int AnInt258 { get; private set; }
        public int AnInt269 { get; private set; }
        public int AnInt270 { get; private set; }
        public int AnInt271 { get; private set; }
        public int[] AnIntArray245 { get; private set; }
        public string AString228 { get; private set; }
        public int AtActionType { get; private set; }
        public int[] Children { get; private set; }
        public int[] ChildX { get; private set; }
        public int[] ChildY { get; private set; }
        public int Height { get; private set; }
        public int Id { get; private set; }
        public int InvSpritePadX { get; private set; }
        public int InvSpritePadY { get; private set; }
        public bool IsInventoryInterface { get; private set; }
        public int MediaId { get; private set; }
        public string Message { get; private set; }
        public int ParentId { get; private set; }
        public int ScrollMax { get; private set; }
        public string SelectedActionName { get; private set; }
        public string SpellName { get; private set; }
        public int SpellUsableOn { get; private set; }

        public int[] SpritesX { get; private set; }
        public int[] SpritesY { get; private set; }
        public int TextColor { get; private set; }
        public string Tooltip { get; private set; }
        public int Type { get; private set; }
        public bool UsableItemInterface { get; private set; }
        public int[][] ValueIndexArray { get; private set; }
        public int Width { get; private set; }
        public byte TextDrawingAreaId { get; private set; }

        public RsInterface(AbstractCache cache)
        {
            Cache = cache;
        }

        internal void Read(DataBuffer stream, int id, int parentid)
        {
            Id = id;
            ParentId = parentid;
            Type = stream.ReadByte();
            AtActionType = stream.ReadByte();
            AnInt214 = stream.ReadShort();
            Width = stream.ReadShort();
            Height = stream.ReadShort();
            AByte254 = stream.ReadByte();
            AnInt230 = stream.ReadByte();
            if (AnInt230 != 0)
                AnInt230 = (AnInt230 - 1 << 8) + stream.ReadByte();
            else
                AnInt230 = -1;
            int i1 = stream.ReadByte();
            if (i1 > 0)
            {
                AnIntArray245 = new int[i1];
                AnIntArray245 = new int[i1];
                for (int j1 = 0; j1 < i1; j1++)
                {
                    AnIntArray245[j1] = stream.ReadByte();
                    AnIntArray245[j1] = stream.ReadShort();
                }

            }
            int k1 = stream.ReadByte();
            if (k1 > 0)
            {
                ValueIndexArray = new int[k1][];
                for (int l1 = 0; l1 < k1; l1++)
                {
                    int i3 = stream.ReadShort();
                    ValueIndexArray[l1] = new int[i3];
                    for (int l4 = 0; l4 < i3; l4++)
                        ValueIndexArray[l1][l4] = stream.ReadShort();

                }

            }
            if (Type == 0)
            {
                ScrollMax = stream.ReadShort();
                ABoolean266 = stream.ReadByte() == 1;
                int i2 = stream.ReadShort();
                Children = new int[i2];
                ChildX = new int[i2];
                ChildY = new int[i2];
                for (int j3 = 0; j3 < i2; j3++)
                {
                    Children[j3] = stream.ReadShort();
                    ChildX[j3] = stream.ReadSignedWord();
                    ChildY[j3] = stream.ReadSignedWord();
                }

            }
            if (Type == 1)
            {
                stream.ReadShort();
                stream.ReadByte();
            }
            if (Type == 2)
            {
                ABoolean259 = stream.ReadByte() == 1;
                IsInventoryInterface = stream.ReadByte() == 1;
                UsableItemInterface = stream.ReadByte() == 1;
                ABoolean235 = stream.ReadByte() == 1;
                InvSpritePadX = stream.ReadByte();
                InvSpritePadY = stream.ReadByte();
                SpritesX = new int[20];
                SpritesY = new int[20];
                for (int j2 = 0; j2 < 20; j2++)
                {
                    int k3 = stream.ReadByte();
                    if (k3 == 1)
                    {
                        SpritesX[j2] = stream.ReadSignedWord();
                        SpritesY[j2] = stream.ReadSignedWord();
                        stream.ReadString();
                    }
                }

                Actions = new string[5];
                for (int l3 = 0; l3 < 5; l3++)
                {
                    Actions[l3] = stream.ReadString();
                    if (Actions[l3].Length == 0)
                        Actions[l3] = null;
                }

            }
            if (Type == 3)
                ABoolean227 = stream.ReadByte() == 1;
            if (Type == 4 || Type == 1)
            {
                ABoolean223 = stream.ReadByte() == 1;
                var k2 = stream.ReadByte();
                TextDrawingAreaId = k2;
                ABoolean268 = stream.ReadByte() == 1;
            }
            if (Type == 4)
            {
                Message = stream.ReadString();
                AString228 = stream.ReadString();
            }
            if (Type == 1 || Type == 3 || Type == 4)
                TextColor = stream.ReadInteger();
            if (Type == 3 || Type == 4)
            {
                AnInt219 = stream.ReadInteger();
                AnInt216 = stream.ReadInteger();
                AnInt239 = stream.ReadInteger();
            }
            if (Type == 5)
            {
                stream.ReadString();
                stream.ReadString();
            }
            if (Type == 6)
            {
                int l = stream.ReadByte();
                if (l != 0)
                {
                    AnInt233 = 1;
                    MediaId = (l - 1 << 8) + stream.ReadByte();
                }
                l = stream.ReadByte();
                if (l != 0)
                {
                    AnInt255 = 1;
                    AnInt256 = (l - 1 << 8) + stream.ReadByte();
                }
                l = stream.ReadByte();
                if (l != 0)
                    AnInt257 = (l - 1 << 8) + stream.ReadByte();
                else
                    AnInt257 = -1;
                l = stream.ReadByte();
                if (l != 0)
                    AnInt258 = (l - 1 << 8) + stream.ReadByte();
                else
                    AnInt258 = -1;
                AnInt269 = stream.ReadShort();
                AnInt270 = stream.ReadShort();
                AnInt271 = stream.ReadShort();
            }
            if (Type == 7)
            {
                ABoolean223 = stream.ReadByte() == 1;
                TextDrawingAreaId = stream.ReadByte();

                ABoolean268 = stream.ReadByte() == 1;
                TextColor = stream.ReadInteger();
                InvSpritePadX = stream.ReadSignedWord();
                InvSpritePadY = stream.ReadSignedWord();
                IsInventoryInterface = stream.ReadByte() == 1;
                Actions = new string[5];
                for (int k4 = 0; k4 < 5; k4++)
                {
                    Actions[k4] = stream.ReadString();
                    if (Actions[k4].Length == 0)
                        Actions[k4] = null;
                }
            }
            if (AtActionType == 2 || Type == 2)
            {
                SelectedActionName = stream.ReadString();
                SpellName = stream.ReadString();
                SpellUsableOn = stream.ReadShort();
            }

            if (Type == 8)
                Message = stream.ReadString();

            if (AtActionType == 1 || AtActionType == 4 || AtActionType == 5 || AtActionType == 6)
            {
                Tooltip = stream.ReadString();
                if (Tooltip.Length == 0)
                {
                    if (AtActionType == 1)
                        Tooltip = "Ok";
                    if (AtActionType == 4)
                        Tooltip = "Select";
                    if (AtActionType == 5)
                        Tooltip = "Select";
                    if (AtActionType == 6)
                        Tooltip = "Continue";
                }
            }
        }
    }
}