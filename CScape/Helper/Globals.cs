﻿namespace CScape.Helper
{
    public static class Globals
    {
        public const int Revision = 317;

        public const string LatestCommitFileDir = @"Meta/last-commit";

        public const byte NullTerminator = 10;
        public const int MaxChatMsgLength = 80;

        public const int PacketSizeByte = -1;
        public const int PacketSizeShort = -2;
        public const int PacketSizeUnknown= -3;

        public const int MaxViewingDistance = 15;
        public const int MaxUpdatePlayers = 256;

        public const string DefaultGreeting = "Welcome to Runescape.";

        public static readonly int[] MaskForBit =
        {
            0, 1, 3, 7, 15, 31, 63, 127, 255, 511,
            1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143, 524287,
            1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727, 268435455, 536870911,
            1073741823, 2147483647,
        };
    }
}
