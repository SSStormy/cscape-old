﻿using System;
using System.Text;
using CScape.Helper;

namespace CScape.IO
{
    public class RsStream
    {
        private readonly byte[] _buffer;

        public int Position { get; set; }
        public int Length { get; }

        public bool IsEos => Position >= Length;

        private int _bitPosition;

        public RsStream(byte[] buffer)
        {
            Length = buffer.Length;
            _buffer = new byte[Length];
            Buffer.BlockCopy(buffer, 0, _buffer, 0, Length);
        }

        public RsStream(byte[] buffer, int start, int size)
        {
            Length = size;
            _buffer = new byte[size];
            Buffer.BlockCopy(buffer, start, _buffer, 0, size);
        }

        /// <summary>
        /// Reads a rs string. Returns 
        /// </summary>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public bool ReadRsString(int maxLength, out string rsString)
        {
            var builder = new StringBuilder(maxLength);
            var retval = true;

            try
            {
                byte c;
                while ((c = ReadUnsignedByte()) != Globals.NullTerminator)
                {
                    builder.Append(Convert.ToChar(c));
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                retval = false;
            }

            rsString = builder.ToString();
            return retval;
        }

        public void FlushInto(byte[] flushTarget, int offsetInFlushTarget, int offsetInInternalBuffer,
            int flushByteAmount, bool usePositionCounter)
        {
            if(flushTarget.Length > offsetInFlushTarget + flushByteAmount) throw new ArgumentException("flushTarget.Length > offsetInFlushTarget + flushByteAmount");
            if(_buffer.Length > offsetInInternalBuffer + flushByteAmount) throw new ArgumentException("_buffer.Length > offsetInInternalBuffer + flushByteAmount");

            for (var i = 0; i < flushByteAmount; i++)
            {
                flushTarget[i + offsetInFlushTarget] = usePositionCounter
                    ? _buffer[offsetInInternalBuffer + Position++]
                    : _buffer[i + offsetInInternalBuffer];
            }
        }

        public byte ReadUnsignedByte()
            => (byte) (_buffer[Position++] & 0xff);

        public int ReadSignedByte()
            => (sbyte) _buffer[Position++];

        public int ReadUnsignedWord()
        {
            Position += 2;
            return ((_buffer[Position - 2] & 0xff) << 8) + (_buffer[Position - 1] & 0xff);
        }

        public int ReadSignedWord()
        {
            Position += 2;
            int i = ((_buffer[Position - 2] & 0xff) << 8) +
                    (_buffer[Position - 1] & 0xff);
            if (i > 32767)
                i -= 0x10000;
            return i;
        }

        public int ReadDWord()
        {
            Position += 4;
            return ((_buffer[Position - 4] & 0xff) << 24) + ((_buffer[Position - 3] & 0xff) << 16) +
                   ((_buffer[Position - 2] & 0xff) << 8) + (_buffer[Position - 1] & 0xff);
        }

        public long ReadQWord()
        {
            var l = ReadDWord() & 0xffffffffL;
            var l1 = ReadDWord() & 0xffffffffL;
            return (l << 32) + l1;
        }

        public int Read3Bytes()
        {
            Position += 3;
            return ((_buffer[Position - 3] & 0xff) << 16) + ((_buffer[Position - 2] & 0xff) << 8) + (_buffer[Position - 1] & 0xff);
        }

        public void InitBitAccess()
            => _bitPosition = Position*8;

        public int ReadBits(int i)
        {
            var k = _bitPosition >> 3;
            var l = 8 - (_bitPosition & 7);
            var i1 = 0;
            _bitPosition += i;
            for (; i > l; l = 8)
            {
                i1 += (_buffer[k++] & Globals.MaskForBit[l]) << i - l;
                i -= l;
            }
            if (i == l)
                i1 += _buffer[k] & Globals.MaskForBit[l];
            else
                i1 += _buffer[k] >> l - i & Globals.MaskForBit[i];
            return i1;
        }

        public void FinishBitAccess()
            => Position = (_bitPosition + 7)/8;
    }
}