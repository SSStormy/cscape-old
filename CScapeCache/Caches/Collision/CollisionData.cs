using System;

namespace CScapeCache.Caches.Collision
{
    public sealed class CollisionData
    {
        public CollisionMasks CollisionMask { get; private set; }

        public static CollisionData None => new CollisionData(CollisionMasks.Open);

        public bool IsBlocked
            => IsCompletely(CollisionMasks.Closed) || Has(CollisionMasks.Blocked) || Has(CollisionMasks.Occupied) || Has(CollisionMasks.Solid);

        public CollisionData(CollisionMasks collisionMask)
        {
            CollisionMask = collisionMask;
        }

        internal CollisionData Clear()
        {
            CollisionMask = 0;
            return this;
        }

        internal CollisionData Add(CollisionMasks flag)
        {
            CollisionMask |= flag;
            return this;
        }

        internal CollisionData Add(CollisionMasks flag, bool condition)
        {
            if (condition)
                Add(flag);

            return this;
        }

        internal CollisionData Clear(CollisionMasks flag)
        {
            CollisionMask &= ~flag;
            return this;
        }

        public bool Has(CollisionMasks flag)
        {
            return (CollisionMask & flag) != 0;
        }

        public bool IsCompletely(CollisionMasks flag)
        {
            return CollisionMask == flag;
        }

        public bool IsEmpty()
        {
            return IsCompletely(0);
        }

        public override bool Equals(object flag)
        {
            if (flag is CollisionMasks)
                return CollisionMask == (CollisionMasks)flag;

            var bitMask = flag as CollisionData;
            return CollisionMask == bitMask?.CollisionMask;
        }
    }
}