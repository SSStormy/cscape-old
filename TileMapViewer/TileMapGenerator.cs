﻿using System;
using System.Linq;
using CScapeCache;
using CScapeCache.Caches;
using CScapeCache.Model;

namespace TileMapViewer
{
    public sealed class TileMapGenerator
    {
        private Random _rng = new Random();
        private readonly int[] _textureMap = new int[0x10000];

        public TileMapGenerator()
        {
            GenerateTextures(0.80000000000000004D);
        }

        public delegate void DrawTile(int x, int y, byte rr, byte gg, byte bb);

        private int method182(MapModel map, int i, int j, int k)
        {
            if ((map.RenderRuleFlags[j, k, i] & 8) != 0)
                return 0;
            if (j > 0 && (map.RenderRuleFlags[1, k, i] & 2) != 0)
                return j - 1;
            return j;
        }

        public void RenderMap(CacheReader reader, MapModel map, DrawTile drawDelegate, int plane)
        {
            var anIntArray124 = new int[MapModel.CoordAxisLength];
            var anIntArray125 = new int[MapModel.CoordAxisLength];
            var anIntArray126 = new int[MapModel.CoordAxisLength];
            var anIntArray127 = new int[MapModel.CoordAxisLength];
            var anIntArray128 = new int[MapModel.CoordAxisLength];

            for (var z = 0; z < MapModel.MaxPlanes; z++)
            {
                for (var x = 0; x < MapModel.CoordAxisLength; x++)
                {
                    for (var y = 0; y < MapModel.CoordAxisLength; y++)
                    {
                        var l12 = map.UnderlayFloorIds[z, x, y] & 0xff;
                        if (l12 > 0)
                        {
                            var flo = reader.FloCache.GetId(l12 - 1);
                            anIntArray124[y] += flo.anInt397;
                            anIntArray125[y] += flo.anInt395;
                            anIntArray126[y] += flo.anInt396;
                            anIntArray127[y] += flo.anInt398;
                            anIntArray128[y]++;
                        }
                    }

                    int l9 = 0;
                    int j13 = 0;
                    int j14 = 0;
                    int k15 = 0;
                    int k16 = 0;
                    for (var y = 0; y < MapModel.CoordAxisLength; y++)
                    {
                        l9 += anIntArray124[y];
                        j13 += anIntArray125[y];
                        j14 += anIntArray126[y];
                        k15 += anIntArray127[y];
                        k16 += anIntArray128[y];

                        if ((map.RenderRuleFlags[0, x, y] & 2) != 0 ||
                            (map.RenderRuleFlags[z, x, y] & 0x10) == 0 && method182(map, y, z, x) == plane)
                        {
                            int l18 = map.UnderlayFloorIds[z,x,y] & 0xff;
                            int i19 = map.OverlayFloorIds[z, x, y] & 0xff;
                            if (l18 > 0 || i19 > 0)
                            {
                                /*
                                int j19 = map.VertexHeights[z, x, y];
                                int k19 = tileVertexHeights[l][l6 + 1][k17];
                                int l19 = tileVertexHeights[l][l6 + 1][k17 + 1];
                                int i20 = tileVertexHeights[z, x, y + 1];
                                int j20 = anIntArrayArray139[l6][k17];
                                int k20 = anIntArrayArray139[l6 + 1][k17];
                                int l20 = anIntArrayArray139[l6 + 1][k17 + 1];
                                int i21 = anIntArrayArray139[l6][k17 + 1];
                                */
                                int j21 = -1;
                                int k21 = -1;
                                if (l18 > 0)
                                {
                                    int l21 = (l9*256)/k15;
                                    int j22 = j13/k16;
                                    int l22 = j14/k16;
                                    j21 = method177(l21, j22, l22);
                                    // l21 = l21 + anInt123 & 0xff; (anInt123 = random -8 => n => 8)
                                    //l22 += anInt133; (anInt133 = random -16 => n => 16)
                                    if (l22 < 0)

                                        l22 = 0;
                                    else if (l22 > 255)
                                        l22 = 255;
                                    k21 = method177(l21, j22, l22);
                                }
                                /*
                                if (z > 0)
                                {
                                    var flag = true;
                                    if (l18 == 0 && map.OverlayClippingPaths[z,x,y] != 0)
                                        flag = false;
                                    if (i19 > 0 && !reader.FloCache.GetId(i19 - 1).aBoolean393)
                                        flag = false;
                                    if (flag && j19 == k19 && j19 == l19 && j19 == i20)
                                        anIntArrayArrayArray135[l][l6][k17] |= 0x924;
                                }
                                */
                                int baseMapTexture = 0; // terrain etc, not walls or walkways
                                if (j21 != -1)
                                {
                                    baseMapTexture = _textureMap[method187(k21, 96)];
                                }
                                if (i19 == 0)
                                {
                                    drawDelegate(x + map.BaseX, y + map.BaseY, (byte)(baseMapTexture >> 16), (byte)(baseMapTexture >> 8), (byte)(baseMapTexture));
                                    /*
                                    worldController.method279(l, l6, k17, 0, 0, -1, j19, k19, l19, i20,
                                        method187(j21, j20), method187(j21, k20), method187(j21, l20),
                                        method187(j21, i21), 0, 0, 0, 0, baseMapTexture, 0);
                                        */
                                }
                                else
                                {
                                    //int k22 = map.OverlayClippingPaths[z,x,y] + 1;
                                    //byte byte4 = tileOverlayRotations[z, x, y];
                                    var flo_2 = reader.FloCache.GetId(i19 - 1);
                                    int i23 = flo_2.anInt391;
                                    int mapTexture;

                                    if (i23 >= 0)
                                    {
                                        // complex textures (wood, brick etc)
                                        //  mapTexture = method369(i23);
                                        mapTexture = 0;
                                    }
                                    else if (flo_2.anInt390 == 0xff00ff)
                                    {
                                        mapTexture = 0;
                                        i23 = -1;
                                    }
                                    else
                                    {
                                        // walkways/roads
                                        mapTexture = _textureMap[method185(flo_2.anInt399, 96)];
                                    }

                                    drawDelegate(x + map.BaseX, y + map.BaseY, (byte)(mapTexture >> 16), (byte)(mapTexture >> 8), (byte)(mapTexture));

                                    /*
                                    worldController.method279(l, l6, k17, k22, byte4, i23, j19, k19, l19, i20,
                                        method187(j21, j20), method187(j21, k20), method187(j21, l20),
                                        method187(j21, i21), method185(worldTexture, j20), method185(worldTexture, k20),
                                        method185(worldTexture, l20), method185(worldTexture, i21), baseMapTexture,
                                        mapTexture);
                                        */
                                }
                            }
                        }
                    }
                }
            }
        }

        private int method185(int i, int j)
        {
            if (i == -2)
                return 0xbc614e;
            if (i == -1)
            {
                if (j < 0)
                    j = 0;
                else
                if (j > 127)
                    j = 127;
                j = 127 - j;
                return j;
            }
            j = (j * (i & 0x7f)) / 128;
            if (j < 2)
                j = 2;
            else
            if (j > 126)
                j = 126;
            return (i & 0xff80) + j;
        }

        public void GenerateTileMap(CacheReader reader, DrawTile drawDelegate, int plane = 0)
        {
            if (reader == null) throw new ArgumentNullException(nameof(reader));
            if (drawDelegate == null) throw new ArgumentNullException(nameof(drawDelegate));

            LoadIfNotLoaded(reader.FloCache);
            LoadIfNotLoaded(reader.MapIndexCache);
            LoadIfNotLoaded(reader.MapCache);

            foreach (var map in reader.MapIndexCache.All.Select(index => reader.MapCache.GetMap(index, 0, 0)))
                RenderMap(reader, map, drawDelegate, plane);
        }


        private static void LoadIfNotLoaded(AbstractCache cache)
        {
            if(!cache.IsLoaded)
                cache.Load();
        }

        public void GenerateTextures(double seed)
        {
            seed += _rng.NextDouble() * 0.029999999999999999D - 0.014999999999999999D;
            int j = 0;
            for (int k = 0; k < 512; k++)
            {
                double d1 = (double)(k / 8) / 64D + 0.0078125D;
                double d2 = (double)(k & 7) / 8D + 0.0625D;
                for (int k1 = 0; k1 < 128; k1++)
                {
                    double d3 = (double)k1 / 128D;
                    double d4 = d3;
                    double d5 = d3;
                    double d6 = d3;
                    if (d2 != 0.0D)
                    {
                        double d7;
                        if (d3 < 0.5D)
                            d7 = d3 * (1.0D + d2);
                        else
                            d7 = (d3 + d2) - d3 * d2;
                        double d8 = 2D * d3 - d7;
                        double d9 = d1 + 0.33333333333333331D;
                        if (d9 > 1.0D)
                            d9--;
                        double d10 = d1;
                        double d11 = d1 - 0.33333333333333331D;
                        if (d11 < 0.0D)
                            d11++;
                        if (6D * d9 < 1.0D)
                            d4 = d8 + (d7 - d8) * 6D * d9;
                        else
                        if (2D * d9 < 1.0D)
                            d4 = d7;
                        else
                        if (3D * d9 < 2D)
                            d4 = d8 + (d7 - d8) * (0.66666666666666663D - d9) * 6D;
                        else
                            d4 = d8;
                        if (6D * d10 < 1.0D)
                            d5 = d8 + (d7 - d8) * 6D * d10;
                        else
                        if (2D * d10 < 1.0D)
                            d5 = d7;
                        else
                        if (3D * d10 < 2D)
                            d5 = d8 + (d7 - d8) * (0.66666666666666663D - d10) * 6D;
                        else
                            d5 = d8;
                        if (6D * d11 < 1.0D)
                            d6 = d8 + (d7 - d8) * 6D * d11;
                        else
                        if (2D * d11 < 1.0D)
                            d6 = d7;
                        else
                        if (3D * d11 < 2D)
                            d6 = d8 + (d7 - d8) * (0.66666666666666663D - d11) * 6D;
                        else
                            d6 = d8;
                    }
                    int l1 = (int)(d4 * 256D);
                    int i2 = (int)(d5 * 256D);
                    int j2 = (int)(d6 * 256D);
                    int k2 = (l1 << 16) + (i2 << 8) + j2;
                    k2 = method373(k2, seed);
                    if (k2 == 0)
                        k2 = 1;
                    _textureMap[j++] = k2;
                }

            }
        }

        private int method177(int i, int j, int k)
        {
            if (k > 179)
                j /= 2;
            if (k > 192)
                j /= 2;
            if (k > 217)
                j /= 2;
            if (k > 243)
                j /= 2;
            return (i / 4 << 10) + (j / 32 << 7) + k / 2;
        }

        private static int method373(int i, double d)
        {
            double d1 = (double)(i >> 16) / 256D;
            double d2 = (double)(i >> 8 & 0xff) / 256D;
            double d3 = (double)(i & 0xff) / 256D;
            d1 = Math.Pow(d1, d);
            d2 = Math.Pow(d2, d);
            d3 = Math.Pow(d3, d);
            int j = (int)(d1 * 256D);
            int k = (int)(d2 * 256D);
            int l = (int)(d3 * 256D);
            return (j << 16) + (k << 8) + l;
        }

        private static int method187(int i, int j)
        {
            if (i == -1)
                return 0xbc614e;
            j = (j * (i & 0x7f)) / 128;
            if (j < 2)
                j = 2;
            else
            if (j > 126)
                j = 126;
            return (i & 0xff80) + j;
        }
    }
}
