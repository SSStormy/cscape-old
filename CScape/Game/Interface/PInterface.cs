using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Network.Synchronization;
using CScape.Network.Synchronization.Machine;
using CScape.Network.Synchronization.Messages;

namespace CScape.Game.Interface
{
    public sealed class PInterface
    {
        private sealed class InterfaceSyncMachine : AbstractSynchronizationMachine
        {
            public PInterface Parent { get; }
            public bool[] IsNew { get; }
            public IInterface[] Interfaces => Parent._interf;

            public InterfaceSyncMachine(Player player, PInterface parent) : base(player)
            {
                Parent = parent;
                IsNew = new bool[TotalInterfaceAmount];
            }

            public override async Task Synchronize()
            {
                for (var i = 0; i < Interfaces.Length; i++)
                {
                    if (Interfaces[i] == null)
                        continue;

                    var context = Interfaces[i].ContextRetriever.GetContext(Player);
                    if (context == null)
                    {
                        IsNew[i] = false; // just in case
                        continue;
                    }

                    if (IsNew[i])
                    {
                        await SyncWithContext(i, context.GetInitialSync());
                        IsNew[i] = false;
                        continue;
                    }

                    if (!context.NeedsSync)
                        continue;

                    await SyncWithContext(i, context.GetSync());
                }
            }

            private async Task SyncWithContext(int index, InterfaceSyncData data)
            {
                if (data.ShouldClose)
                {
                    Interfaces[index] = null;
                    Debug.Assert(data.Messages.Any());
                }

                foreach (var message in data.Messages)
                    await Player.Connection.SendMessage(message);
            }
        }

        public const int OtherInterfaceAmount = 2;
        public const int TotalInterfaceAmount = OtherInterfaceAmount + MaxSidebarInterfaceAmount;
        private const int SidebarStartIndex = 2;
        public const int MaxSidebarInterfaceAmount = 14;

        public Player Player { get; }

        private readonly IInterface[] _interf = new IInterface[TotalInterfaceAmount];

        public IInterface Main
        {
            get { return _interf[0]; }
            set { SetInterface(0, value); }
        }

        public IInterface Walkable
        {
            get { return _interf[1]; }
            set { SetInterface(1, value); }
        }

        public IReadOnlyList<IInterface> Sidebar
            => new ArraySegment<IInterface>(_interf, SidebarStartIndex, MaxSidebarInterfaceAmount);

        private readonly InterfaceSyncMachine _sync;

        public PInterface(Player player)
        {
            Player = player;
            _sync = new InterfaceSyncMachine(Player, this);
            Player.SyncMachines.AddFirst(_sync);
        }

        /// <returns>True if interface was set successfully, false otherwise.</returns>
        public bool SetSidebar(int sidebarIndex, IInterface sidebar)
        {
            var index = SidebarStartIndex + sidebarIndex;
            Debug.Assert(index >= 2);
            return SetInterface(index, sidebar);
        }

        /// <returns>True if interface was set successfully, false otherwise.</returns>
        private bool SetInterface(int index, IInterface newVal)
        {
            Debug.Assert(index > 0 && index < TotalInterfaceAmount);

            if (_interf[index] == null)
            {
                _sync.IsNew[index] = newVal != null;
                _interf[index] = newVal;
                return true;
            }

            IEnumerable<ISynchronizationMessage> backlog;
            var interf = _interf[index];
            var overrideOvrStrg = interf.ContextRetriever.GetContext(Player).OverrideStrategy;

            if ((overrideOvrStrg != null && overrideOvrStrg.CanOverride(interf, newVal, out backlog)) ||
                interf.BaseOverrideStrategy.CanOverride(interf, newVal, out backlog))
            {
                if (backlog != null)
                    Player.SyncQueue.AddFirstRange(backlog);

                _sync.IsNew[index] = newVal != null;
                _interf[index] = newVal;
                return true;
            }
            return false;
        }
    }
}