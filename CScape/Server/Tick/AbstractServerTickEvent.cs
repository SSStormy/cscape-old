﻿using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    public abstract class AbstractServerTickEvent : IServerTickHandler
    {
        public ScapeServer Server { get; }

        public AbstractServerTickEvent(ScapeServer server)
        {
            Server = server;
        }

        public abstract bool ShouldFire { get; }
        public abstract void TickOccured();
        public abstract Task Process();
    }
}