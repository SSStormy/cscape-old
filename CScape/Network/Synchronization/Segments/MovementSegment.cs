using CScape.Game.Objects;
using CScape.IO;

namespace CScape.Network.Synchronization.Segments
{
    public sealed class MovementSegment : IEntityUpdateSegment
    {
        public MovementSegment(IEntity entity, ISyncFlagWriter flagWriter)
        {
            Entity = entity;
            FlagWriter = flagWriter;
        }

        public void WriteInto(PacketBuilder packet)
        {
            Entity.Position.Follow.GenerateMovement();
            Entity.Position.ProcessMovement();

            var needsUpdate = FlagWriter.NeedsWriting;
            if (Pos.DidJustTeleport)
            {
                packet.WriteBits(1, 1);
                packet.WriteBits(2, 3);
                packet.WriteBits(2, Pos.GlobalZ);
                packet.WriteBits(1, Pos.IsRegionDirty ? 0 : 1);
                packet.WriteBits(1, needsUpdate ? 1 : 0);
                packet.WriteBits(7, Pos.LocalY);
                packet.WriteBits(7, Pos.LocalX);
            }
            else if (Pos.MoveType == MovementType.Running)
            {
                packet.WriteBits(1, 1);
                packet.WriteBits(2, 2);
                packet.WriteBits(3, (int)Pos.Dir1);
                packet.WriteBits(3, (int)Pos.Dir2);
                packet.WriteBits(1, needsUpdate ? 1 : 0);
            }
            else if (Pos.MoveType == MovementType.Walking)
            {
                packet.WriteBits(1, 1);
                packet.WriteBits(2, 1);
                packet.WriteBits(3, (int)Pos.Dir1);
                packet.WriteBits(1, needsUpdate ? 1 : 0);
            }
            else
            {
                if (needsUpdate)
                {
                    packet.WriteBits(1, 1);
                    packet.WriteBits(2, 0);
                }
                else
                {
                    packet.WriteBits(1, 0);
                }
            }
        }

        private PositionController Pos => Entity.Position;
        public ISyncFlagWriter FlagWriter { get; }
        public IEntity Entity { get; }
    }
}