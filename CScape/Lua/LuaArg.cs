﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace CScape.IO
{
    public enum ArgumentType
    {
        Required,
        Optional,
        Unparsed
    }

    public enum ArgumentValueType
    {
        Int,
        String
    }

    public class LuaArg
    {
        public ArgumentValueType ValType { get; }

        public ArgumentType ArgType { get; }
        public Func<string, Tuple<bool, object>> TryConverter { get; }

        public bool ContainsDefaultValue { get; }
        public object DefaultValue { get; }

        public LuaArg(ArgumentType type, Func<string, Tuple<bool, object>> converter, 
            ArgumentValueType valType)
        {
            ArgType = type;
            TryConverter = converter;
            ValType = valType;
        }

        public LuaArg(ArgumentType type, Func<string, Tuple<bool, object>> converter,
            ArgumentValueType valType, object defaultValue) : this(type, converter, valType)
        {
            ContainsDefaultValue = true;
            DefaultValue = defaultValue;
        }
    }

    public static class LuaArgFactory
    { 
        private static readonly
            Dictionary<ArgumentValueType, Func<string, Tuple<bool, object>>>
            Converters =
                new Dictionary<ArgumentValueType, Func<string, Tuple<bool, object>>>
                {
                    {
                        ArgumentValueType.Int, s =>
                        {
                            int val;
                            return int.TryParse(s, out val)
                                ? Tuple.Create(true, (object) val)
                                : Tuple.Create(false, (object) null);
                        }
                    },
                    {
                        ArgumentValueType.String, s => Tuple.Create(true, (object) s)
                    }
                };

        public static LuaArg New(ArgumentValueType valType, ArgumentType argType)
        {
            if (!Converters.ContainsKey(valType))
                throw new ArgumentException($"Unknown type identifyer {valType}.");

            return new LuaArg(argType, Converters[valType], valType);
        }

        public static LuaArg New(ArgumentValueType valType, ArgumentType argType, object defaultValue)
        {
            if(argType != ArgumentType.Optional)
                throw new ArgumentException("Only optional arguments can have default values.");

            if (!Converters.ContainsKey(valType))
                throw new ArgumentException($"Unknown type identifyer {valType}.");

            return new LuaArg(argType, Converters[valType], valType, defaultValue);
        }
    }
}
