# cscape
A Runescape 317 server emulator base written in C#/lua.

Client - https://gitlab.com/SSStormy/cscape-client

## Notable features
* Login protocol (types 15, 18)
* Player movement (as of writing, client is responsible for the pathfinding, server verifies collision)
* Player & NPC position and movement updating for clients.
* Container & interface management
* Cache reading
* Player de/serialization
* Lua as a scripting language

## Thanks/Credits
This project would not have been possible without the following people/public resources:

* https://github.com/Lin20/cachesuite for the base cache reading API
* https://www.rune-server.org/members/native%5E/ 317 client & cache
* https://www.rune-server.org/members/veer/ login & player update protocol breakdown
* https://www.rune-server.org/members/leanbow/ outbound packet schema
* The Mistex dev team | Player updating, region calculations
* https://www.rune-server.org/runescape-development/rs2-server/informative-threads/532273-landscape-objectscape-information-collision-calculation.html | Collision writeup

### License
GNU GPLv3 
