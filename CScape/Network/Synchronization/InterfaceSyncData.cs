using System.Collections.Generic;
using CScape.Network.Synchronization.Messages;

namespace CScape.Network.Synchronization
{
    public class InterfaceSyncData
    {
        public IEnumerable<ISynchronizationMessage> Messages { get; }
        public bool ShouldClose { get; }

        public InterfaceSyncData(IEnumerable<ISynchronizationMessage> messages, bool shouldClose)
        {
            Messages = messages;
            ShouldClose = shouldClose;
        }
    }
}