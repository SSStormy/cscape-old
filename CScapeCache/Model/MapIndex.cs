﻿using System;

namespace CScapeCache.Model
{
    public class MapIndex : IEquatable<MapIndex>
    {
        public int RegionHash { get; }
        public int TileMapIndex { get; }
        public int ObjectMapIndex { get; }
        public byte Priority { get; }

        public int RegionX { get; }
        public int RegionY { get; }

        internal MapIndex(int regionHash, int tileMapIndex, int objectMapIndex, byte priority)
        {
            RegionX = regionHash >> 8;
            RegionY = regionHash & 0xff;
            RegionHash = regionHash;
            TileMapIndex = tileMapIndex;
            ObjectMapIndex = objectMapIndex;
            Priority = priority;
        }

        public bool Equals(MapIndex other)
        {
            if (ReferenceEquals(this, other))
                return true;
            return other?.RegionHash == RegionHash;
        }

        public override int GetHashCode()
            => RegionHash;
    }
}
