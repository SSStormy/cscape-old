﻿using System;
using System.IO;
using CScape.Game.Objects.Player;
using CScape.Helper;
using Newtonsoft.Json;

namespace CScape.Server
{
    public sealed class ServerConfig
    {
        public const string VersionUseCommitHash = "USE_COMMIT";
        public const string UnknownVersion = "Unknown";

        public int MaxPlayers { get; }
        public int MaxNpcs { get; }

        public int SpawnX { get; set; }
        public int SpawnY { get; set; }
        public int SpawnZ { get; set; }

        public string LuaCommandsFolder { get; set; }
        public string ItemDefDir { get; set; }
        public string LuaDir { get; set; }
        public string InPacketSchemaDir { get; set; }
        public string OutPacketSchemaDir { get; set; }
        public string PassLookupDir { get; set; }
        public string PlayerSaveDir { get; set; }

        public string InterfaceIdDir { get; set; }
        public string ButtonIdDir { get; set; }
        public string TextIdDir { get; set; }
        public string ContainerId { get; set; }
        public string CacheDataDir { get; }
        public string CacheIndexBaseDir { get; }
        public string CollisionDataDir { get; set; }
        public string PrivateKeyDir { get; set; }

        public string Version { get; set; }
        public string Name { get; set; }

        public string GreetingMessage { get; set; }

        public IExpGainModifier GlobalExpModifier { get; set; }

        [JsonConstructor]
        public ServerConfig(int maxPlayers, int spawnX, int spawnY, string luaCommandsFolder, string itemDefDir,
            string inPacketSchemaDir, string luaDir, string outPacketSchemaDir, string passLookupDir,
            string playerSaveDir, string interfaceIdDir, string buttonIdDir, string textIdDir, int maxNpcs,
            string cacheDataDir,
            string cacheIndexBaseDir, string containerId, string collisionDataDir, string privateKeyDir, string version, string name, string greetingMessage)
        {
            if (luaCommandsFolder == null) throw new ArgumentNullException(nameof(luaCommandsFolder));
            if (itemDefDir == null) throw new ArgumentNullException(nameof(itemDefDir));
            if (inPacketSchemaDir == null) throw new ArgumentNullException(nameof(inPacketSchemaDir));
            if (luaDir == null) throw new ArgumentNullException(nameof(luaDir));
            if (outPacketSchemaDir == null) throw new ArgumentNullException(nameof(outPacketSchemaDir));
            if (passLookupDir == null) throw new ArgumentNullException(nameof(passLookupDir));
            if (playerSaveDir == null) throw new ArgumentNullException(nameof(playerSaveDir));
            if (interfaceIdDir == null) throw new ArgumentNullException(nameof(interfaceIdDir));
            if (buttonIdDir == null) throw new ArgumentNullException(nameof(buttonIdDir));
            if (textIdDir == null) throw new ArgumentNullException(nameof(textIdDir));
            if (cacheDataDir == null) throw new ArgumentNullException(nameof(cacheDataDir));
            if (cacheIndexBaseDir == null) throw new ArgumentNullException(nameof(cacheIndexBaseDir));
            if (containerId == null) throw new ArgumentNullException(nameof(containerId));
            if (collisionDataDir == null) throw new ArgumentNullException(nameof(collisionDataDir));
            if (privateKeyDir == null) throw new ArgumentNullException(nameof(privateKeyDir));
            if (version == null) throw new ArgumentNullException(nameof(version));
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (greetingMessage == null)
                greetingMessage = Globals.DefaultGreeting;

            MaxPlayers = maxPlayers;
            SpawnX = spawnX;
            SpawnY = spawnY;
            LuaCommandsFolder = luaCommandsFolder;
            ItemDefDir = itemDefDir;
            InPacketSchemaDir = inPacketSchemaDir;
            LuaDir = luaDir;
            OutPacketSchemaDir = outPacketSchemaDir;
            PassLookupDir = passLookupDir;
            PlayerSaveDir = playerSaveDir;
            InterfaceIdDir = interfaceIdDir;
            ButtonIdDir = buttonIdDir;
            TextIdDir = textIdDir;
            MaxNpcs = maxNpcs;
            CacheDataDir = cacheDataDir;
            CacheIndexBaseDir = cacheIndexBaseDir;
            ContainerId = containerId;
            CollisionDataDir = collisionDataDir;
            PrivateKeyDir = privateKeyDir;
            Name = name;
            GreetingMessage = greetingMessage;

            if (version == VersionUseCommitHash)
            {
                Version = File.Exists(Globals.LatestCommitFileDir)
                    ? "Commit " + File.ReadAllText( Globals.LatestCommitFileDir)
                    : UnknownVersion;
            }
            else
                Version = version;
        }
    }
}