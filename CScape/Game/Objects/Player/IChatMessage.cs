﻿namespace CScape.Game.Objects.Player
{
    public interface IChatMessage
    {
        IEntity Sender { get; }
        string Message { get; }
    }
}
