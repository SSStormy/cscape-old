﻿REQUIRE = { "player", "server" }

function command (player, server)
	if not player.interface.canOpenMainInterface() then
		return false
	end

	player.interface.showMain (characterDesignConfig.__new(server))
end