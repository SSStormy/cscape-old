﻿using System;
using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class FollowReqPacketHandler : IPacketHandler
    {
        public Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var pid = packetStream.ReadUnsignedWord();
            var player = context.Server.PlayerProvider.GetById(pid);
            if (player == null)
            {
                context.Logs.Warning(this, $"Player {context.Player} ({context.Player.PlayerId}) tried to follow unknown PID {pid}");
                return Task.FromResult(PacketResult.AllOk);
            }

            context.Player.Position.Follow.FollowedEntity = player;
            return Task.FromResult(PacketResult.AllOk);
        }
    }
}
