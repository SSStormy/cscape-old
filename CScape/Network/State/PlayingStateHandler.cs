﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Game;
using CScape.IO;
using CScape.Network.Synchronization.Messages;
using CScape.Server;

namespace CScape.Network.State
{
    public sealed class PlayingStateHandler : IStateHandler
    {
        public ConnectionContext Context { get; }

        public int[] Keys { get; }

        public IsaacRandomGen Isaac { get; }
        public LogManager Logs { get; }

        public PlayingStateHandler(ConnectionContext context, int[] keys)
        {
            Logs = context.Logs;
            Context = context;
            Keys = keys;

            Isaac = new IsaacRandomGen(Keys);

            int[] outKeys = new int[UserConnectProof.KeySize];
            Buffer.BlockCopy(keys, 0, outKeys, 0, sizeof(int)*UserConnectProof.KeySize);

            for (int i = 0; i < UserConnectProof.KeySize; i++)
                outKeys[i] += UserConnectProof.KeySalt;

            Context.IsaacOut = new IsaacRandomGen(outKeys);
        }

        public async Task PostLogin()
        {
            // send player init packet
            await Context.Send(OutPacketType.InitializePlayer, p =>
            {
                p.WriteByte(Context.Player.IsMember ? 1 : 0)
                .WriteWord(Context.Player.PlayerId);
            });

            await
                Context.SendMessage(new LoadRegionMessage(Context.Player.Position.RegionX,
                    Context.Player.Position.RegionY));

            await Context.Player.ForceShowDefaultTabs();
            await Context.Player.SendDefaultRightClickOptions();

            // todo : skill sync machines
            /*
            foreach (var skill in Context.Player.Skills.SkillList)
                await skill.SendSkillData();
                */
        }

        // for debugging
        private byte _prevOpcode;
        private RsPacketStream _prevCallReader;

        public async Task Handle(RsPacketStream reader)
        {
            try
            {
                while (true)
                {
                    if (reader.IsEos)
                        break;

                    var opcode = reader.ReadOpcode(Isaac);
                    var size = reader.ReadSize(opcode);

                    Debug.Assert(reader.Length >= size);

                    var prevPos = reader.Position;

                    Context.Server.Stats.CurrentTick.PacketsReceived++;

                    await Context.Parent.Parent.InboundPacketHandlers.Handle(
                        opcode, size, reader, Context);

                    if (reader.Position != prevPos + size)
                    {
                        Context.Dispose();
                        Debug.Fail(
                            $"{reader.Position} != {prevPos} + {size} ({prevPos + size}) packet {opcode} conid {Context.ConnectionId}");
                    }
                    _prevOpcode = opcode;
                }
            }
            catch (IndexOutOfRangeException iorex)
            {
                Logs.Exception(this, "Index of out range exception in handling of packets.", iorex, Context.ConnectionId);
                await Context.Server.PlayerProvider.LogoutPlayer(Context.Player);
            }
            _prevCallReader = reader;
        }
    }
}
