﻿using System;
using CScapeCache.Core;
using CScapeCache.Model;

namespace CScapeCache.Caches
{
    public class MapCache : AbstractLookupCache<MapIndex, MapModel>
    {
        private static readonly int[] AnIntArray1471;
        private readonly Archive _archive;

        static MapCache()
        {
            const int len = 2048;
            AnIntArray1471 = new int[len];
            for (var i = 0; i < len; i++)
                AnIntArray1471[i] = (int) (65536D*Math.Cos(i*0.0030679614999999999D));
        }

        internal MapCache(CacheReader cache) : base(cache)
        {
            _archive = cache.Archives[4];
        }

        public MapModel GetMap(MapIndex index, int baseX, int baseY)
        {
            if (index == null) throw new ArgumentNullException(nameof(index));
            if (baseX < 0) throw new ArgumentOutOfRangeException(nameof(baseX));
            if (baseY < 0) throw new ArgumentOutOfRangeException(nameof(baseY));

            ThrowIfNotLoaded();

            if (ValueLookup.ContainsKey(index))
                return ValueLookup[index];

            var tileBuffer = new DataBuffer(_archive.ExtractFile(index.TileMapIndex));

            if (baseX == 0 && baseY == 0)
            {
                // base pos is bad
                baseX = (index.RegionX * 64);
                baseY = (index.RegionY * 64);
            }

            var map = new MapModel(index, baseX, baseY);

            /*
             * Read tile data.
             */

            for (var plane = 0; plane < 4; plane++)
            {
                for (var x = 0; x < MapModel.CoordAxisLength; x++)
                {
                    for (var y = 0; y < MapModel.CoordAxisLength; y++)
                    {
                        while (true)
                        {
                            int data = tileBuffer.ReadByte();

                            if (data == 0)
                            {
                                if (plane == 0)
                                    map.VertexHeights[0, x, y] = -method172(932731 + x + baseX, 556238 + y + baseY)*
                                                                 8;
                                else
                                    map.VertexHeights[plane, x, y] = map.VertexHeights[plane - 1, x, y] - 240;
                                break;
                            }
                            if (data == 1)
                            {
                                int height = tileBuffer.ReadByte();

                                if (height == 1)
                                    height = 0;

                                if (plane == 0)
                                    map.VertexHeights[0, x, y] = -height*8;
                                else
                                    map.VertexHeights[plane, x, y] = map.VertexHeights[plane - 1, x, y] - height*8;
                                break;
                            }
                            if (data <= 49)
                            {
                                map.OverlayFloorIds[plane, x, y] = tileBuffer.ReadByte();
                                map.OverlayClippingPaths[plane, x, y] = (byte) ((data - 2)/4);
                                //  overlayRotations[plane,x,y] = (byte)(data - 2 + shapeOffset & 0x3);
                            }
                            else if (data <= 81)
                                map.RenderRuleFlags[plane, x, y] = (byte) (data - 49);
                            // 0x1 = clipped, 0x2 = bridge, 0x4 = roof, 0x8 = ??
                            else
                                map.UnderlayFloorIds[plane, x, y] = (byte) (data - 81);
                        }
                    }
                }
            }

            /*
             * Read map object data.
             */

            var objBuffer = new DataBuffer(_archive.ExtractFile(index.ObjectMapIndex));

            var id = -1;

            do
            {
                var idOffset = objBuffer.ReadSmart422();

                if (idOffset == 0)
                    break;

                id += idOffset;
                var data = 0;

                do
                {
                    var dataOffset = objBuffer.ReadSmart422();

                    if (dataOffset == 0)
                        break;

                    data += dataOffset - 1;
                    int offsetY = data & 0x3f;
                    int offsetX = data >> 6 & 0x3f;
                    int plane = data >> 12;
                    int meta = objBuffer.ReadByte();
                    int type = meta >> 2;
                    int rotation = meta & 0x3;
                    int globalX = offsetX + baseX;
                    int globalY = offsetY + baseY;
                    map.Objects.Add(new MapObjectModel(id, offsetX, offsetY, rotation, type, plane, globalX, globalY));
                } while (true);
            } while (true);
            ValueLookup.Add(index, map);

            return map;
        }

        private static int method172(int i, int j)
        {
            var k = method176(i + 45365, j + 0x16713, 4) - 128 + (method176(i + 10294, j + 37821, 2) - 128 >> 1) +
                    (method176(i, j, 1) - 128 >> 2);
            k = (int) (k*0.29999999999999999D) + 35;
            if (k < 10)
                k = 10;
            else if (k > 60)
                k = 60;
            return k;
        }

        private static int method176(int i, int j, int k)
        {
            var l = i/k;
            var i1 = i & k - 1;
            var j1 = j/k;
            var k1 = j & k - 1;
            var l1 = method186(l, j1);
            var i2 = method186(l + 1, j1);
            var j2 = method186(l, j1 + 1);
            var k2 = method186(l + 1, j1 + 1);
            var l2 = method184(l1, i2, i1, k);
            var i3 = method184(j2, k2, i1, k);
            return method184(l2, i3, k1, k);
        }


        private static int method184(int i, int j, int k, int l)
        {
            var i1 = 0x10000 - AnIntArray1471[k*1024/l] >> 1;
            return (i*(0x10000 - i1) >> 16) + (j*i1 >> 16);
        }

        private static int method186(int i, int j)
        {
            var k = method170(i - 1, j - 1) + method170(i + 1, j - 1) + method170(i - 1, j + 1) +
                    method170(i + 1, j + 1);
            var l = method170(i - 1, j) + method170(i + 1, j) + method170(i, j - 1) + method170(i, j + 1);
            var i1 = method170(i, j);
            return k/16 + l/8 + i1/4;
        }

        private static int method170(int i, int j)
        {
            var k = i + j*57;
            k = k << 13 ^ k;
            var l = k*(k*k*15731 + 0xc0ae5) + 0x5208dd0d & 0x7fffffff;
            return l >> 19 & 0xff;
        }

        protected override void InternalLoad()
        {
            // ignored, maps are lazy initialized
        }
    }
}