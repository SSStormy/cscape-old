﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CScape.Game.Objects;
using CScape.IO;

namespace CScape.Network.Packet
{
    public class WalkingPacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            if (opcode == (byte) InPacketType.MapWalk)
                length -= 14; // get rid of the 14 trailing empty bytes

            var stepAmount = (length - 5)/2;

            if (stepAmount > PositionController.MaxTiles)
                return PacketResult.IncompleteRead;

            var wayX = new List<int> {0};
            var wayY = new List<int> {0};

            var firstX = packetStream.ReadUnsignedWord() - context.Player.Position.RegionX*8;
            for (int i = 0; i < stepAmount; i++)
            {
                wayX.Add(packetStream.ReadSignedByte());
                wayY.Add(packetStream.ReadSignedByte());
            }

            var firstY = packetStream.ReadUnsignedWord() - context.Player.Position.RegionY * 8;
            var running = packetStream.ReadSignedByte() == 1;

            ++stepAmount;
            for (int i = 0; i < stepAmount; i++)
            {
                wayX[i] += firstX;
                wayY[i] += firstY;
            }

            await context.Player.Position.SubmitMovementRequest(wayX, wayY);

            return opcode == (byte) InPacketType.MapWalk ? 
                PacketResult.IncompleteRead : 
                PacketResult.AllOk;
        }
    }
}
