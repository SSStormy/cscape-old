﻿using System.Collections.Generic;

namespace CScapeCache.Model
{
    public class MapObjectModel
    {
        public int OffsetX { get; }
        public int OffsetY { get; }
        public int Plane { get; }
        public int GlobalX { get; set; }
        public int GlobalY { get; set; }
        public int Id { get; }
        public int Type { get; }
        public int Rotation { get; }

        public MapObjectModel(int id, int offsetX, int offsetY, int rotation, int type, int plane,
            int globalX, int globalY)
        {
            OffsetX = offsetX;
            OffsetY = offsetY;
            Rotation = rotation;
            Id = id;
            Type = type;
            Plane = plane;
            GlobalX = globalX;
            GlobalY = globalY;
        }
    }

    public class MapModel
    {
        public MapIndex Index { get; set; }
        public int BaseX { get; set; }
        public int BaseY { get; set; }

        public const int MaxPlanes = 4;
        public const int CoordAxisLength = 64;

        public int[,,] VertexHeights { get; } = new int[MaxPlanes, CoordAxisLength, CoordAxisLength];
        public int[,,] OverlayFloorIds { get; } = new int[MaxPlanes, CoordAxisLength, CoordAxisLength];
        public int[,,] OverlayClippingPaths { get; } = new int[MaxPlanes, CoordAxisLength, CoordAxisLength];
        public int[,,] RenderRuleFlags { get; } = new int[MaxPlanes, CoordAxisLength, CoordAxisLength];
        public int[,,] UnderlayFloorIds { get; } = new int[MaxPlanes, CoordAxisLength, CoordAxisLength];

        public List<MapObjectModel> Objects { get; } = new List<MapObjectModel>();

        internal MapModel(MapIndex index, int baseX, int baseY)
        {
            Index = index;
            BaseX = baseX;
            BaseY = baseY;
        }
    }
}
