﻿using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Server;
using Imms;

namespace CScape.Game.Interface
{
    public interface IShowableInterface
    {
        int InterfaceId { get; }
    }

    public interface ICloseableInterface
    {
        int CloseButtonId { get; }
    }

    public interface ICloseCallbackOverride
    {
        Task CloseCallbackOverride(Player player);
    }

    public interface ICloseCallback
    {
        Task CloseCallback(Player player);
    }

    public interface IQueueableDialogInterface : IShowableInterface, ICloseableInterface, IChatInterface
    {
        ITextConfig TextConfig { get; }
    }

    public interface IChatInterface
    {
        
    }

    public interface ITextConfig
    {
        ImmMap<int, string> ChildMessages { get; }
    }

    public interface IShowMainAndTabInterfaceConfig : IShowableInterface
    {
        int TabInterfaceId { get; }
    }

    public interface IShowSidebarInterfaceConfig : IShowableInterface
    {
        int TabIndex { get; }
    }

    public sealed class GenericSidebarConfig : IShowSidebarInterfaceConfig
    {
        public GenericSidebarConfig(int index, int iid)
        {
            InterfaceId = iid;
            TabIndex = index;
        }

        public int InterfaceId { get; }
        public int TabIndex { get; }
    }

    public sealed class GenericShowableConfig : IShowableInterface
    {
        public GenericShowableConfig(int interfaceId)
        {
            InterfaceId = interfaceId;
        }

        public int InterfaceId { get; }
    }

    public sealed class GenericClosableConfig : IShowableInterface, ICloseableInterface
    {
        public GenericClosableConfig(int closeButtonId, int interfaceId)
        {
            CloseButtonId = closeButtonId;
            InterfaceId = interfaceId;
        }

        public int InterfaceId { get; }
        public int CloseButtonId { get; }
    }

    public sealed class GenericDialogConfig : IQueueableDialogInterface
    {
        public int InterfaceId { get; }
        public int CloseButtonId { get; }
        public ITextConfig TextConfig { get; }

        public GenericDialogConfig(int interfaceId, int closeButtonId, ITextConfig textConfig)
        {
            InterfaceId = interfaceId;
            CloseButtonId = closeButtonId;
            TextConfig = textConfig;
        }
    }

    public sealed class CharacterDesignConfig : IShowableInterface, ICloseableInterface
    {
        public int InterfaceId { get; }
        public int CloseButtonId { get; }

        public CharacterDesignConfig(ScapeServer server)
        {
            InterfaceId = server.Interfaces.InterfaceIds.CharacterDesign;
            CloseButtonId = server.Interfaces.ButtonIds.FinishCharacterDesign;
        }
    }
}