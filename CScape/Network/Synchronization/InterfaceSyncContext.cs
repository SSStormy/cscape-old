using System;
using System.Diagnostics;
using CScape.Game.Container;
using CScape.Game.Interface;
using CScape.Network.Synchronization.Messages;
using Imms;

namespace CScape.Network.Synchronization
{
    public class InterfaceSyncContext
    {
        public IInterface Interface { get; }
        public IInterfaceCanOverrideStrategy OverrideStrategy { get; }

        private IContainer _container;

        public IContainer Container
        {
            get { return _container; }
            set
            {
                if (value == null)
                {
                    _container.SyncClear -= OnContainerClear;
                    _container.SyncDirtyAtIndex -= OnContainerSetDirty;

                    _container = null;
                    PushMessage(new ClearContainerMessage(_container));
                    return;
                }
                _container = value;

                _container.SyncClear += OnContainerClear;
                _container.SyncDirtyAtIndex += OnContainerSetDirty;

                SyncContainer();
            }
        }

        private void OnContainerClear(object sender, EventArgs e)
        {
            Debug.Assert(sender == _container);
            PushMessage(new ClearContainerMessage(Container));
        }

        private void OnContainerSetDirty(object sender, int index)
        {
            Debug.Assert(sender == _container);
            PushMessage(new UpdateContainerItemsMessage(_container, index));
        }

        public bool NeedsSync => !_queue.IsEmpty;

        private readonly object _msgQueueLock = new object();
        private ImmList<ISynchronizationMessage> _queue = ImmList<ISynchronizationMessage>.Empty;
        private bool _closeFlag;

        public InterfaceSyncContext(IInterface interf, IInterfaceCanOverrideStrategy overrideStrategy = null)
        {
            Interface = interf;
            OverrideStrategy = overrideStrategy;
        }

        protected void PushMessage(ISynchronizationMessage msg)
        {
            lock (_msgQueueLock)
                _queue = _queue.AddLast(msg);
        }

        private InterfaceSyncData GetData()
        {
            lock (_msgQueueLock)
            {
                var msgs = _queue;
                var close = _closeFlag;

                _closeFlag = false;
                _queue = ImmList<ISynchronizationMessage>.Empty;

                return new InterfaceSyncData(msgs, close);
            }
        }

        public void SyncContainer()
        {
            if(_container != null)
                PushMessage(new FullContainerUpdateMessage(_container));
        }

        public InterfaceSyncData GetInitialSync()
        {
            PushMessage(Interface.OpenMessageBuilder.BuildMessage());
            SyncContainer();

            return GetData();
        }

        public InterfaceSyncData GetSync()
            => GetData();
    }
}