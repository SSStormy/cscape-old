﻿using System;
using System.Collections.Generic;
using CScapeCache.Core;
using CScapeCache.Model;

namespace CScapeCache.Caches
{
    public class GenericConfigCache<T> : AbstractCache
    {
        protected  DataBuffer DataFile { get; }
        protected  DataBuffer IndexFile { get; }
        protected  ICacheFactory<T, GenericConfigCache<T>> Factory { get; }

        protected T[] InternalCache { get; set; }
        
        public CacheReader Cache { get; }
        public int TotalObjects { get; protected set; }
        public IEnumerable<T> All => Array.AsReadOnly(InternalCache);

        internal GenericConfigCache(CacheReader cache, DataBuffer dataFile, DataBuffer indexFile, ICacheFactory<T, GenericConfigCache<T>> factory) : base(cache)
        {
            if (dataFile == null) throw new ArgumentNullException(nameof(dataFile));
            if (factory == null) throw new ArgumentNullException(nameof(factory));

            DataFile = dataFile;
            IndexFile = indexFile;
            Factory = factory;
            Cache = cache;
        }

        public T GetId(int id)
        {
            if (id < 0 || id >= TotalObjects)
                throw new ArgumentOutOfRangeException(nameof(id));

            ThrowIfNotLoaded();
            return InternalCache[id];
        }

        protected override void InternalLoad()
        {
            TotalObjects = IndexFile.ReadShort();

            InternalCache = new T[TotalObjects];

            var i = 2;
            for (var j = 0; j < TotalObjects; j++)
            {
                DataFile.Location = i;
                InternalCache[j] = Factory.Read(this, i, DataFile);
                i += IndexFile.ReadShort();
            }
        }
    }

    public interface ICacheFactory<out T, in TParent>
    {
        T Read(TParent parent, int id, DataBuffer dataFile);
    }


    public sealed class ItemModelFactory : ICacheFactory<ItemModel, GenericConfigCache<ItemModel>>
    {
        public ItemModel Read(GenericConfigCache<ItemModel> parent, int id, DataBuffer dataFile)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            if (dataFile == null) throw new ArgumentNullException(nameof(dataFile));

            var item = new ItemModel(parent, id);
            item.ReadItem(dataFile);
            return item;
        }
    }

    public sealed class ObjectModelFactory : ICacheFactory<ObjectModel, GenericConfigCache<ObjectModel>>
    {
        public ObjectModel Read(GenericConfigCache<ObjectModel> parent, int id, DataBuffer dataFile)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            if (dataFile == null) throw new ArgumentNullException(nameof(dataFile));

            var obj = new ObjectModel(parent, id);
            obj.ReadObject(dataFile);
            return obj;
        }
    }

    public sealed class FloModelFactory : ICacheFactory<FloModel, GenericConfigCache<FloModel>>
    {
        public FloModel Read(GenericConfigCache<FloModel> parent, int id, DataBuffer dataFile)
        {
            if (parent == null) throw new ArgumentNullException(nameof(parent));
            if (dataFile == null) throw new ArgumentNullException(nameof(dataFile));

            var flo = new FloModel(parent, id);
            flo.ReadItem(dataFile);
            return flo;
        }
    }
}
