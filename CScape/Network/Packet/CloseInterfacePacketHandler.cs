﻿using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public class CloseInterfacePacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            await context.Player.Interface.ForceCloseAllNonSideInterfaces();
            return PacketResult.AllOk;
        }
    }
}
