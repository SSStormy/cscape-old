﻿using System;
using CScape.Network.Synchronization;

namespace CScape.Game.Objects
{
    public interface IEntity : IEquatable<IEntity>
    {
        PositionController Position { get; }
        int Id { get; }
        ISyncFlagFacade Sync { get; }
    }
}
