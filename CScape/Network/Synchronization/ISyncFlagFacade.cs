namespace CScape.Network.Synchronization
{
    public interface ISyncFlagFacade
    {
        void FacingTile();
        void ResetAllFlags();
    }
}