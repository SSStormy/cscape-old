﻿REQUIRE = { "player", "args" }
PARAMS =
{
    iid = arg.new(valType.string, argType.required)
}

function command (player, args)
	if args.iid == "clr" and player.interface.mainInterface ~= nil then
        player.interface.mainInterface.close()
	else
		player.interface.showMain (genericShowableConfig.__new(tonumber(args.iid)))
	end
end
