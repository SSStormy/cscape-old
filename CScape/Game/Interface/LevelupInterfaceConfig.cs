﻿using System;
using System.Diagnostics;
using CScape.Game.Objects.Player;
using CScape.Server;
using Imms;

namespace CScape.Game.Interface
{
    public static class LevelUpInterfaceFactory
    {
        private sealed class LevelupTextConfig : ITextConfig
        {
            public ImmMap<int, string> ChildMessages { get; } = ImmMap<int, string>.Empty();

            public LevelupTextConfig(int msg1, int msg2, SkillType skill, int newLvl)
            {
                ChildMessages = ChildMessages.Add(msg1, $"Congratulations! You've just advanced a {skill} level!").
                    Add(msg2, $"You have now reached level {newLvl}!");
            }
        }

        public static IQueueableDialogInterface Create(InterfaceIdConfig repo, SkillType skill, int newLevel)
        {
            try
            {
                var si = repo.InterfaceIds.SkillLvlUpLookup[(int) skill];

                return new GenericDialogConfig(si,
                    repo.ButtonIds.LevelUpContinueLookup[si],
                    new LevelupTextConfig(repo.TextIds.LineOneLookup[si], repo.TextIds.LineTwoLookup[si], skill,
                        newLevel));
            }
            catch (Exception ex)
            {
                Debug.Fail(ex.ToString());
            }
            return null;
        }
    }
}

