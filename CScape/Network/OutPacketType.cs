﻿namespace CScape.Network
{
    // Server -> Client
    public enum OutPacketType : byte
    {
        FlashTab = 24,
        UpdateItems = 34,
        SendItemChunk = 53,
        SendSidebarInterface = 71,
        RegionInit = 73,
        PlayerUpdate = 81,
        SendMainInterface = 97,
        ForceShowTab = 106,
        LogoutOrder = 109,
        SetInterfaceText = 126,
        SendChatInterface = 164,
        SendWalkableInterface = 208,
        ClearInterfaces = 219,
        SendDualMainInterfaceWithInventoryOverlay = 248,
        InitializePlayer = 249,
        SendServerMessage = 253,
        PlayerOptions =104,
        SkillData = 134,
        NpcUpdate = 65,
        ResetFlag = 78,
        DebugInfo = 2
    }
}
