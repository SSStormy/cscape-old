﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class PacketHandlers
    {
        private readonly Dictionary<InPacketType, IPacketHandler> _handlers;

        public PacketHandlers()
        {
            var walking = new WalkingPacketHandler();
            var objHandler = new ObjectActionPacketHandler();
            var bttnHandler = new ButtonPressedPacketHandler();
            var itemOpt = new ItemOptionPacketHandler();
            var npcOpt = new NpcOptionPacketHandler();

            _handlers = new Dictionary<InPacketType, IPacketHandler>
            {
                {InPacketType.ChatCommand, new CommandPacketHandler()},
                {InPacketType.MouseClick, new MouseClickPacketHandler()},
                {InPacketType.MoveItem, new MoveItemPacketHandler()},
                {InPacketType.ChatMessage, new ChatMessagePacketHandler()},
                {InPacketType.CloseInterface, new CloseInterfacePacketHandler()},
                {InPacketType.WalkingRequest1, walking},
                {InPacketType.WalkingRequest2, walking},
                {InPacketType.MapWalk, walking},
                {InPacketType.ButtonPressed, bttnHandler},
                {InPacketType.ObjectAction1, objHandler},
                {InPacketType.ObjectAction2, objHandler},
                {InPacketType.ObjectAction3, objHandler},
                {InPacketType.DialogContinue, bttnHandler},
                {InPacketType.ItemOption1, itemOpt},
                {InPacketType.ItemOption2, itemOpt},
                {InPacketType.ItemOption3, itemOpt},
                {InPacketType.WearItem, itemOpt},
                {InPacketType.UnequipItem, itemOpt},
                {InPacketType.NpcAttack, npcOpt},
                {InPacketType.NpcOption2, npcOpt},
                {InPacketType.NpcOption3, npcOpt},
                {InPacketType.NpcOption4, npcOpt},
                {InPacketType.CharacterDesignData, new CharacterDesignDataPacketHandler() },
                {InPacketType.FollowRequest, new FollowReqPacketHandler() },
            };
        }

        public async Task Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var enumop = (InPacketType) opcode;
            var expectedSizePostHandle = packetStream.Position + length;

            context.Logs.Debug(this, $">> op: {enumop}: len: {length}", conid: context.ConnectionId);

            if (!_handlers.ContainsKey(enumop))
            {
                packetStream.Position = expectedSizePostHandle;
                return;
            }

            var result = await _handlers[enumop].Handle(opcode, length, packetStream, context);
            switch (result)
            {
                case PacketResult.IncompleteRead:
                    packetStream.Position = expectedSizePostHandle;
                    return;
                case PacketResult.KillConnection:
                    context.Dispose();
                    return;
            }
        }
    }
}