﻿using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    public class TickActionTickHandler : IServerTickHandler
    {
        public bool ShouldFire => Server.Tick.NeedsHandling();

        public void TickOccured()
        {
        }

        public ScapeServer Server { get; }

        public TickActionTickHandler(ScapeServer server)
        {
            Server = server;
        }

        public async Task Process()
        {
            await Server.Tick.HandleAll();
        }
    }
}
