using System.Collections.Generic;
using CScape.Network.Synchronization.Messages;

namespace CScape.Game.Interface
{
    public interface IInterfaceCanOverrideStrategy
    {
        /// <summary>
        /// Called when another interface wants to override this interface.
        /// </summary>
        /// <returns>
        /// True, if the overrider can override this interface and this interface has cleaned itself up. Backlog will be set to an IEnumerable of messages to be sent before first syncing the overrider.
        /// False, if the overrider cannot override this interface. Backlog is null.
        /// </returns>
        bool CanOverride(IInterface parent, IInterface overrider, out IEnumerable<ISynchronizationMessage> backlog);
    }
}