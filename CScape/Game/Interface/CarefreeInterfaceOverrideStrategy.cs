using System.Collections.Generic;
using CScape.Network.Synchronization.Messages;

namespace CScape.Game.Interface
{
    public class CarefreeInterfaceOverrideStrategy : IInterfaceCanOverrideStrategy
    {
        public bool CanOverride(IInterface parent, IInterface overrider, out IEnumerable<ISynchronizationMessage> backlog)
        {
            backlog = new[] { parent.CloseMessageBuilder.BuildMessage() };
            return true;
        }
    }
}