﻿using CScape.IO;
using CScape.Server;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class UpdateServerStatsMessage : ISynchronizationMessage
    {
        private readonly ServerStats.StatCollection _stats;

        public UpdateServerStatsMessage(ServerStats.StatCollection stats)
        {
            _stats = stats;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var packet = new PacketBuilder(receiver.Player, 32);
            packet.OpcodeAndSizePlaceholder(OutPacketType.DebugInfo);
            packet.WriteByte(1);
            packet.WriteDWord(_stats.PacketsSent);
            packet.WriteDWord(_stats.BytesSent);
            packet.WriteDWord(_stats.BytesReceived);
            packet.WriteDWord(_stats.PacketsReceived);
            packet.WriteDWord(_stats.TickProcessTime);
            packet.WriteDWord(_stats.PacketBuildersAllocated);
            packet.WriteSize();
            return packet;
        }
    }

    public sealed class HideServerStatsMessage : ISynchronizationMessage
    {
        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var packet = new PacketBuilder(receiver.Player, 8);
            packet.OpcodeAndSizePlaceholder(OutPacketType.DebugInfo);
            packet.WriteByte(0);
            packet.WriteSize();
            return packet;
        }
    }
}
