using System.Collections.Generic;
using System.Threading.Tasks;
using CScape.Game.Objects;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.Network.Synchronization.Messages;
using CScape.Network.Synchronization.Segments;

namespace CScape.Network.Synchronization.Machine
{
    public sealed class PlayerSyncMachine : AbstractSynchronizationMachine
    {
        public PlayerSyncMachine(Player player) : base(player)
        {
        }

        private void AddSegment(IEntityUpdateSegment segment, IList<IUpdateSegment> list,ref int neededUpdates)
        {
            if (segment.FlagWriter.NeedsWriting)
                neededUpdates++;
            list.Add(segment);
        }

        public override async Task Synchronize()
        {
            var neededUpdates = 0;

            var known = Player.UpdatePlayers;
            var oldCount = known.Count;
            var dist = Player.ViewingDistance;
            var otherSegs = new List<IUpdateSegment>(Globals.MaxUpdatePlayers);

            var cur = 0;
            while (cur < known.Count)
            {
                var local = known[cur];

                if (local.Position.DidJustTeleport ||
                    !local.IsPlaying ||
                    !local.IsInLocalRange(Player) ||
                    (local.Position.GetMaxDelta(Player.Position) > dist))
                {
                    otherSegs.Add(new EntityRemoveSegment());
                    known.RemoveAt(cur);
                }
                else
                {
                    AddSegment(new MovementSegment(local, new PlayerSyncFlagWriter(local)), otherSegs, ref neededUpdates);
                    cur++;
                }
            }

            var newPlayers = 0;
            const int maxPlayersPerSync = 20;

            foreach (var local in Player.Position.GetLocalClusterPlayers())
            {
                if (Player.UpdatePlayers.Count > Globals.MaxUpdatePlayers)
                {
                    Player.HasExcessivePlayers = true;
                    break;
                }
                if (newPlayers >= maxPlayersPerSync)
                    break;

                if (local.Equals(Player))
                    continue;

                if (!local.Position.IsWithinDistance(Player.Position, dist))
                    continue;

                if (Player.UpdatePlayers.Contains(local))
                    continue;

                // player was cleaned after we've retrieved it as a local cluster player.
                if (!local.IsPlaying)
                {
                    // just in case.
                    local.Position.LeaveAllMaps();
                    continue;
                }

                newPlayers++;

                AddSegment(new AddPlayerSegment(local, Player), otherSegs, ref neededUpdates);
                known.Add(local);
            }

            await Player.Connection.SendMessage(new UpdatePlayerMessage(new MovementSegment(Player, new PlayerSyncFlagWriter(Player)), otherSegs, oldCount, neededUpdates));
        }
    }
}