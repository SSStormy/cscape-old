﻿using System.Collections.Generic;

namespace CScape.Game.Interface
{
    public class InterfaceId
    {
        public int SwordAttack { get; set; }
        public int SkillMenu { get; set; }
        public int QuestMenu { get; set; }
        public int PrayerMenu { get; set; }
        public int PlayerBackpack { get; set; }
        public int Equipment { get; set; }
        public int SpellbookStandard { get; set; }
        public int FriendsList { get; set; }
        public int IgnoresList { get; set; }
        public int LogoutTab { get; set; }
        public int OptionsLowDetail { get; set; }
        public int OptionsHighDetail { get; set; }
        public int PlayerControls { get; set; }
        public int Book { get; set; }
        public int Shop { get; set; }
        public int CharacterDesign { get; set; }

        public Dictionary<int, int> SkillLvlUpLookup { get; set; }
    }
}
