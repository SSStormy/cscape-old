﻿using System;
using System.Threading.Tasks;
using CScape.Game.Interface;
using CScape.Network;
using Imms;
using Newtonsoft.Json;

namespace CScape.Game.Objects.Player
{
    public enum SkillType
    {
        Attack = 0,
        Defence = 1,
        Strength = 2,
        Hitpoints = 3,
        Ranged = 4,
        Prayer = 5,
        Magic = 6,
        Cooking = 7,
        Woodcutting = 8,
        Fletching = 9,
        Fishing = 10,
        Firemaking = 11,
        Crafting = 12,
        Smithing = 13,
        Mining = 14,
        Herblore = 15,
        Agility = 16,
        Thieving = 17,
        Slayer = 18,
        Farming = 19,
        Runecrafting = 20
    }

    [JsonObject(MemberSerialization.OptIn)]
    public sealed class PlayerSkills
    {
        private static int? _maxSkills = null;

        public static int MaxSkills
        {
            get
            {
                if (_maxSkills == null)
                    _maxSkills = Enum.GetValues(typeof(SkillType)).Length;
                return _maxSkills.Value;
            }
        }

        public SkillData Defence => SkillList[(int)SkillType.Defence];
        public SkillData Attack => SkillList[(int)SkillType.Attack];
        public SkillData Strength => SkillList[(int)SkillType.Strength];
        public SkillData Hitpoints => SkillList[(int)SkillType.Hitpoints];
        public SkillData Ranged => SkillList[(int)SkillType.Ranged];
        public SkillData Prayer => SkillList[(int)SkillType.Prayer];
        public SkillData Magic => SkillList[(int)SkillType.Magic];
        public SkillData Cooking => SkillList[(int)SkillType.Cooking];
        public SkillData Woodcutting => SkillList[(int)SkillType.Woodcutting];
        public SkillData Fletching => SkillList[(int)SkillType.Fletching];
        public SkillData Fishing => SkillList[(int)SkillType.Fishing];
        public SkillData Firemaking => SkillList[(int)SkillType.Firemaking];
        public SkillData Crafting => SkillList[(int)SkillType.Crafting];
        public SkillData Smithing => SkillList[(int)SkillType.Smithing];
        public SkillData Mining => SkillList[(int)SkillType.Mining];
        public SkillData Herblore => SkillList[(int)SkillType.Herblore];
        public SkillData Agility => SkillList[(int)SkillType.Agility];
        public SkillData Thieving => SkillList[(int)SkillType.Thieving];
        public SkillData Slayer => SkillList[(int)SkillType.Slayer];
        public SkillData Farming => SkillList[(int)SkillType.Farming];
        public SkillData Runecrafting => SkillList[(int)SkillType.Runecrafting];

        public Player Player
        {
            get { return _player; }
            set
            {
                if (_player != null)
                {
                    _player.Server.Logs.Warning(this, $"Tried to set player when it's not null on SKills. {_player.Username}", conid: _player.Connection.ConnectionId);
                    return;
                }

                _player = value;
                foreach (var skill in SkillList)
                    skill.Player = Player;
            }
        }

        public ImmList<SkillData> SkillList { get; }

        [JsonProperty("SkillList")] // cba to write a ImmList converter
        private SkillData[] _skills => SkillList.ToArray();

        public SkillData this[int i]
        {
            get
            {
                if (i < 0 || i >= MaxSkills)
                    return null;

                return SkillList[i];
            }
        }

        private Player _player;

        [JsonConstructor]
        public PlayerSkills(SkillData[] skillList)
        {
            SkillList = ImmList<SkillData>.Empty.AddFirstRange(skillList);
        }

        public PlayerSkills(Player player)
        {
            _player = player;
            SkillList = ImmList<SkillData>.Empty;
            for (var i = 0; i < MaxSkills; i++)
                SkillList = SkillList.AddLast(new SkillData(player, (SkillType) i));
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class SkillData
    {
        [JsonProperty]
        public float Experience { get; set; }

        /// <summary>
        /// Returns the level of the skill based off of the experience points.
        /// </summary>
        /// 
        public int ExpLevel
        {
            get
            {
                if (Experience >= 13034431) return 99;
                if (Experience >= 11805606) return 98;
                if (Experience >= 10692629) return 97;
                if (Experience >= 9684577) return 96;
                if (Experience >= 8771558) return 95;
                if (Experience >= 7944614) return 94;
                if (Experience >= 7195629) return 93;
                if (Experience >= 6517253) return 92;
                if (Experience >= 5902831) return 91;
                if (Experience >= 5346332) return 90;
                if (Experience >= 4842295) return 89;
                if (Experience >= 4385776) return 88;
                if (Experience >= 3972294) return 87;
                if (Experience >= 3597792) return 86;
                if (Experience >= 3258594) return 85;
                if (Experience >= 2951373) return 84;
                if (Experience >= 2673114) return 83;
                if (Experience >= 2421087) return 82;
                if (Experience >= 2192818) return 81;
                if (Experience >= 1986068) return 80;
                if (Experience >= 1798808) return 79;
                if (Experience >= 1629200) return 78;
                if (Experience >= 1475581) return 77;
                if (Experience >= 1336443) return 76;
                if (Experience >= 1210421) return 75;
                if (Experience >= 1096278) return 74;
                if (Experience >= 992895) return 73;
                if (Experience >= 899257) return 72;
                if (Experience >= 814445) return 71;
                if (Experience >= 737627) return 70;
                if (Experience >= 668051) return 69;
                if (Experience >= 605032) return 68;
                if (Experience >= 547953) return 67;
                if (Experience >= 496254) return 66;
                if (Experience >= 449428) return 65;
                if (Experience >= 407015) return 64;
                if (Experience >= 368599) return 63;
                if (Experience >= 333804) return 62;
                if (Experience >= 302288) return 61;
                if (Experience >= 273742) return 60;
                if (Experience >= 247886) return 59;
                if (Experience >= 224466) return 58;
                if (Experience >= 203254) return 57;
                if (Experience >= 184040) return 56;
                if (Experience >= 166636) return 55;
                if (Experience >= 150872) return 54;
                if (Experience >= 136594) return 53;
                if (Experience >= 123660) return 52;
                if (Experience >= 111945) return 51;
                if (Experience >= 101333) return 50;
                if (Experience >= 91721) return 49;
                if (Experience >= 83014) return 48;
                if (Experience >= 75127) return 47;
                if (Experience >= 67983) return 46;
                if (Experience >= 61512) return 45;
                if (Experience >= 55649) return 44;
                if (Experience >= 50339) return 43;
                if (Experience >= 45529) return 42;
                if (Experience >= 41171) return 41;
                if (Experience >= 37224) return 40;
                if (Experience >= 33648) return 39;
                if (Experience >= 30408) return 38;
                if (Experience >= 27473) return 37;
                if (Experience >= 24815) return 36;
                if (Experience >= 22406) return 35;
                if (Experience >= 20224) return 34;
                if (Experience >= 18247) return 33;
                if (Experience >= 16456) return 32;
                if (Experience >= 14833) return 31;
                if (Experience >= 13363) return 30;
                if (Experience >= 12031) return 29;
                if (Experience >= 10824) return 28;
                if (Experience >= 9730) return 27;
                if (Experience >= 8740) return 26;
                if (Experience >= 7842) return 25;
                if (Experience >= 7028) return 24;
                if (Experience >= 6291) return 23;
                if (Experience >= 5624) return 22;
                if (Experience >= 5018) return 21;
                if (Experience >= 4470) return 20;
                if (Experience >= 3973) return 19;
                if (Experience >= 3523) return 18;
                if (Experience >= 3115) return 17;
                if (Experience >= 2746) return 16;
                if (Experience >= 2411) return 15;
                if (Experience >= 2107) return 14;
                if (Experience >= 1833) return 13;
                if (Experience >= 1584) return 12;
                if (Experience >= 1358) return 11;
                if (Experience >= 1154) return 10;
                if (Experience >= 969) return 9;
                if (Experience >= 801) return 8;
                if (Experience >= 650) return 7;
                if (Experience >= 512) return 6;
                if (Experience >= 388) return 5;
                if (Experience >= 276) return 4;
                if (Experience >= 174) return 3;
                if (Experience >= 83) return 2;
                if (Experience >= 0) return 1;
                return 0;
            }
        }

        public IExpGainModifier Modifier { get; set; }
        public int LevelModifier { get; set; }

        public int AbsoluteLevel => ExpLevel + LevelModifier;

        public Player Player { get; set; }

        [JsonProperty]
        public SkillType Skill { get; }

        [JsonConstructor]
        public SkillData(float experience, SkillType skill)
        {
            Experience = experience;
            Skill = skill;
        }

        // default
        public SkillData(Player player, SkillType skillIndex)
        {
            Skill = skillIndex;
            Player = player;
        }

        public async Task GainExp(float exp, IExpGainModifier overrideModifier = null)
        {
            float gain;
            if (overrideModifier != null)
                gain = overrideModifier.Modify(exp, Player);
            else if (Modifier != null)
                gain = Modifier.Modify(exp, Player);
            else if (Player.Server.Config.GlobalExpModifier != null)
                gain = Player.Server.Config.GlobalExpModifier.Modify(exp, Player);
            else
                gain = exp;

            var prevLvl = ExpLevel;
            Experience += gain;

            await SendSkillData();

            if (ExpLevel != prevLvl)
                await SendLevelUpInterface(ExpLevel);
        }

        public async Task SendSkillData()
        {
            await Player.Connection.Send(OutPacketType.SkillData, b =>
            {
                b.WriteByte((int) Skill)
                    .WriteDWord((int)Experience)
                    .WriteByte(AbsoluteLevel);
            });
        }

        public async Task SendLevelUpInterface(int newLvl)
        {
            var cfg = LevelUpInterfaceFactory.Create(Player.Server.Interfaces, Skill, newLvl);

            if (Player.Interface.MainInterface == null)
                await Player.Interface.QueueDialog(cfg);

            Player.SendMessage($"Congratulations! You've just advanced a {Skill} level!");
        }
    }

    public interface IExpGainModifier
    {
        float Modify(float originalExpGain, Player receiver);
    }

    public class MultipliedExpGainModifier : IExpGainModifier
    {
        public float Multiplier { get; }

        public MultipliedExpGainModifier(float multiplier)
        {
            Multiplier = multiplier;
        }

        public float Modify(float originalExpGain, Player receiver)
            => originalExpGain*Multiplier;
    }
}
