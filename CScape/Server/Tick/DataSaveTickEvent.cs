﻿using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    public sealed class DataSaveTickEvent : AbstractTimedTickEvent
    {
        public override int TickInterval => 20;

        public DataSaveTickEvent(ScapeServer server) : base(server)
        {
        }
        
        public override Task Process()
        {
            Server.Pwd.Save();
            Server.PlayerIo.SaveAllPlayers();
            return Task.CompletedTask;
        }
    }
}