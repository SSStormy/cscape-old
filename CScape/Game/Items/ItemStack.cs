﻿using System;
using Newtonsoft.Json;

namespace CScape.Game.Items
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ItemStack : ICloneable
    {
        private uint _amount;
        public const int MaxStack = int.MaxValue;
        public const int EmptyValue = 0;

        [JsonProperty]
        public int ItemId { get; set; } = EmptyValue;

        [JsonProperty]
        public uint Amount
        {
            get { return _amount; }
            set
            {
                if (value <= 0)
                    SetEmpty();
                else
                    _amount = value;
            }
        }

        public bool IsEmpty
        {
            get
            {
                if (Amount != 0)
                    return ItemId == EmptyValue;

                ItemId = EmptyValue;
                return true;
            }
        }

        [JsonConstructor]
        public ItemStack(int itemId, uint amount)
        {
            ItemId = itemId;
            Amount = amount;
        }

        public static implicit operator ItemStack(int itemId)
            => new ItemStack(itemId, 1);

        public static ItemStack Empty => new ItemStack(EmptyValue, 0);

        /// <summary>
        /// For use in lua scripts.
        /// </summary>
        public static ItemStack New(int itemId, uint amount)
            => new ItemStack(itemId, amount);

        public void SetEmpty()
        {
            ItemId = EmptyValue;
            _amount = 0;
        }

        public object Clone()
            => new ItemStack(ItemId, Amount);
    }
}
