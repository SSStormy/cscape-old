﻿using System;
using CScape.Helper;

namespace CScape.Game.Objects
{
    public static class EntityExtensionMethods
    {
        /// <summary>
        /// Sets and synchronizes the facing tile of the position controller. 
        /// </summary>
        public static void SyncSetFacingTile(this PositionController pos, int x, int y)
        {
            pos.FacingX = x;
            pos.FacingY = y;
            pos.ParentEntity.Sync.FacingTile();
        }

        /// <summary>
        /// Sets and synchronizes the facing direction to the tile in the given direction.
        /// </summary>
        public static void SyncSetFaceTileInDirection(this PositionController pos, MoveDirectionType dir)
        {
            var delta = PositionController.GetDelta(dir);
            pos.Logs.Debug(pos, $"{pos.ParentEntity} sync set facing to [{dir} {delta}]");
            pos.SyncSetFacingTile(pos.GlobalX + delta.Item1, pos.GlobalY + delta.Item2);
        }

        public static bool IsInLocalRange(this IEntity us, IEntity other)
            => IsInLocalRange(us.Position, other.Position);

        public static bool IsInLocalRange(this PositionController us, PositionController check)
            => IsInLocalRange(us, check.GlobalX, check.GlobalY, check.GlobalZ);

        public static bool IsInLocalRange(this PositionController us, int x, int y)
            => IsInLocalRange(us, x, y, us.GlobalZ);

        public static bool IsInLocalRange(this PositionController us, int x, int y, int z)
            => IsWithinDistance(us, x, y, z, Globals.MaxViewingDistance);

        public static bool IsWithinDistance(this PositionController us, PositionController other, int distance)
            => IsWithinDistance(us, other.GlobalX, other.GlobalY, other.GlobalZ, distance);

        public static bool IsWithinDistance(this PositionController us, int x, int y, int distance)
            => IsWithinDistance(us, x, y, us.GlobalZ, distance);

        public static bool IsWithinDistance(this PositionController us, int x, int y, int z,
            int distance)
        {
            return Math.Abs(us.GlobalX - x) <= distance &&
                   Math.Abs(us.GlobalY - y) <= distance &&
                   us.GlobalZ == z;
        }
    }
}