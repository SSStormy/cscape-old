﻿using System;
using System.IO;
using System.IO.Compression;

namespace CScapeCache.Core
{
    public class Archive
    {
        public FileStream DataFile { get; }
        public FileStream IndexFile { get; }
        public int ArchiveIndex { get; }
        private readonly byte[] _gzipBuffer;

        public Archive(FileStream data, FileStream indexes, int archiveIndex)
        {
            DataFile = data;
            IndexFile = indexes;
            ArchiveIndex = archiveIndex;
            _gzipBuffer = new byte[65000];
        }

        public int GetFileCount()
        {
            if (IndexFile == null)
                return 0;
            return (int) (IndexFile.Length/6);
        }

        public int GetFileSize(int fileIndex)
        {
            IndexFile.Seek(fileIndex*6, SeekOrigin.Begin);
            return (IndexFile.ReadByte() << 16) + (IndexFile.ReadByte() << 8) + IndexFile.ReadByte();
        }

        public int GetChunkOffset(int fileIndex)
        {
            IndexFile.Seek(fileIndex*6 + 3, SeekOrigin.Begin);
            return (IndexFile.ReadByte() << 16) + (IndexFile.ReadByte() << 8) + IndexFile.ReadByte();
        }

        public byte[] ExtractFile(int fileIndex)
        {
            if (fileIndex >= GetFileCount())
                return null;

            IndexFile.Seek(fileIndex*6, SeekOrigin.Begin);
            var fileSize = (IndexFile.ReadByte() << 16) + (IndexFile.ReadByte() << 8) + IndexFile.ReadByte();
            var chunkOffset = (IndexFile.ReadByte() << 16) + (IndexFile.ReadByte() << 8) + IndexFile.ReadByte();

            if (chunkOffset < 0 || chunkOffset > DataFile.Length/520L)
                return null;
            var outBuffer = new byte[fileSize];
            var writeOffset = 0;
            for (var chunkIndex = 0; writeOffset < fileSize; chunkIndex++)
            {
                if (chunkOffset == 0)
                    return null;
                var chunkSize = Math.Min(512, fileSize - writeOffset);
                DataFile.Seek(chunkOffset*520L, SeekOrigin.Begin);
                if (DataFile.Position + chunkSize + 8 > DataFile.Length)
                    return null;
                var checksumFileIndex = (DataFile.ReadByte() << 8) + DataFile.ReadByte();
                var checksumChunkIndex = (DataFile.ReadByte() << 8) + DataFile.ReadByte();
                var nextChunkOffset = (DataFile.ReadByte() << 16) + (DataFile.ReadByte() << 8) + DataFile.ReadByte();
                var checksumArchiveIndex = DataFile.ReadByte();
                if (checksumFileIndex != fileIndex || checksumChunkIndex != chunkIndex ||
                    checksumArchiveIndex - 1 != ArchiveIndex)
                    return null;
                if (nextChunkOffset < 0 || nextChunkOffset*520L > DataFile.Length)
                    return null;
                DataFile.Read(outBuffer, writeOffset, chunkSize);
                writeOffset += chunkSize;
                chunkOffset = nextChunkOffset;
            }
            if (ArchiveIndex > 0)
            {
                var i = 0;
                try
                {
                    using (var gzipinputstream = new GZipStream(new MemoryStream(outBuffer), CompressionMode.Decompress))
                    {
                        do
                        {
                            if (i == _gzipBuffer.Length)
                                return null;
                            var k = gzipinputstream.Read(_gzipBuffer, i, _gzipBuffer.Length - i);
                            if (k == 0)
                                break;
                            i += k;
                        } while (true);
                    }
                    outBuffer = new byte[i];
                    Array.Copy(_gzipBuffer, outBuffer, outBuffer.Length);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return outBuffer;
        }
    }
}