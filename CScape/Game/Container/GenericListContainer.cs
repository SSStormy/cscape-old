﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CScape.Game.Items;
using CScape.Server;
using CScape.Server.Repository;

namespace CScape.Game.Container
{
    public class GenericListContainer : GenericContainer, IListContainer
    {
        public IRepository<ItemDef, int> ItemRepo => Server.ItemRepo;

        public override IEnumerator<ItemStack> GetEnumerator()
            => Slots.Select(s => s.Item).GetEnumerator();

        public GenericListContainer(ScapeServer server, int containerId, ContainerSlot[] slots) : base(server, containerId, slots)
        {
        }

        public GenericListContainer(ScapeServer server, int size, int containerId) : base(size, containerId, server)
        {
        }

        public override ContainerChangeInfo CanAddItem(ItemStack item)
        {
            if (item.IsEmpty)
                return ContainerChangeInfo.Invalid;


            var def = ItemRepo.Get(item.ItemId);
            if(!def.Stackable && item.Amount > 1)
                return ContainerChangeInfo.Invalid;

            for (var i = 0; i < Capacity; i++)
            {
                var slotItem = InternalSlots[i].Item;
                if (slotItem.IsEmpty)
                    return new ContainerChangeInfo(true, i);

                if (slotItem.ItemId == item.ItemId)
                {
                    var info = GetAmountStackInfo(def, slotItem, item, i);
                    if (info.IsValidOperation)
                        return info;
                }
            }
                return ContainerChangeInfo.Invalid;
        }

        public override ContainerChangeInfo CanRemoveItem(ItemStack item)
        {
            if (item.IsEmpty)
            return ContainerChangeInfo.Invalid;

            for (var i = 0; i < Capacity; i++)
            {
                var slotItem = InternalSlots[i].Item;

                if (slotItem.IsEmpty)
                    continue;

                if (slotItem.ItemId == item.ItemId)
                {
                    var left = item.Amount - slotItem.Amount;

                    if (left == 0)
                        return new ContainerChangeInfo(true, i);

                    // more items in container then we need to remove
                    if (left > 0)
                        return new ContainerChangeInfo(true, i, true, left);
                    
                    // more items to remove then in container
                    return new ContainerChangeInfo(true, i);
                }

            }
            return ContainerChangeInfo.Invalid;
        }

        private bool HandleChangeInfo(ContainerChangeInfo info, ItemStack item,
            Action<ItemStack> amntChangeFunc, Action<int> setFunc)
        {
            Debug.Assert(item != null);

            if (info.IsValidOperation)
            {
                if (info.ShouldChangeAmount)
                    amntChangeFunc(InternalSlots[info.Index].Item);

                else
                    setFunc(info.Index);

                SyncSlot(info.Index);
                return true;
            }

            return false;
        }

        protected bool InternalAddItem(ItemStack item, ContainerChangeInfo info)
            => HandleChangeInfo(info,
                item, 
                i => i.Amount += item.Amount,
                i =>
                {
                    Items++;
                    InternalSlots[i].Item = item;
                });

        protected bool InternalRemoveItem(ItemStack item, ContainerChangeInfo info)
            => HandleChangeInfo(info,
                item,
                i => i.Amount -= item.Amount,
                i =>
                {
                    Items--;
                    InternalSlots[i].Item.SetEmpty();
                });

        public virtual bool AddItem(ItemStack item)
            => InternalAddItem(item, CanAddItem(item));

        public virtual bool RemoveItem(ItemStack item)
            => InternalRemoveItem(item, CanRemoveItem(item));
    }
}