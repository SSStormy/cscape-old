﻿using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed  class NpcOptionPacketHandler : IPacketHandler
    {
        public Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var npcId = packetStream.ReadUnsignedWord();
            var npc = context.Server.NpcProvider.GetById(npcId);

            if (npc == null)
            {
                context.Logs.Warning(this, $"{context.Player.Username} tried to interact with null npcId {npcId}");
                return Task.FromResult(PacketResult.AllOk);
            }

//            context.Player.UpdateState.UpdateInteractObject(npcId);

            return Task.FromResult(PacketResult.AllOk);
        }
    }
}
