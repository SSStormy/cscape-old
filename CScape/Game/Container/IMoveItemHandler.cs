using System.Threading.Tasks;

namespace CScape.Game.Container
{
    public interface IMoveItemHandler
    {
        Task Swap(int target, int slot);
    }
}