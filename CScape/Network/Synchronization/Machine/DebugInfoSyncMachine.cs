using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Network.Synchronization.Messages;

namespace CScape.Network.Synchronization.Machine
{
    public sealed class DebugInfoSyncMachine : AbstractSynchronizationMachine
    {
        public DebugInfoSyncMachine(Player player) : base(player)
        {

        }

        public async Task Hide()
        {
            await Player.Connection.SendMessage(new HideServerStatsMessage());
        }

        public override async Task Synchronize()
        {
            await Player.Connection.SendMessage(
                new UpdateServerStatsMessage(Player.Server.Stats.LastTick));
        }
    }
}