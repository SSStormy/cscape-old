﻿using System.Diagnostics;
using CScape.Network;
using CScape.Server;

namespace CScape.IO
{
    public sealed class RsPacketStream : RsStream
    {
        public ScapeServer Server { get; }
        public ConnectionContext Context { get; }

        public RsPacketStream(byte[] buffer, int start, int size, ScapeServer server, ConnectionContext context)
            : base(buffer, start, size)
        {
            Server = server;
            Context = context;
        }

        public byte ReadOpcode(IsaacRandomGen isaac = null)
        {
            if (isaac == null)
                return ReadUnsignedByte();

            return (byte) (ReadUnsignedByte() - isaac.GetValue() & 0xff);
        }

        public int ReadSize(byte opcode)
        {
            if (opcode == 0)
                return 0;

            var sizeMeta = Server.InboundSchemas.Get(opcode);

            if (sizeMeta == -3)
            {
                Debug.Fail($"Invalid packet opcode {opcode}");
                Context.Dispose();
            }

            if (sizeMeta == -1)
                return ReadSignedByte();

            return sizeMeta;
        }
    }
}