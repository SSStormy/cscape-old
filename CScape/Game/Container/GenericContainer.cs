﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using CScape.Game.Items;
using CScape.Server;
using CScape.Helper;

namespace CScape.Game.Container
{
    public abstract class GenericContainer : IContainer
    {
        public ScapeServer Server { get; }

        protected readonly ContainerSlot[] InternalSlots;

        public abstract IEnumerator<ItemStack> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int ContainerId { get; }
        public int Capacity { get; set; }
        public int Items { get; protected set; }

        public IEnumerable<ContainerSlot> Slots => InternalSlots;

        public event EventHandler SyncClear;
        public event EventHandler<int> SyncDirtyAtIndex;

        public virtual ItemStack this[int i]
        {
            get { return InternalSlots[i].Item; }
            set
            {
                Debug.Assert(value != null);
                Debug.Assert(i >= 0 && i < Capacity);
                SetItem(value, i);
            }
        }
        public abstract ContainerChangeInfo CanAddItem(ItemStack item);
        public abstract ContainerChangeInfo CanRemoveItem(ItemStack item);

        public void Clear()
        {
            SyncClear.Raise(this, EventArgs.Empty);

            Items = 0;
            for (var i = 0; i < Capacity; i++)
                InternalSlots[i].Item.SetEmpty();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SyncSlot(int index)
            => SyncDirtyAtIndex.Raise(this, index);

        public GenericContainer(ScapeServer server, int containerId, ContainerSlot[] slots) :this(containerId, server, slots.Length)
        {
            Debug.Assert(server != null);

            InternalSlots = slots;
            var items = 0;
            for (var i = 0; i < slots.Length; i++)
            {
                if (!slots[i].Item.IsEmpty)
                    items++;
            }
            Items = items;
        }

        public GenericContainer(int size, int containerId, ScapeServer server) : this(containerId, server, size)
        {
            InternalSlots = new ContainerSlot[size];
            for (var i = 0; i < size; i++)
            {
                var slot = new ContainerSlot(i);
                InternalSlots[i] = slot;
            }
        }

        private GenericContainer(int id, ScapeServer server, int capacity)
        {
            Capacity = capacity;
            ContainerId = id;
            Server = server;
        }

        protected void SetItem(ItemStack item, int index)
        {
            var slot = InternalSlots[index];
            if(slot.Item.IsEmpty)
                Items++;
            slot.Item = item;
            SyncSlot(index);
        }

        protected ContainerChangeInfo GetAmountStackInfo(ItemDef def, ItemStack slotItem, ItemStack item, int index)
        {
            if (def.Stackable)
            {
                if (slotItem.Amount < ItemStack.MaxStack)
                {
                    if (slotItem.Amount + item.Amount <= ItemStack.MaxStack)
                        return new ContainerChangeInfo(true, index, true, 0);

                    return new ContainerChangeInfo(true, index, true,
                        (uint)Math.Abs(ItemStack.MaxStack - slotItem.Amount - item.Amount));
                }
                    
            }
            return ContainerChangeInfo.Invalid;
        }
    }
}