﻿using CScapeCache.Core;

namespace CScapeCache.Model
{
    public class NpcDefinition
    {
        public static int[] StreamIndices;

        private static DataBuffer _stream;
        private static NpcDefinition[] _cache;

        public static CacheReader LoadedCache { get; set; }

        public long Id { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string[] Actions { get; set; }
        public int CombatLevel { get; set; }
        public int[] SubNpciDs { get; set; }
        public int HeadIcon { get; set; }
        public bool Clickable { get; set; }
        public bool HasVisibilityPriority { get; set; }
        public sbyte Size { get; set; }
        public bool ShowMinimapDot { get; set; }
        public int[] ModelRecolorIndexes { get; set; }
        public int[] ModelRecolorColors { get; set; }
        public sbyte LightIntensity { get; set; }
        public int LightDistance { get; set; }
        public int[] ModelIndexes { get; set; }
        public int[] FaceModels { get; set; }
        public int ScaleXz { get; set; }
        public int ScaleY { get; set; }
        public int StandAnimation { get; set; }
        public int WalkAnimation { get; set; }
        public int Turn90CwAnimation { get; set; }
        public int Turn90CcwAnimation { get; set; }
        public int Turn180Animation { get; set; }
        public int TurnAmount { get; set; }
        public int VarBitIndex { get; set; }
        public int SettingsId { get; set; }

        private static int StorageCacheIndex { get; set; }

        private NpcDefinition()
        {
            Turn90CcwAnimation = -1;
            VarBitIndex = -1;
            Turn180Animation = -1;
            SettingsId = -1;
            CombatLevel = -1;
            WalkAnimation = -1;
            Size = 1;
            HeadIcon = -1;
            StandAnimation = -1;
            Id = -1L;
            TurnAmount = 32;
            Turn90CwAnimation = -1;
            Clickable = true;
            ScaleY = 128;
            ShowMinimapDot = true;
            ScaleXz = 128;
            HasVisibilityPriority = false;
        }

        public static NpcDefinition GetNpc(int i)
        {
            for (var j = 0; j < 20; j++)
                if (_cache[j].Id == i)
                    return _cache[j];

            StorageCacheIndex = (StorageCacheIndex + 1)%20;
            var entityDef = _cache[StorageCacheIndex] = new NpcDefinition();
            _stream.Location = StreamIndices[i];
            entityDef.Id = i;
            entityDef.ReadNpc(_stream);
            return entityDef;
        }

        public static void UnpackNpCs(CacheReader loadedCache, DataBuffer dataFile, DataBuffer indexFile)
        {
            LoadedCache = loadedCache;
            _stream = dataFile;
            var indexStream = indexFile;
            var totalNpCs = indexStream.ReadShort();
            StreamIndices = new int[totalNpCs];
            var i = 2;
            for (var j = 0; j < totalNpCs; j++)
            {
                StreamIndices[j] = i;
                i += indexStream.ReadShort();
            }

            _cache = new NpcDefinition[20];
            for (var k = 0; k < 20; k++)
                _cache[k] = new NpcDefinition();
        }

        public static void NullLoader()
        {
            StreamIndices = null;
            _cache = null;
            _stream = null;
        }

        private void ReadNpc(DataBuffer stream)
        {
            do
            {
                int i = stream.ReadByte();
                if (i == 0)
                    return;
                if (i == 1)
                {
                    int j = stream.ReadByte();
                    ModelIndexes = new int[j];
                    for (var j1 = 0; j1 < j; j1++)
                        ModelIndexes[j1] = stream.ReadShort();
                }
                else if (i == 2)
                    Name = stream.ReadString();
                else if (i == 3)
                    Description = stream.ReadString();
                else if (i == 12)
                    Size = stream.ReadSignedByte();
                else if (i == 13)
                    StandAnimation = stream.ReadShort();
                else if (i == 14)
                    WalkAnimation = stream.ReadShort();
                else if (i == 17)
                {
                    WalkAnimation = stream.ReadShort();
                    Turn180Animation = stream.ReadShort();
                    Turn90CwAnimation = stream.ReadShort();
                    Turn90CcwAnimation = stream.ReadShort();
                }
                else if (i >= 30 && i < 40)
                {
                    if (Actions == null)
                        Actions = new string[5];
                    Actions[i - 30] = stream.ReadString();
                    if (Actions[i - 30].ToLower().Equals("hidden"))
                        Actions[i - 30] = null;
                }
                else if (i == 40)
                {
                    int k = stream.ReadByte();
                    ModelRecolorIndexes = new int[k];
                    ModelRecolorColors = new int[k];
                    for (var k1 = 0; k1 < k; k1++)
                    {
                        ModelRecolorIndexes[k1] = stream.ReadShort();
                        ModelRecolorColors[k1] = stream.ReadShort();
                    }
                }
                else if (i == 60)
                {
                    int l = stream.ReadByte();
                    FaceModels = new int[l];
                    for (var l1 = 0; l1 < l; l1++)
                        FaceModels[l1] = stream.ReadShort();
                }
                else if (i == 90)
                    stream.ReadShort();
                else if (i == 91)
                    stream.ReadShort();
                else if (i == 92)
                    stream.ReadShort();
                else if (i == 93)
                    ShowMinimapDot = false;
                else if (i == 95)
                    CombatLevel = stream.ReadShort();
                else if (i == 97)
                    ScaleXz = stream.ReadShort();
                else if (i == 98)
                    ScaleY = stream.ReadShort();
                else if (i == 99)
                    HasVisibilityPriority = true;
                else if (i == 100)
                    LightIntensity = stream.ReadSignedByte();
                else if (i == 101)
                    LightDistance = stream.ReadSignedByte()*5;
                else if (i == 102)
                    HeadIcon = stream.ReadShort();
                else if (i == 103)
                    TurnAmount = stream.ReadShort();
                else if (i == 106)
                {
                    VarBitIndex = stream.ReadShort();
                    if (VarBitIndex == 65535)
                        VarBitIndex = -1;
                    SettingsId = stream.ReadShort();
                    if (SettingsId == 65535)
                        SettingsId = -1;
                    int i1 = stream.ReadByte();
                    SubNpciDs = new int[i1 + 1];
                    for (var i2 = 0; i2 <= i1; i2++)
                    {
                        SubNpciDs[i2] = stream.ReadShort();
                        if (SubNpciDs[i2] == 65535)
                            SubNpciDs[i2] = -1;
                    }
                }
                else if (i == 107)
                    Clickable = false;
            } while (true);
        }
    }
}