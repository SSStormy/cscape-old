﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CScape.Game;
using CScape.Game.Objects.Player;
using CScape.Helper.Json;
using Newtonsoft.Json;

namespace CScape.Server
{
    public sealed class PlayerIo
    {
        private readonly HashSet<string> _availPlayerLookup = new HashSet<string>();

        public LogManager Logs => Server.Logs;
        public ScapeServer Server { get; }

        private readonly object _playerIoLock = new object();

        private readonly JsonConverter[] _playerJsonConverters;

        public PlayerIo(ScapeServer server)
        {
            Server = server;
            _playerJsonConverters = new JsonConverter[]
            {
                new GenericContainerJsonConverter(Server)
            };
        }

        public bool IsPlayerAvailable(string username)
            => _availPlayerLookup.Contains(username);

        public void LoadAvailablePlayers()
        {
            if (!Directory.Exists(Server.Config.PlayerSaveDir))
            {
                Logs.Warning(this, "Player dir not found, creating.");
                Directory.CreateDirectory(Server.Config.PlayerSaveDir);
                return;
            }

            foreach (var pFile in
                Directory.EnumerateFiles(Server.Config.PlayerSaveDir, "*", SearchOption.AllDirectories)
                    .Select(Path.GetFileNameWithoutExtension))
            {
                _availPlayerLookup.Add(pFile);
            }
        }

        private string GetSavePath(string username)
            => Path.Combine(Server.Config.PlayerSaveDir, username + ".json");

        public Player LoadPlayer(string username)
        {
            try
            {
                var path = GetSavePath(username);

                if (!File.Exists(path))
                {
                    Logs.Fatal(this, $"Could not find {username}'s json.");
                    return null;
                }

                return JsonConvert.DeserializeObject<Player>(File.ReadAllText(path), _playerJsonConverters);
            }
            catch (JsonException jex)
            {
                Logs.Fatal(this, $"Could NOT load {username}'s json.", jex);
            }
            return null;
        }

        public void SavePlayer(Player player, bool savePwds)
        {
            lock (player)
            {
                File.WriteAllText(GetSavePath(player.Username),
                    JsonConvert.SerializeObject(player, Formatting.Indented, _playerJsonConverters));
                if (savePwds)
                    Server.Pwd.Save();
            }
        }

        public void SaveAllPlayers()
        {
            foreach (var player in Server.PlayerProvider.Players)
                SavePlayer(player, false);
            Server.Pwd.Save();
        }

        public Player CreateNewPlayer(UserConnectProof proof)
        {
            lock (_playerIoLock)
            {
                var path = GetSavePath(proof.Username);

                if (File.Exists(path))
                {
                    var existingPlayer = LoadPlayer(proof.Username);
                    if (existingPlayer != null)
                    {
                        _availPlayerLookup.Add(existingPlayer.Username);
                        return existingPlayer;
                    }
                }

                var player = Player.Default(proof.Context, proof.Username);
                SavePlayer(player, true);

                _availPlayerLookup.Add(proof.Username);
                Server.Pwd.RegisterNewPwd(proof.Username, proof.Password);

                Logs.Debug(this, $"Created new player {player.Username}");
                return player;
            }
        }
    }
}
