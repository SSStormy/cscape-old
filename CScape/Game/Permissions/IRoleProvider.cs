﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CScape.Game.Objects.Player;
using CScape.Server;

namespace CScape.Game.Permissions
{
    // todo : role system
    public interface IRoleProvider
    {
        Task<IEnumerable<Role>> GetRoles(Player player, ScapeServer host);
    }
}
