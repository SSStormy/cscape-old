using System;

namespace CScapeCache.Caches.Collision
{
    public sealed class PositionKey : IEquatable<PositionKey>
    {
        public  uint X { get;  }
        public uint Y { get; }
        public ushort Z { get; }

        public PositionKey(int x, int y) : this(x,y,0)
        {
            
        }

        public PositionKey(int x, int y, int z)
        {
            X = (uint)x;
            Y = (uint)y;
            Z = (ushort)z;
        }

        public PositionKey(uint x, uint y, ushort z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public PositionKey Clone()
            => new PositionKey(X, Y, Z);

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            var key = obj as PositionKey;
            if (key == null)
                return false;

            return Equals(key);
        }

        public bool Equals(PositionKey other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return X == other.X && Y == other.Y && Z == other.Z;
        }

        public override int GetHashCode()
        {
            return (int)(((Z << 30) & 0xC0000000) | ((Y << 15) & 0x3FFF8000) | (X & 0x7FFF));
        }
    }
}