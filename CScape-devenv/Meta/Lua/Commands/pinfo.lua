﻿REQUIRE = { "player" }

function command (player)
	printPlayerInfoTo(player, player)

	for lplayer in player.position.getLocalClusterPlayers () do
		printPlayerInfoTo (lplayer, player)
	end
end

function printPlayerInfoTo(infoTarget, receiver)
	receiver.sendMessage ("[" .. infoTarget.username .. "]: PID: " .. infoTarget.playerId ..
	" ConID: " .. infoTarget.connection.connectionId )
end

