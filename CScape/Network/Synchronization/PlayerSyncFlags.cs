﻿using System;

namespace CScape.Network.Synchronization
{
    [Flags]
    public enum PlayerSyncFlags : ushort
    {
        None = 0,
        MovementAnim = 0x400,
        UpdateGraphics = 0x100,
        StartAnimation = 8,
        OverheadText = 4,
        ChatText = 0x80,
        InteractingObject = 1,
        Appearance = 0x10,
        FacingTile = 2,
        Health = 0x20,
        SpecialHealth = 0x200
    }
}
