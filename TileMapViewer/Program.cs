﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using CScapeCache;

namespace TileMapViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var bitmap = new Bitmap(11000, 4000);
            var generator = new TileMapGenerator();
            var reader =
                new CacheReader(
                    @"C:\Users\shittyvm\Documents\Visual Studio 2015\Projects\RspsTests\CScape\Meta\Cache\main_file_cache.dat",
                    @"C:\Users\shittyvm\Documents\Visual Studio 2015\Projects\RspsTests\CScape\Meta\Cache\main_file_cache.idx");

            generator.GenerateTileMap(reader,
                (x, y, rr, gg, bb) => { bitmap.SetPixel(y, x, Color.FromArgb(rr, gg, bb)); });

            bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
            bitmap.Save("out.png", ImageFormat.Png);
        }
    }
}
