﻿namespace CScape.Game.Container
{
    public class ContainerId
    {
        public int PlayerBackpack { get; set; }
        public int PlayerEquipment { get; set; }
    }
}
