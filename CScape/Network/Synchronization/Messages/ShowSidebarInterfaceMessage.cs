using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class ShowSidebarInterfaceMessage : ISynchronizationMessage
    {
        public int InterfaceId { get; }
        public int SidebarId { get; }

        public ShowSidebarInterfaceMessage(int interfaceId, int sidebarId)
        {
            InterfaceId = interfaceId;
            SidebarId = sidebarId;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var packet = new PacketBuilder(receiver.Player, 8);
            packet.OpcodeAndSizePlaceholder(OutPacketType.SendSidebarInterface)
                .WriteWord(InterfaceId)
                .WriteByte(SidebarId);
            return packet;
        }
    }
}