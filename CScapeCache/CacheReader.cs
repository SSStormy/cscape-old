﻿using System;
using System.Collections.Generic;
using System.IO;
using CScapeCache.Caches;
using CScapeCache.Caches.Collision;
using CScapeCache.Core;
using CScapeCache.Model;

/*
 * Pulled from https://github.com/Lin20/cachesuite at 9/13/2016
 * Huge thanks to lin20 for this.
 */

namespace CScapeCache
{
    public class CacheReader : IDisposable
    {
        public FileStream DataFile { get; private set; }
        public Archive[] Archives { get; private set; }
        public List<SubArchive> SubArchives { get; private set; }

        public VarBit[] VarBitCache { get; internal set; }

        public GenericConfigCache<ItemModel> ItemCache { get; private set; }
        public GenericConfigCache<ObjectModel> ObjectCache { get; private set; }
        public FloCache FloCache { get; private set; }
        public MapIndexCache MapIndexCache { get; private set; }
        public MapCache MapCache { get; private set; }
        public CollisionCache CollisionCache { get; private set; }
        public InterfaceCache InterfaceCache { get; private set; }

        public bool IsDisposed { get; private set; }

        public const int ArchiveCount = 5;

        public CacheReader(string dataDir, string indexBaseDir)
        {
            if (dataDir == null) throw new ArgumentNullException(nameof(dataDir));
            if(!File.Exists(dataDir)) throw new FileNotFoundException(nameof(dataDir));
            if (indexBaseDir == null) throw new ArgumentNullException(nameof(indexBaseDir));

            Archives = new Archive[ArchiveCount];
            SubArchives = new List<SubArchive>();

            DataFile = new FileStream(dataDir, FileMode.Open);

            for (var i = 0; i < ArchiveCount; i++)
                Archives[i] = new Archive(DataFile, new FileStream(indexBaseDir + i, FileMode.Open), i);

            for (var i = 0; i < Archives[0].GetFileCount(); i++)
                SubArchives.Add(new SubArchive(Archives[0].ExtractFile(i)));

            ItemCache = new GenericConfigCache<ItemModel>(this, SubArchives[2].ExtractFile("obj.dat"), SubArchives[2].ExtractFile("obj.idx"), new ItemModelFactory());
            MapIndexCache = new MapIndexCache(this);
            MapCache = new MapCache(this);
            ObjectCache = new GenericConfigCache<ObjectModel>(this, SubArchives[2].ExtractFile("loc.dat"), SubArchives[2].ExtractFile("loc.idx"), new ObjectModelFactory());
            CollisionCache = new CollisionCache(this);
            FloCache = new FloCache(this, SubArchives[2].ExtractFile("flo.dat"), new FloModelFactory());
            InterfaceCache = new InterfaceCache(this);
        }

        public void Dispose()
        {
            if (IsDisposed)
                return;

            CollisionCache = null;
            MapIndexCache = null;
            ItemCache = null;
            MapCache = null;
            ObjectCache = null;
            InterfaceCache = null;

            IsDisposed = true;

            DataFile?.Close();
            if (Archives == null)
                return;

            foreach (var archive in Archives)
                archive?.IndexFile?.Close();

            Archives = null;
            SubArchives = null;
        }
    }
}