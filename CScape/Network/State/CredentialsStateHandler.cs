﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Game;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.IO;
using CScape.Server;

namespace CScape.Network.State
{
    /// <summary>
    /// Handles the parsing and lifetime of all player login credentials.
    /// </summary>
    public sealed class CredentialsStateHandler : IStateHandler
    {
        public ConnectionContext Context { get; }
        private LoginCryptoManager Crypto => Context.Server.LoginCrypto;

        private readonly bool _isReconnecting;

        public CredentialsStateHandler(ConnectionContext context, bool isReconnecting)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            Context = context;
            _isReconnecting = isReconnecting;
        }

        public async Task Handle(RsPacketStream packet)
        {
            if (packet == null) throw new ArgumentNullException(nameof(packet));

            var cryptoPayload = new byte[packet.Length - packet.Position];
            packet.FlushInto(cryptoPayload, 0, packet.Position, cryptoPayload.Length, true);
            var payload = Crypto.DecryptLogin(cryptoPayload, 0, cryptoPayload.Length);

            if (payload == null)
            {
                Context.Dispose();
                return;
            }

            var stream = new RsStream(payload);

            var connectState = stream.ReadUnsignedByte();
            var size = stream.ReadUnsignedByte();

            if (size + stream.Position != stream.Length)
            {
                Context.Logs.Warning(this,
                    $"Size mismatch in login credentials: size({size}) + stream.Position({stream.Position}) != stream.Length({stream.Length})",
                    conid: Context.ConnectionId);
                Context.Dispose();
                return;
            }

            var two55 = stream.ReadUnsignedByte();
            var revision = stream.ReadSignedWord();

            var lowMem = stream.ReadUnsignedByte() == 1;
            var crcs = new int[UserConnectProof.CrcAmount];

            for (var i = 0; i < UserConnectProof.CrcAmount; i++)
                crcs[i] = stream.ReadDWord();

            var ten = stream.ReadUnsignedByte();
            var tenTwo = stream.ReadUnsignedByte(); // shrug

            var keys = new int[UserConnectProof.KeySize];

            for (int i = 0; i < UserConnectProof.KeySize; i++)
                keys[i] = stream.ReadDWord();

            var userId = stream.ReadDWord();

            string name;
            string pass;
            PlayerLoginResult result;

            if (stream.ReadRsString(Player.MaxUsernameChars, out name))
            {
                if (stream.ReadRsString(Player.MaxPasswordChars, out pass))
                {
                    var proof = new UserConnectProof(Context, connectState, size, lowMem, crcs, keys, userId, name, pass, revision);
                    result = await Context.Parent.Parent.PlayerProvider.Login(proof);
                }
                else
                    result = PlayerLoginResult.InvalidUserOrPass;
            }
            else result = PlayerLoginResult.InvalidUserOrPass;

            Context.Logs.Debug(this, $"[ConId ({Context.ConnectionId})] Login result: {result.Error}");

            var builder = Context.Builder(3);

            if (!result.Valid)
            {
                builder.WriteByte((byte)result.Error);
                await Context.SendPacket(builder);
                return;
            }

            builder.WriteByte((byte)PlayerLoginResult.ErrorState.None);
            if (!_isReconnecting)
            {
                builder.WriteByte((byte)result.Player.Title);
                builder.WriteByte((byte)(result.Player.IsFlagged ? 1 : 0));
            }

            Context.Player = result.Player;
            result.Player.IsLowDetail = lowMem;

            var playing = new PlayingStateHandler(Context, keys);
            Context.Handler = playing;

            await Context.SendPacket(builder);
            await playing.PostLogin();
        }
    }
}
