﻿using System.Diagnostics;
using System.Threading.Tasks;
using CScape.Game.Container;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class MoveItemPacketHandler : IPacketHandler
    {
        public async Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var containerId= packetStream.ReadUnsignedWord();
            var unknownByte = packetStream.ReadSignedByte(); // something interface specific maybe?
            var slotToMove = packetStream.ReadSignedWord();
            var targetSlot = packetStream.ReadSignedWord();

            Debug.Assert(unknownByte == 0);

            IContainer container;
            if (!context.Player.Containers.TryGetValue(containerId, out container))
            {
                context.Logs.Warning(this, $"Player {context.Player.Username} tried to swap items in container {container} that isn't registered for the player.");
                return PacketResult.KillConnection;
            }
            var handler = container as IMoveItemHandler;
            if (handler == null)
            {
                context.Logs.Warning(this, $"Player {context.Player.Username} tried to swap items in container {container} which is not an IMoveItemHandler");
                return PacketResult.AllOk;
            }

            if (slotToMove < 0 || slotToMove >= container.Capacity)
            {
                context.Logs.Warning(this, $"Player {context.Player.Username} tried to swap items in container {container} at slot {slotToMove} which is not in range (max: {container.Capacity})");
                return PacketResult.KillConnection;
            }

            await handler.Swap(slotToMove, targetSlot);

            return PacketResult.AllOk;
        }
    }
}
