﻿using System.Threading.Tasks;
using CScape.IO;

namespace CScape.Network.Packet
{
    public interface IPacketHandler
    {
        Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context);
    }
}
