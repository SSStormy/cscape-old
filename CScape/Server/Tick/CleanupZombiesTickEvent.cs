﻿using System.Linq;
using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    public sealed class CleanupZombiesTickEvent : AbstractTimedTickEvent
    {
        public override int TickInterval => 10;

        public LogManager Logs => Server.Logs;

        public CleanupZombiesTickEvent(ScapeServer server) : base(server)
        {
        }

        public override async Task Process()
        {
            foreach (var context in Server.ConnectionManager.Contexts.Where(c => c != null))
            {
                if (context.IsDead)
                {
                    Logs.Debug(this, "Disposed zombie.", conid: context.ConnectionId);
                    context.Dispose();
                }
            }

            foreach (var player in Server.PlayerProvider.Players)
            {
                if (player.Connection == null || player.Connection.IsDead)
                    Server.PlayerProvider.CleanupPlayer(player);
            }
        }
    }
}
