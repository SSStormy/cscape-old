using System.Threading.Tasks;
using CScape.Network;

namespace CScape.Game.Interface
{
    public class SidebarInterfaceControler : InterfaceController
    {
        private IShowSidebarInterfaceConfig _main;

        public SidebarInterfaceControler(PlayerInterfaces parent, IShowSidebarInterfaceConfig config) 
            : base(parent, config)
        {
            _main = config;
        }

        public override async Task Show() => await InternalShow(_main.TabIndex, _main.InterfaceId);

        protected override async Task InternalClose()
        {
            Parent.SideInterfaces[_main.TabIndex] = null;
            await InternalShow(_main.TabIndex, -1);
        } 

        private async Task InternalShow(int index, int iid)
        {
            var builder = Parent.Player.Connection.Builder(8);
            builder.OpcodeAndSizePlaceholder(OutPacketType.SendSidebarInterface)
                .WriteWord(iid)
                .WriteByte(index);

            await Parent.Player.Connection.SendPacket(builder, false, false);
        }

        public async Task ForceShowThis()
        {
            await Parent.Player.Connection.Send(OutPacketType.ForceShowTab, b =>
            {
                b.WriteByte(_main.TabIndex);
            }, 4);
        }

        public async Task Flash()
        {
            await Parent.Player.Connection.Send(OutPacketType.FlashTab, b =>
            {
                b.WriteByte(_main.TabIndex);
            }, 4);
        }
    }
}