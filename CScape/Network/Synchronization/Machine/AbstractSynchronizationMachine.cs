﻿using System.Threading.Tasks;
using CScape.Game.Objects.Player;

namespace CScape.Network.Synchronization.Machine
{
    public abstract class AbstractSynchronizationMachine
    {
        public Player Player { get; }

        public AbstractSynchronizationMachine(Player player)
        {
            Player = player;
        }

        public abstract Task Synchronize();
    }
}
 