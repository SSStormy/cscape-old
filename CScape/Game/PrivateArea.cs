﻿using System;
using System.Collections.Generic;
using System.Linq;
using CScapeCache.Caches;
using CScapeCache.Caches.Collision;
using Newtonsoft.Json;

namespace CScape.Game
{
    public struct PrivateTile
    {
        public int SourceX { get; set; }
        public int SourceY { get; set; }
        public int SourceZ { get; set; }
        public int Rotation { get; set; }

        public PrivateTile(int sourceX, int sourceY, int sourceZ, int rotation)
        {
            SourceX = sourceX;
            SourceY = sourceY;
            SourceZ = sourceZ;
            Rotation = rotation;
        }
    }

    public class PrivateArea : Region
    {
        private PrivateArea(int x, int y) : base(x,y)
        {
            
        }

        public void Deallocate()
        {
            
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class PrivateAreaConfig
    {
        /*
         * PositonKey coordinates are treated as coordinates that 
         * are relative to the origin point of the private area given by:
         *   (World.PaStartX + originX; World.PaStartY + originY)
         * OriginX and OriginY values will be set by World during allocation of the private area.
         *   
         * The relative key coordinates will be mapped to global coordinates upon allocation.
         */

        [JsonProperty("tiles")]
        private readonly Dictionary<PositionKey, PrivateTile> _tiles = new Dictionary<PositionKey, PrivateTile>();

        public uint BiggestXOffset { get; private set; }
        public uint BiggestYOffset { get; private set; }

        public IEnumerable<KeyValuePair<PositionKey, PrivateTile>> Tiles => _tiles;

        [JsonConstructor]
        private PrivateAreaConfig(Dictionary<PositionKey, PrivateTile> tiles)
        {
            if(tiles == null) throw new ArgumentNullException(nameof(tiles));

            _tiles = tiles;

            foreach (var pair in _tiles)
                CalcBiggestOffset(pair.Key);
        }

        public PrivateAreaConfig()
        {
            
        }

        private void CalcBiggestOffset(PositionKey key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (key.X > BiggestXOffset)
                BiggestXOffset = key.X;
            if (key.Y > BiggestYOffset)
                BiggestYOffset = key.Y;
        }

        /// <summary>
        /// Sets the tile at the given relative position, overriding the existing value.
        /// </summary>
        public void SetTile(PositionKey relativePos, PrivateTile tileSource)
        {
            if (relativePos == null) throw new ArgumentNullException(nameof(relativePos));
            CalcBiggestOffset(relativePos);

            if (_tiles.ContainsKey(relativePos))
                _tiles[relativePos] = tileSource;
            else
                _tiles.Add(relativePos, tileSource);
        }

        /// <summary>
        /// Returns the tile at the given relative position. Throws a KeyNotFoundException exception if value isn't set.
        /// </summary>
        public PrivateTile GetTile(PositionKey relativePos)
        {
            if (relativePos == null) throw new ArgumentNullException(nameof(relativePos));
            if (_tiles.ContainsKey(relativePos))
                return _tiles[relativePos];

            throw new KeyNotFoundException();
        }

        public PrivateAreaConfig Clone()
        {
            return new PrivateAreaConfig(
                _tiles.ToDictionary(tile => tile.Key.Clone(), tile => tile.Value))
            {
                BiggestXOffset = BiggestXOffset,
                BiggestYOffset = BiggestYOffset
            };
        }
            
    }
}
