﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using MoonSharp.Interpreter;
using MoonSharp.RemoteDebugger;

// ReSharper disable once CheckNamespace
namespace CScape.Lua
{
    public class LuaScript
    {
        public Script Lua { get; }
        public string ScriptDirectory { get; }

        public bool Debug { get; set; }

        private RemoteDebuggerService _debugger;

        protected LuaScript(Script lua, string scriptDirectory)
        {
            Lua = lua;
            ScriptDirectory = scriptDirectory;
        }

        public static async Task<LuaScript> FromFile(string fileDir)
        {
            if (!File.Exists(fileDir))
                throw new ArgumentException($"Script file at {fileDir} doesn't exist.");

            var script = new Script();
            await script.DoStreamAsync(File.Open(fileDir, FileMode.Open));

            return new LuaScript(script, fileDir);
        }

        public async Task<DynValue> Call(string funcName, params object[] args)
        {
            try
            {
                var obj = Lua.Globals[funcName];

                if (Debug)
                {
                    if (_debugger == null)
                    {
                        _debugger = new RemoteDebuggerService();
                        _debugger.Attach(Lua, ScriptDirectory);
                    }

                    Process.Start(_debugger.HttpUrlStringLocalHost);
                }
                await Lua.CallAsync(obj, args);

            }
            catch (ScriptRuntimeException ex)
            {
                System.Diagnostics.Debug.Fail($"Script {ScriptDirectory} threw. Ex: {ex.DecoratedMessage}");
            }
            return DynValue.Nil;
        }
    }
}
