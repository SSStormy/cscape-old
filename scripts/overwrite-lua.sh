#!/bin/bash

#
# Copies all lua scripts from the project source into the runtime folder given by $1 (First arg)
#

if [[ -z $1 ]]; then
	echo "Build folder not specified (Release, Debug, FastDebug)"
else
	BUILD=$1
	LTARG=./CScape/bin/$BUILD/Meta/Lua
	LSRC=./CSCape/Meta/Lua

	rm -rf $LTARG
	cp -r $LSRC $LTARG
fi


