using CScape.Game.Container;
using CScape.IO;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class ClearContainerMessage : ISynchronizationMessage
    {
        public IContainer Container { get; }

        public ClearContainerMessage(IContainer container)
        {
            Container = container;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            var packet = new PacketBuilder(receiver.Player, 8);
            packet.OpcodeAndSizePlaceholder(OutPacketType.SendItemChunk)
                .WriteWord(Container.ContainerId)
                .WriteWord(0);
            packet.WriteSize();
            return packet;
        }
    }
}