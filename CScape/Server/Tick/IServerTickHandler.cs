﻿using System.Threading.Tasks;

namespace CScape.Server.Tick
{
    public interface IServerTickHandler
    {
        bool ShouldFire { get; }
        void TickOccured();
        Task Process();
    }
}
