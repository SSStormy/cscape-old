﻿using System.Collections.Generic;
using System.IO;
using CScapeCache.Caches.Collision;

namespace CollisionCalculator
{
    public static class CollisionSerializer
    {
        public static void Serialize(string outputDir, Dictionary<PositionKey, CollisionData> collision)
        {
            using(var file = File.Open(outputDir, FileMode.Truncate, FileAccess.Write, FileShare.Read))
            using (var writer = new BinaryWriter(file))
            {
                writer.Write(collision.Count);
                foreach (var pair in collision)
                {
                    writer.Write(pair.Key.X);
                    writer.Write(pair.Key.Y);
                    writer.Write(pair.Key.Z);
                    writer.Write((int)pair.Value.CollisionMask);
                }
            }
        } 

        public static Dictionary<PositionKey, CollisionData> Deserialize(string inputDir)
        {
            var dict = new Dictionary<PositionKey, CollisionData>();

            using (var file = File.Open(inputDir, FileMode.Open, FileAccess.Read, FileShare.None))
            using (var reader = new BinaryReader(file))
            {
                var len = reader.ReadInt32();

                for (var i = 0; i < len; i++)
                    dict.Add(new PositionKey(reader.ReadUInt32(), reader.ReadUInt32(), reader.ReadUInt16()), new CollisionData((CollisionMasks)reader.ReadInt32()));
            }
            return dict;
        }
    }
}
