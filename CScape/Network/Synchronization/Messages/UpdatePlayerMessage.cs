﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CScape.IO;
using CScape.Network.Synchronization.Segments;

namespace CScape.Network.Synchronization.Messages
{
    public sealed class UpdatePlayerMessage : ISynchronizationMessage
    {
        private readonly IEntityUpdateSegment _ourMovement;
        private readonly IList<IUpdateSegment> _otherMoveAndAdd;
        private readonly int _knownLocalPlayers;
        private readonly int _updatesToWrite;

        public UpdatePlayerMessage(IEntityUpdateSegment ourMovement,
            IList<IUpdateSegment> otherMoveAndAdd, int knownLocalPlayers, int updatesToWrite)
        {
            _ourMovement = ourMovement;
            _otherMoveAndAdd = otherMoveAndAdd;
            _knownLocalPlayers = knownLocalPlayers;
            _updatesToWrite = updatesToWrite;
        }

        public PacketBuilder GetPacket(ConnectionContext receiver)
        {
            const int maxMasksWriterSize = 4920;
            const int maxMainPacketSize = 70;
            const int maxResultSize = maxMainPacketSize + maxMainPacketSize;

            var packetSize = 10 + 3 * _otherMoveAndAdd.Count;
            var updSize = 246*(_updatesToWrite + 1);

            Debug.Assert(maxMainPacketSize >= packetSize);
            Debug.Assert(maxMasksWriterSize >= updSize);

            var packet = new PacketBuilder(receiver.Player, packetSize);
            var masks = new RsByteWriter(updSize);

            packet.OpcodeAndSizePlaceholder(OutPacketType.PlayerUpdate);
            packet.InitBitAccess();

            // unset the chat text flag for our player since we don't want our player to see what they send x2
            _ourMovement.FlagWriter.UnsetLocal((int)PlayerSyncFlags.ChatText);
            _ourMovement.WriteInto(packet);
            if(_ourMovement.FlagWriter.NeedsWriting)
                _ourMovement.FlagWriter.Write(masks);

            packet.WriteBits(8, _knownLocalPlayers);
            foreach (var osync in _otherMoveAndAdd)
            {
                osync.WriteInto(packet);
                var usync = osync as IEntityUpdateSegment;
                if (usync == null || !usync.FlagWriter.NeedsWriting)
                    continue;

                usync.FlagWriter.Write(masks);
            }

            if (masks.Position > 0)
            {
                packet.WriteBits(11, 2047);
                packet.FinishBitAccess();
                packet.WriteBytes(masks.ToByteArray(false, false));
            }
            else
                packet.FinishBitAccess();

            packet.WriteSize();
            Debug.Assert(maxResultSize >= packet.Position);
            return packet;
        }
    }
}
