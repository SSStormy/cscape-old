﻿namespace CScape.Network.Packet
{
    public enum PacketResult
    {
        AllOk,
        IncompleteRead,
        KillConnection,
    }
}
