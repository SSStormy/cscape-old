﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game.Interface;
using CScape.Game.Items;
using CScape.Helper;
using CScape.Lua;
using CScape.Network;
using CScape.Network.Synchronization.Machine;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Interop;

namespace CScape.Server.Repository
{
    public sealed class LuaRepository : IRepository<LuaScript, string>
    {
        public const string ScriptKeyOverride = "SCRIPT_KEY";
        public const string DebugKey = "DEBUG";

        public const string LuaIdentifier = ".lua";
        public string CommandIdentifier { get; }

        public string ScriptRootDir { get; set; }
        public LogManager Logs => Server.Logs;

        private readonly Dictionary<string, LuaScript> _scripts =
            new Dictionary<string, LuaScript>(StringComparer.OrdinalIgnoreCase);

        public ScapeServer Server { get; }

        public IEnumerable<LuaScript> SearchFor(Func<LuaScript, bool> predicate)
            => _scripts.Values.Where(predicate);

        public IEnumerable<LuaScript> GetAll()
            => _scripts.Values;

        public LuaScript Get(string key)
            => _scripts.ContainsKey(key) ? _scripts[key] : null;

        // we're using a list here since an IEnumerable did not play well with lua scripts
        public List<LuaCommandScript> GetAllCommands()
            => GetAll().OfType<LuaCommandScript>().ToList();

        public async Task Load()
        {
            _scripts.Clear();

            foreach (var file in Directory.EnumerateFiles(ScriptRootDir, "*", SearchOption.AllDirectories))
            {
                if (!file.EndsWith(LuaIdentifier))
                    continue;

                LuaScript script;
                if (file.StartsWith(CommandIdentifier))
                    script = await LuaCommandScript.FromFile(file, Server);
                else
                    script = await LuaScript.FromFile(file);

                RegisterScript(script);

            }
        }

        public LuaRepository(ScapeServer server)
        {
            Server = server;
            ScriptRootDir = server.Config.LuaDir;
            UserData.RegistrationPolicy = InteropRegistrationPolicy.Automatic;
            UserData.RegisterAssembly();
            CommandIdentifier = Path.Combine(Server.Config.LuaDir, Server.Config.LuaCommandsFolder);
        }

        public void RegisterScript(LuaScript script)
        {
            var debugVal = script.Lua.Globals.Get(DebugKey);
            if (debugVal != null && debugVal.Type == DataType.Boolean)
                script.Debug = debugVal.Boolean;

            script.Lua.Globals["outPacketType"] = UserData.CreateStatic<OutPacketType>();
            script.Lua.Globals["itemStack"] = UserData.CreateStatic<ItemStack>();
            script.Lua.Globals["globals"] = UserData.CreateStatic(typeof(Globals));
            script.Lua.Globals["bookTextConfig"] = UserData.CreateStatic<BookTextConfig>();
            script.Lua.Globals["genericShowableConfig"] = UserData.CreateStatic<GenericShowableConfig>();
            script.Lua.Globals["genericCloseableConfig"] = UserData.CreateStatic<GenericClosableConfig>();
            script.Lua.Globals["characterDesignConfig"] = UserData.CreateStatic<CharacterDesignConfig>();
            script.Lua.Globals["debugInfoSyncMachine"] = UserData.CreateStatic<DebugInfoSyncMachine>();

            var key = script.Lua.Globals[ScriptKeyOverride] as string ??
                      Path.GetFileNameWithoutExtension(script.ScriptDirectory);

            _scripts.Add(key, script);

            Logs.Debug(this,
                $"Registered script at {script.ScriptDirectory} as {key}.");
        }
    }
}