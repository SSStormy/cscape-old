﻿using System;
using System.Collections.Generic;
using CScapeCache.Model;

namespace CScapeCache.Caches
{
    public class MapIndexCache : AbstractLookupCache<int, MapIndex>
    {
        public MapIndexCache(CacheReader reader) : base(reader)
        {
        }

        public MapIndex GetIndices(int mapX, int mapY)
        {
            if(mapX < 0) throw new ArgumentOutOfRangeException(nameof(mapX));
            if(mapY < 0) throw new ArgumentOutOfRangeException(nameof(mapY));

            ThrowIfNotLoaded();

            var hash = (mapX << 8) + mapY;
            return ValueLookup.ContainsKey(hash) ? ValueLookup[hash] : null;
        }

        protected override void InternalLoad()
        {
            var dataFile = Reader.SubArchives[5].ExtractFile("map_index");
            var length = dataFile.Buffer.Length / 7;

            ValueLookup = new Dictionary<int, MapIndex>(length);
            for (var i = 0; i < length; i++)
            {
                var hash = dataFile.ReadShort();
                ValueLookup.Add(hash,
                    new MapIndex(hash, dataFile.ReadShort(), dataFile.ReadShort(), dataFile.ReadByte()));
            }
        }
    }
}
