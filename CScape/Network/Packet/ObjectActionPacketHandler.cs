﻿using System;
using System.Threading.Tasks;
using CScape.Game.Objects;
using CScape.Helper;
using CScape.IO;

namespace CScape.Network.Packet
{
    public sealed class ObjectActionPacketHandler : IPacketHandler
    {
        public Task<PacketResult> Handle(byte opcode, int length, RsPacketStream packetStream, ConnectionContext context)
        {
            var x = packetStream.ReadUnsignedWord();
            var y = packetStream.ReadUnsignedWord();
            var oid = packetStream.ReadUnsignedWord();

            Console.WriteLine($"X: {x}");
            Console.WriteLine($"Y: {y}");
            Console.WriteLine($"OID: {oid}");

            if (!context.Player.Position.IsInLocalRange(x, y))
            {
                context.Logs.Warning(this, $"{context.Player.Username} tried to interact with out of range obj {oid} x: {x} y: {y}");
                return Task.FromResult(PacketResult.AllOk);
            }

            // todo : object action handlers
            context.Player.Position.SyncSetFacingTile(x, y);

            return Task.FromResult(PacketResult.AllOk);
        }
    }
}
