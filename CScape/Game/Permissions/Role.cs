﻿using System;

namespace CScape.Game.Permissions
{
    public class Role : IEquatable<Role>
    {
        public int Id { get; }
        public string Name { get; }
        public int BadgeId { get; }

        public Role(int id, string name, int badgeId)
        {
            Id = id;
            Name = name;
            BadgeId = badgeId;
        }

        public override int GetHashCode()
        {
            return Id*13;
        }

        public bool Equals(Role other)
        {
            return other?.Id == Id;
        }
    }
}
