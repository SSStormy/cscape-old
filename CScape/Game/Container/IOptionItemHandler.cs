﻿using System.Threading.Tasks;
using CScape.Network.Packet;

namespace CScape.Game.Container
{
    public interface IOptionItemHandler
    {
        bool[] ShouldIgnoreOpAt { get; }
        Task HandleOption(ItemOptionType option, int slotId);
    }
}