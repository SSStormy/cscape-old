using System.Threading.Tasks;
using CScape.Network;

namespace CScape.Game.Interface
{
    public class WalkableInterfaceController : InterfaceController
    {
        public WalkableInterfaceController(PlayerInterfaces parent, IShowableInterface config) : base(parent, config)
        {
        }

        public override async Task Show() => await InternalShow(Interface.InterfaceId);

        protected override async Task InternalClose()
        {
            Parent.WalkableInterface = null;
            await InternalShow(-1);
        } 

        private async Task InternalShow(int iid)
        {
            var builder = Parent.Player.Connection.Builder(8);
            builder.OpcodeAndSizePlaceholder(OutPacketType.SendWalkableInterface).WriteWord(iid);

            await Parent.Player.Connection.SendPacket(builder, false, false);
        }
    }
}