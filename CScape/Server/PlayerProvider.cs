﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CScape.Game;
using CScape.Game.Objects.Player;
using CScape.Helper;
using CScape.Network;
using Imms;

namespace CScape.Server
{
    public class PlayerProvider
    {
        private static readonly HashSet<char> ValidChars = new HashSet<char>
        {
            '_',
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        };

        public ScapeServer Server { get; }
        public LogManager Logs => Server.Logs;
        public PlayerIo Io => Server.PlayerIo;
        public int MaxPlayers => Server.Config.MaxPlayers;

        private readonly object _modifyLock = new object();

        private Player[] _players;
        public ImmSet<Player> Players { get; private set; } = ImmSet<Player>.Empty();

        public PlayerProvider(ScapeServer server)
        {
            Server = server;
            _players = new Player[MaxPlayers];
        }

        public Player GetById(int pid)
        {
            if (pid <= 0 || pid > MaxPlayers)
            {
                Logs.Warning(this, $"Tried to get by out of range pid {pid}");
                return null;
            }
            return _players[pid];
        }

        public async Task<PlayerLoginResult> Login(UserConnectProof proof)
        {
            /*
             * Check preconditions
             */
            if (proof == null)
            {
                Logs.Warning(this, "Login was given null proof");
                return PlayerLoginResult.GeneralFailure;
            }

            if (proof.Revision != Globals.Revision)
                return PlayerLoginResult.WrongRevision;

            if (Players.Length >= MaxPlayers)
                return PlayerLoginResult.WorldFull;

            // check for invalid chars in username
            if (proof.Username.Any(c => !ValidChars.Contains(c)))
                return PlayerLoginResult.InvalidUserOrPass;

            if (string.IsNullOrWhiteSpace(proof.Username) ||
                string.IsNullOrWhiteSpace(proof.Password) ||
                proof.Password.Length > Player.MaxPasswordChars ||
                proof.Username.Length > Player.MaxUsernameChars)
            {
                return PlayerLoginResult.GeneralFailure;
            }

            /*
             * Verify Username/Password pair.
             */

            // check if the player already exists in our db
            Player player;
            if (!Io.IsPlayerAvailable(proof.Username))
                player = Io.CreateNewPlayer(proof);
            else
            {
                // verify pass
                if (!Server.Pwd.VerifyPwd(proof.Username, proof.Password))
                    return PlayerLoginResult.InvalidUserOrPass;

                player = Io.LoadPlayer(proof.Username);
            }

            if (player == null)
                return PlayerLoginResult.GeneralFailure;

            // ideally, this check should be up there in the preconditions but we do
            // not want to allow an unauthorized user to know anything about a player
            // it doesn't own.
            if (Players.FirstOrDefault(p =>
                p.Username.Equals(proof.Username, StringComparison.Ordinal)) != null)
                return PlayerLoginResult.AlreadyLoggedIn;

            /*
             * Data is all ok, set up player for playing.
             */

            var pid = 0;
            for (int i = 1; i <= MaxPlayers; i++)
            {
                if (_players[i] != null)
                    continue;

                pid = i;
                break;
            }

            if (pid == 0)
            {
                Logs.Fatal(this, "RAN OUT OF PIDS.");
                return PlayerLoginResult.GeneralFailure;
            }

            if (!await player.PrePlay(proof.Context, pid))
                return PlayerLoginResult.AlreadyLoggedIn;

            lock (_modifyLock)
            {
                Players = Players.Add(player);
                _players[pid] = player;
            }

            return PlayerLoginResult.Success(player);
        }

        public void CleanupPlayer(Player player)
        {
            Logs.Debug(this, $"{player} Cleaning player.");

            player.Position.LeaveAllMaps();

            if (!player.Connection.IsDead)
                player.Connection.Dispose();

            player.MarkedAsKilledConnection = true;
            player.IsPlaying = false;


            player.StaleGlobalPosX = player.Position.GlobalX;
            player.StaleGlobalPosY = player.Position.GlobalY;
            player.StaleGlobalPosZ = player.Position.GlobalZ;

            Io.SavePlayer(player, true);
            lock (_modifyLock)
            {
                Players = Players.Remove(player);
                _players[player.PlayerId] = null;
            }
        }

        public async Task LogoutPlayer(Player player)
        {
            Logs.Debug(this, $"{player} logging out.");
            await player.Connection.Send(OutPacketType.LogoutOrder);
            player.Connection.Dispose();

            // todo : keep the player alive for an amount of time, then remove it from players.
            CleanupPlayer(player);
        }
    }

    public class PlayerLoginResult
    {
        public enum ErrorState : byte
        {
            None = 2,
            InvalidUserOrPass = 3,
            AlreadyLoggedIn = 5,
            WrongRevision = 6,
            WorldIsFull = 7,
            GeneralFailure = 13,
        }

        public ErrorState Error { get; }
        public Player Player { get; }

        public bool Valid => Error == ErrorState.None;

        public static PlayerLoginResult InvalidUserOrPass
            => new PlayerLoginResult(ErrorState.InvalidUserOrPass, null);

        public static PlayerLoginResult AlreadyLoggedIn
            => new PlayerLoginResult(ErrorState.AlreadyLoggedIn, null);

        public static PlayerLoginResult WrongRevision
            => new PlayerLoginResult(ErrorState.WrongRevision, null);

        public static PlayerLoginResult WorldFull
            => new PlayerLoginResult(ErrorState.WorldIsFull, null);

        public static PlayerLoginResult GeneralFailure
            => new PlayerLoginResult(ErrorState.GeneralFailure, null);

        private PlayerLoginResult(ErrorState error, Player player)
        {
            Player = player;
            Error = error;
        }

        public static PlayerLoginResult Success(Player player)
            => new PlayerLoginResult(ErrorState.None, player);
    }
}