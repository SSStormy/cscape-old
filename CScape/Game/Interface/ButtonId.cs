﻿using System.Collections.Generic;

namespace CScape.Game.Interface
{
    public class ButtonId
    {
        public int RunButtonId { get;  set; }
        public int WalkButtonId { get;  set; }
        public int LogoutButtonId { get;  set; }
        public int BookNext { get;  set; }
        public int BookPrevious { get;  set; }
        public int BookCloseWindow { get;  set; }
        public int FinishCharacterDesign { get; set; }

        public Dictionary<int, int> LevelUpContinueLookup { get;  set; }
    }
}