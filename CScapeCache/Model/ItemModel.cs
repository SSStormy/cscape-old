﻿using CScapeCache.Caches;
using CScapeCache.Core;

namespace CScapeCache.Model
{
    public class ItemModel
    {
        public GenericConfigCache<ItemModel> Cache { get; }

        public int Id { get; internal set; }
        public string Name { get; private set; }
        public string Description { get; set; }
        public bool MembersObject { get; set; }
        public string[] GroundActions { get; set; }
        public int Cost { get; set; }
        public string[] Actions { get; set; }
        public int Team { get; set; }
        public bool Stackable { get; set; }
        public int NotedModelItemId { get; set; }
        public int[] StackedItemIDs { get; set; }
        public int[] StackChangeAmounts { get; set; }
        public int ModelIndex { get; set; }
        public int[] ModelRecolorIndexes { get; set; }
        public int[] ModelRecolorColors { get; set; }
        public int LightDistance { get; set; }
        public int LightIntensity { get; set; }
        public int NotedId { get; set; }
        public int SpriteX { get; set; }
        public int SpriteY { get; set; }
        public int RotationX { get; set; }
        public int RotationY { get; set; }
        public int RotationZ { get; set; }
        public int Zoom { get; set; }
        public int ScaleX { get; set; }
        public int ScaleY { get; set; }
        public int ScaleZ { get; set; }

        public sbyte AByte154 { get; set; }
        public int AnInt162 { get; set; }

        public int AnInt164 { get; set; }
        public int AnInt165 { get; set; }
        public int AnInt166 { get; set; }
        public int AnInt173 { get; set; }
        public int AnInt175 { get; set; }
        public int AnInt185 { get; set; }
        public int AnInt188 { get; set; }
        public int AnInt197 { get; set; }
        public int AnInt200 { get; set; }
        public sbyte AByte205 { get; set; }

        internal ItemModel(GenericConfigCache<ItemModel> cache, int id)
        {
            Cache = cache;
            Id = id;
            ModelIndex = 0;
            Name = null;
            Description = null;
            ModelRecolorIndexes = null;
            ModelRecolorColors = null;
            Zoom = 2000;
            RotationX = 0;
            RotationY = 0;
            RotationZ = 0;
            SpriteX = 0;
            SpriteY = 0;
            Stackable = false;
            Cost = 1;
            MembersObject = false;
            GroundActions = null;
            Actions = null;
            AnInt165 = -1;
            AnInt188 = -1;
            AByte205 = 0;
            AnInt200 = -1;
            AnInt164 = -1;
            AByte154 = 0;
            AnInt185 = -1;
            AnInt162 = -1;
            AnInt175 = -1;
            AnInt166 = -1;
            AnInt197 = -1;
            AnInt173 = -1;
            StackedItemIDs = null;
            StackChangeAmounts = null;
            NotedId = -1;
            NotedModelItemId = -1;
            ScaleX = 128;
            ScaleY = 128;
            ScaleZ = 128;
            LightIntensity = 0;
            LightDistance = 0;
            Team = 0;
        }

        internal void ConvertToNote()
        {
            var itemDef = Cache.GetId(NotedModelItemId);
            ModelIndex = itemDef.ModelIndex;
            Zoom = itemDef.Zoom;
            RotationX = itemDef.RotationX;
            RotationY = itemDef.RotationY;

            RotationZ = itemDef.RotationZ;
            SpriteX = itemDef.SpriteX;
            SpriteY = itemDef.SpriteY;
            ModelRecolorIndexes = itemDef.ModelRecolorIndexes;
            ModelRecolorColors = itemDef.ModelRecolorColors;
            var itemDef1 = Cache.GetId(NotedId);
            Name = itemDef1.Name;
            MembersObject = itemDef1.MembersObject;
            Cost = itemDef1.Cost;
            var s = "a";
            var c = itemDef1.Name[0];
            if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
                s = "an";
            Description = "Swap this note at any bank for " + s + " " + itemDef1.Name + ".";
            Stackable = true;
        }

        public int GetModelIndex(int amount)
        {
            if (StackedItemIDs != null && amount > 1)
            {
                var j = -1;
                for (var k = 0; k < 10; k++)
                    if (amount >= StackChangeAmounts[k] && StackChangeAmounts[k] != 0)
                        j = StackedItemIDs[k];

                if (j != -1)
                    return Cache.GetId(j).ModelIndex;
            }

            return ModelIndex;
        }

        internal void ReadItem(DataBuffer stream)
        {
            do
            {
                int i = stream.ReadByte();
                if (i == 0)
                    return;
                if (i == 1)
                    ModelIndex = stream.ReadShort();
                else if (i == 2)
                    Name = stream.ReadString();
                else if (i == 3)
                    Description = stream.ReadString();
                else if (i == 4)
                    Zoom = stream.ReadShort();
                else if (i == 5)
                    RotationX = stream.ReadShort();
                else if (i == 6)
                    RotationY = stream.ReadShort();
                else if (i == 7)
                {
                    SpriteX = stream.ReadShort();
                    if (SpriteX > 32767)
                        SpriteX -= 0x10000;
                }
                else if (i == 8)
                {
                    SpriteY = stream.ReadShort();
                    if (SpriteY > 32767)
                        SpriteY -= 0x10000;
                }
                else if (i == 10)
                    stream.ReadShort();
                else if (i == 11)
                    Stackable = true;
                else if (i == 12)
                    Cost = stream.ReadInteger();
                else if (i == 16)
                    MembersObject = true;
                else if (i == 23)
                {
                    AnInt165 = stream.ReadShort();
                    AByte205 = stream.ReadSignedByte();
                }
                else if (i == 24)
                    AnInt188 = stream.ReadShort();
                else if (i == 25)
                {
                    AnInt200 = stream.ReadShort();
                    AByte154 = stream.ReadSignedByte();
                }
                else if (i == 26)
                    AnInt164 = stream.ReadShort();
                else if (i >= 30 && i < 35)
                {
                    if (GroundActions == null)
                        GroundActions = new string[5];
                    GroundActions[i - 30] = stream.ReadString();
                    if (GroundActions[i - 30].ToLower().Equals("hidden"))
                        GroundActions[i - 30] = null;
                }
                else if (i >= 35 && i < 40)
                {
                    if (Actions == null)
                        Actions = new string[5];
                    Actions[i - 35] = stream.ReadString();
                }
                else if (i == 40)
                {
                    int j = stream.ReadByte();
                    ModelRecolorIndexes = new int[j];
                    ModelRecolorColors = new int[j];
                    for (var k = 0; k < j; k++)
                    {
                        ModelRecolorIndexes[k] = stream.ReadShort();
                        ModelRecolorColors[k] = stream.ReadShort();
                    }
                }
                else if (i == 78)
                    AnInt185 = stream.ReadShort();
                else if (i == 79)
                    AnInt162 = stream.ReadShort();
                else if (i == 90)
                    AnInt175 = stream.ReadShort();
                else if (i == 91)
                    AnInt197 = stream.ReadShort();
                else if (i == 92)
                    AnInt166 = stream.ReadShort();
                else if (i == 93)
                    AnInt173 = stream.ReadShort();
                else if (i == 95)
                    RotationZ = stream.ReadShort();
                else if (i == 97)
                    NotedId = stream.ReadShort();
                else if (i == 98)
                    NotedModelItemId = stream.ReadShort();
                else if (i >= 100 && i < 110)
                {
                    if (StackedItemIDs == null)
                    {
                        StackedItemIDs = new int[10];
                        StackChangeAmounts = new int[10];
                    }
                    StackedItemIDs[i - 100] = stream.ReadShort();
                    StackChangeAmounts[i - 100] = stream.ReadShort();
                }
                else if (i == 110)
                    ScaleX = stream.ReadShort();
                else if (i == 111)
                    ScaleY = stream.ReadShort();
                else if (i == 112)
                    ScaleZ = stream.ReadShort();
                else if (i == 113)
                    LightIntensity = stream.ReadSignedByte();
                else if (i == 114)
                    LightDistance = stream.ReadSignedByte()*5;
                else if (i == 115)
                    Team = stream.ReadByte();
            } while (true);
        }
    }
}