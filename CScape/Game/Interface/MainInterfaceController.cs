using System.Threading.Tasks;
using CScape.Network;

namespace CScape.Game.Interface
{
    public class MainInterfaceController : InterfaceController
    {
        public MainInterfaceController(PlayerInterfaces parent, IShowableInterface config) 
            : base(parent, config)
        {
        }

        public override async Task Show()
        {
            var builder = Parent.Player.Connection.Builder(8);
            var tabMain = Interface as IShowMainAndTabInterfaceConfig;

            if (Interface is IChatInterface)
            {
                builder.OpcodeAndSizePlaceholder(OutPacketType.SendChatInterface)
                    .WriteWord(Interface.InterfaceId);
            }
            else if (tabMain != null)
            {
                builder.OpcodeAndSizePlaceholder(OutPacketType.SendDualMainInterfaceWithInventoryOverlay)
                    .WriteWord(tabMain.InterfaceId)
                    .WriteWord(tabMain.TabInterfaceId);
            }
            else
            {
                builder.OpcodeAndSizePlaceholder(OutPacketType.SendMainInterface)
                    .WriteWord(Interface.InterfaceId);
            }

            await Parent.Player.Connection.SendPacket(builder, false, false);
        }

        protected override async Task InternalClose()
        {
            Parent.MainInterface = null;
            var builder = Parent.Player.Connection.Builder(4);
            builder.OpcodeAndSizePlaceholder(OutPacketType.ClearInterfaces);
            await Parent.Player.Connection.SendPacket(builder, false, false);
        }
    }
}