﻿using System;
using System.Runtime.CompilerServices;

namespace CScape.Helper
{
    public static class Utils
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Raise<T>(this EventHandler<T> handler, object s, T e)
            => handler?.Invoke(s, e);

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Raise(this EventHandler handler, object s, EventArgs e)
            => handler?.Invoke(s, e);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsBitSet(int val, int bitIndex)
            => (val & (1 << bitIndex)) != 0;

        public static DateTime UnixEpoch { get; } =
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long GetCurrentUnixTimestamp()
            => (long) (DateTime.UtcNow - UnixEpoch).TotalSeconds;

        public static T Clamp<T>(T value, T min, T max) where T : IComparable<T>
        {
            if (value.CompareTo(max) > 0)
                return max;

            return value.CompareTo(min) < 0 ? min : value;
        }
    }
}