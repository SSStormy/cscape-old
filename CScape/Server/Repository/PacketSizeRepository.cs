﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CScape.Server.Repository
{
    public sealed class PacketSizeRepository : IRepository<int, int>
    {
        public ScapeServer Server { get; }
        public string SchemaDir { get; }

        private Dictionary<int, int> _packets = new Dictionary<int, int>();

        public PacketSizeRepository(ScapeServer server, string schemaDir)
        {
            Server = server;
            SchemaDir = schemaDir;
        }

        public IEnumerable<int> SearchFor(Func<int, bool> predicate)
            => _packets.Values.Where(predicate);

        public IEnumerable<int> GetAll()
            => _packets.Values;

        public int Get(int key)
        {
            if(!_packets.ContainsKey(key))
                throw new ArgumentException(nameof(key));

            return _packets[key];
        }

        public Task Load()
        {
            _packets.Clear();

            if (!File.Exists(SchemaDir))
                throw new FileNotFoundException(nameof(SchemaDir));

            _packets = JsonConvert.DeserializeObject<Dictionary<int, int>>(File.ReadAllText(SchemaDir));
            return Task.CompletedTask;
        }
    }
}
